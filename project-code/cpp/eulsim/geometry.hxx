/** geometry.hxx
 *
 * Geometrical operations.
 *
 * Shayan Hoshyari March 2018
 */

#ifndef EULSIM_GEOMETRY_H
#define EULSIM_GEOMETRY_H

#include <string>

#include <eulsim/defs.hxx>
#include <eulsim/dof_vector.hxx>

namespace eulsim
{

class Grid;

class ImplicitFunction
{
public:
  virtual double eval(Vec2r) = 0;
  virtual ~ImplicitFunction() {}
};

struct IsoSurface
{
  int n_points;
  int n_chains;
  std::vector<int> chain_size;
  std::vector<int> chain_begin;
  std::vector<double> points;
};

struct Triangulation
{
  int n_points;
  int n_triangles;
  std::vector<double> points;
  std::vector<int> conn;
};

// Find the intersection of one line and the zero surface of an implicit function
// Uses Regula-Falsi iterations.
bool Geometry_signed_distance_intersection(ImplicitFunction &sgn, const Vec2r &from, const Vec2r &to, Real *t,
                                           const int max_iter = 20);

// Find the intersection of two-lines
bool Geometry_line_intersection(const Vec2r &y0, const Vec2r &y1, const Vec2r &x0, const Vec2r &x1, Real *t);

// Find the isosurface of a function using marching squares
// The first version creates the buffer, the second version uses a supplied buffer.
bool Geometry_extract_zero_surface(ImplicitFunction &implicit_function, Grid &grid, IsoSurface *zero_surface,
                                   Triangulation *tri, const int step=1);

// Apply several iterations of a very simple "Jacobi"-style propagation of valid velocity data in all directions
// From Chrystopher Batty's code
// No periodic nonesense.
template <int BS>
void Geometry_extrapolate_closest_point(DOFVector_R<BS, STORAGE_VERTICES> &dv, DOFVector_1CV &is_valid);
template <int BS>
void Geometry_extrapolate_closest_point(DOFVector_R<BS, STORAGE_VERTICES> &dv,        //
                                        DOFVector_R<BS, STORAGE_VERTICES> &dv_buffer, //
                                        DOFVector_1CV &is_valid,                      //
                                        DOFVector_1CV &is_valid_buffer, const int n_layers);

// Integrate a quantity inside a grid. A very naive subsampling for RHO.
//
// Does not work for boundaries
double Geometry_integrate_via_voxels(Grid &grid, ImplicitFunction &fn);
double Geometry_subsample_via_voxels(Grid &grid, const Index2d &vid, //
                                     ImplicitFunction &value, ImplicitFunction &isin, int level = 3);
// Periodic boundary conditions
// double Geometry_subsample_periodic_via_voxels(Grid &grid, const Index2d &vid, //
//                                              ImplicitFunction &value, ImplicitFunction &isin, int level = 3);


} // namespace eulsim

#endif /*EULSIM_GEOMETRY_H*/
