function [params] = default_params(params)
% function [params] = default_params(params)
if( nargin == 0) 
    params = struct;
end
params = utility.check_default(params, 'c', 1);
params = utility.check_default(params, 'dx', 0.01);
params = utility.check_default(params, 'dt', 0.01);
params = utility.check_default(params, 'fdadvm', 'upwind');
end

