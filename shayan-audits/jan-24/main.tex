%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% - Scribe notes for CS530P 25th January 2018.
% - Instructor: Prof. Dinesh Pai, Scribe: Shayan Hoshyari
% - Adapted from: Template for Scribe Notes -- CS500, Will Evans (Spring 2012)
% - Adapted from: Erik Demaine at MIT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[11pt]{article}
\usepackage{latexsym}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{amsfonts}
%\usepackage{epsfig}
\usepackage[margin=1in]{geometry}
\usepackage{url}
\usepackage{graphicx}
\usepackage{float}
%\usepackage{psfig}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Fonts
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage[T1]{fontenc}
\usepackage{kpfonts}%  for math    
\usepackage{libertine}%  serif and sans serif
\usepackage[scaled=0.85]{beramono}%% mono


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Extra commands
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\scribeTitle}[3]{
  \begin{centering}
  \framebox{ \vbox{
      \textbf{CPSC 530P Sensorimotor Computation}
      
      \vspace{-1mm}
      { #1 }
      
      \vspace{-1mm}
      \emph{#2, Scribe: #3}
    }}
  \end{centering}
  \vspace*{4mm}
}

\newcommand{\R}{\mathbb{R}}     % use for capital R real number set symbol
\newcommand{\Z}{\mathbb{Z}}     % use for capital Z integer set symbol


\newtheorem{theorem}{Theorem}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{observation}[theorem]{Observation}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{fact}[theorem]{Fact}
\newtheorem{assumption}[theorem]{Assumption}

\usepackage[bookmarks,bookmarksnumbered,%
allbordercolors={0.8 0.8 0.8},%
linktocpage%
]{hyperref}

\parindent 0in
\parskip 1.5ex
%\renewcommand{\baselinestretch}{1.25}

\newcommand{\reffig}[1]{Fig.~\ref{#1}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Document Body
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

% ---------------------------------------------
% Title
% ---------------------------------------------
\scribeTitle{Lecture: 24th January, 2017}{Prof.\ Dinesh Pai}{Shayan Hoshyari}


% ---------------------------------------------
% Logistics
% ---------------------------------------------

\section*{Administrivia (consult lecture slides)}
\begin{itemize}
\item Homework 1 will be out soon.
\item Deadlines for the final project: Feb 9, March 9, April 13.
\item Types of final projects.
\item Presentation date: March 28.
\end{itemize}

\section*{Types of Skeletal Muscle Motor Units}

Muscles are either categorized as cardiac, skeletal, or
smooth. Cardiac muscles can be found in the heart. They have an
intertwined fiber structure compared to the parallel fiber structure
of the skeletal muscles, and are more electrically coupled, such that
electric voltage propagates through them very fast. Smooth muscles are
more disorganized and are found in the eye or the intestines. Smooth
and cardiac muscles are controlled by the autonomic nervous
system. Skeletal muscles --- the subject of this section --- are
mostly connected to bones and have a parallel fiber structure.

Skeletal muscles consist of motor units --- the collection of a motor
neuron and skeletal muscle fibers. Depending on their constituting
fibers motor units fall into the following groups:
\begin{description}
\item[Slow]%
  These fibers have a slow reaction to the signals from the neuron,
  i.e., there is a relatively longer time interval between the time
  that the neuron starts firing and the time the fiber starts
  producing force. The same is also true when firing stops.

  Despite their slow reaction time, these motor units are very efficient
  in terms of energy, and are extremely fatigue resistant. Therefore,
  they can maintain a constant force over a long period of time.

  Slow motor units need a lot of oxygen. Therefore, they have a large
  amount of red blood cells, which gives them a dark shade. They can
  be found in the soleus muscle, a part of the calf, chicken
  legs, and duck breasts, postural muscles, etc, where a steady force
  has to be provided for a long period of time to do a routine task.
  
\item[Fast fatiguable]%
  These motor units have a very fast response to neuron firing, and
  can produce a much bigger force compared to slow muscles. On the
  other hand, these muscles can only maintain their force over a short
  period of time and get tired pretty quickly.

  Examples include the gastroc muscle, yet another part of the calf,
  and chicken breasts. These muscles are usually used in hunting or
  life-and-death situations.

\item[Fast fatigue-resistant]%
  These motor units have a combination of the properties of fast
  fatiguable and slow muscles.

\end{description}

Note that although each muscle is usually made up of all three types of motor
units, it does not necessarily have the same percentage of each motor
unit type.


\section*{Recruitment}

The produced force of a motor unit depends on the firing rate of its
neuron. The more the firing rate the more the produced force. However,
a motor unit has a specific limit and cannot produce more than a
certain force. Therefore, as the required force increases, the brain
starts activating more and more motor units in the muscle. This
process is called recruitment.

As depicted in \reffig{fig:recruitment-1} and
\reffig{fig:recruitment-3}, the brain first recruits muscles with
smaller force capacities, and as the required force increases the
brain starts deploying bigger muscles. This strategy is implemented
via the mechanism shown in \reffig{fig:recruitment-2}. Stronger
muscles have bigger neurons connected to them. A bigger neuron means a
larger capacitance, which in turn requires a higher firing rate to
produce the same current compared to a smaller neuron.

\section*{Biomechanics of Muscle Tissues}

As discussed in the previous lecture, \reffig{fig:force-vs-disp} shows
the result of a tensile testing performed on a muscle specimen. The
{\em total force} curve shows the force-displacement relation of an
active muscle, whereas the {\em passive force} curve shows the
force-displacement relation for a muscle which is not
activated. Subtracting the mentioned curves gives the {\em active
  force} curve, which is a constitutive model for the force produced
by the motor neurons as a function of displacement. This model can be
justified by realizing that the maximum of the curves corresponds to
the maximum interaction between the thick filaments and thin filaments
as shown in \reffig{fig:fiber}-a. While the depletion of the force for
smaller displacements is due to the interaction of the Z-disc with the
filaments, the decrease of the force for high displacements is due to
the reduction of the interaction of the mentioned filaments.

The mentioned model, however, has stability problems in numerical
simulations. Suppose that a muscle fiber is at equilibrium at the
maximum of the bell curve. If the muscle fiber goes under a small
displacement, the produced force will be reduced, which will cause
further displacement of the muscle, i.e., instability. {\em Sarcomere
  popping} is among the theories that has been developed for
explaining this phenomenon. However, evidence shows that sarcomere
popping does not occur.

\reffig{fig:paradox} shows another flaw of the bell curve constitutive
model. Suppose a muscle is at the length shown by the red dot in this
figure. If the muscle is stretched slightly and then activated, it
will produce the force shown by the cyan cross. However, if the muscle
is first activated and then stretched, it produces the force shown by
the red circle. This phenomenon is known as {\em residual force
  enhancement}, and cannot be explained by the hill-type model. It has
been discovered that a third type of protein, known as {\em titin},
which acts similar to a string, and is connected to the Z-disc and the
thick filament, contributes to the residual force enhancement
behaviour. Titin is shown if \reffig{fig:fiber}-b.

Evidence shows that an elastic solid model is not enough to model the
behaviour of the muscles. As shown in \reffig{fig:force-vs-velocity},
muscles also behave like a viscous fluid. The force produced by the
muscle can be affected by the the displacement rate, and the direction
of the displacement. When the muscle goes under tension, it quickly
reaches a maximum force, while the produced force slowly goes down as
the muscle goes under faster compression rates. This behaviour of the
muscle can be used to study arm wrestling more closely. It usually
takes a long time for an arm wrestling match to end. The reason is
that as a player starts to overcome his/her opponent, the muscle gets
shortened and the produced force gets reduced. On the other hand, when
a player is losing, the muscles stretch, and their strength will be
increased.

In the next session, a 1-D finite element formulation for the solution
of muscle models will be presented.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% FIGURES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{figure}[p]  
\centering
\includegraphics[width=0.95\linewidth]{img/motor-unit-types}
  \caption[Slow, fast fatigue-resistant, and fast fatiguable motor
  units.]{ Rows A, B, and C show the force response of slow, fast
    fatigue-resistant, and fast fatiguable motor units to input stimuli
    for different time scales. }
  \label{fig:motor-unit-types}
\end{figure}

\begin{figure}[p]  
\centering
\includegraphics[width=0.95\linewidth]{img/recruitment-1}
  \caption[Activation of multiple motor units in
  recruitment.]{Recruitment: the firing rate, produced force, and
    activated motor units are shown as the required output force
    increases. }
  \label{fig:recruitment-1}
\end{figure}

\begin{figure}[p]
  \centering
\includegraphics[width=0.6\linewidth]{img/recruitment-2}
\caption[Effect of neuron size in recruitment.]{Relation of the neuron
  size and motor unit power. The stronger the motor unit, the bigger
  the neuron.}
\label{fig:recruitment-2}
\vspace{1cm}

\includegraphics[width=0.6\linewidth]{img/recruitment-3}
\caption{Firing rate versus required force for muscles with different sizes.}
\label{fig:recruitment-3}
\end{figure}



\begin{figure}[p]  
  \centering
\includegraphics[width=0.8\linewidth]{img/force-vs-disp} \\
\caption{Force-displacement muscle constitutive model.}
\label{fig:force-vs-disp}
\vspace{1cm}

\includegraphics[width=0.6\linewidth]{img/paradox} \\
\caption{Residual force enhancement.}
\label{fig:paradox}
\end{figure}


\begin{figure}[p]  
\centering
\includegraphics[width=0.95\linewidth]{img/fiber-1} \\
\makebox[0.95\linewidth][c]{(a)}
\vspace{1cm}

\includegraphics[width=0.95\linewidth]{img/fiber-2} \\
\makebox[0.95\linewidth][c]{(b)}
\caption{Closer look at the proteins present in a muscle fiber. (a)
  The thick filament and the thin filament at a position where they
  have maximum contact and produce the maximum force. (b) Presence of
  titin which contributes to residual force enhancement.}
  \label{fig:fiber}
\end{figure}

\begin{figure}[p]  
\centering
\includegraphics[width=0.6\linewidth]{img/force-vs-velocity} 
\caption{Relation of force versus velocity for an average motor unit.}
  \label{fig:force-vs-velocity}
\end{figure}


\end{document}
