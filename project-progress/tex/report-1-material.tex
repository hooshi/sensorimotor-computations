%%==========================================================================
%%                             Introduction
%%==========================================================================

\section{Introduction}
\label{section-intro}

Simulation of Large deformations in solids is of interest in many
fields such as mechanical engineering, computer graphics, and
biomechanics. For example, muscle tissue is a soft material, and its
correct modeling, either for the purpose of physics based animation,
or biomechanics design, requires modeling of large deformations.

% Eulerian or lagrangian
An important challenge in simulation of large deformations is keeping
track of the solid and its motion. A Lagrangian formulation explicitly
follows each material particle. Thus, tracking of free surfaces
between different materials, and handling of constitutive models that
include the displacement values are tasks well suited for Lagrangian
methods. However, these methods may require complicated geometrical
and topological operations, such as remeshing, mesh untangling, and
collision detection. Eulerian frameworks, on the other hand, are
formulated using a fixed spacial grid. Although they are mostly
suitable for constitutive models that include rate of the
displacement, they can still be used for tracking of free surfaces,
albeit by introduction of additional variables such as reference maps
and/or level sets. Hybrid approaches are also possible, such as
Arbitrary Eulerian Lagrangian (ALE) \cite{donea1982arbitrary} and
Eulerian on Lagrangian (EoL) methods \cite{fan2013eulerian}.

% Contact can be more challenging, cite moose, talk about the
% analytical soltuion
Many industrially relevant problems include more than just a single
solid with specified boundaries. Rather, they include interactions
between different materials, such as collisions between solids, or
fluid-solid interactions. This quote from the documentations
\cite{MOOSEtuto} of the MOOSE \cite{gaston2015physics} finite-element
library (a physics solver developed at Idaho National Lab) shows how
challenging these problems can be: ``Development, testing, and
application testing of contact require many, many man-months of
effort. In fact, it is probably man years''. Solution methods to these
problems can be very different and heavily depend on the quantities of
interest. For example, solutions in the computer graphics community
usually tend to be fast and visually plausible at the cost of
accuracy, whereas engineering communities might be more interested in
accuracy at the cost of solution speed.

% This work, discretization simple finite difference
% The levels of the project
Levin \etal{} \cite{levin2011} introduced a robust yet not so
complicated Eulerian framework for the simulation of transient
frictionless solid contact in computer graphics. This Eulerian
framework was based on the earlier work of Kamrin \etal{}
\cite{kamrin2012}, and was later extended by Teng \etal{}
\cite{teng2016eulerian} to include fluid-solid interactions. The goal
of this work is the development of a C++ partial implementation of the
work of Levin \etal{} \cite{levin2011} and Kamrin \etal{}
\cite{kamrin2012}, and is divided into four main parts, the first of
which is the subject of this report.
\begin{enumerate}
\item Solution of steady-state problems with no free boundaries.
\item Solution of a model 1-D transient problem.
\item Solution of a transient problem of the movement of a single
  solid.
\item Solution of the transient interactions of two solids.
\end{enumerate}
The numerical method is described in section~\ref{section-method},
followed by some results in section~\ref{section-results}. Finally,
the future plan in elaborated in section~\ref{section-plan}.

%%==========================================================================
%%                             Methods
%%==========================================================================
\section{Numerical Method}
\label{section-method}

In this section, the problem statement and its solution method will be
presented.

%%==============================================
\subsection{Problem Statement}
%%==============================================
The problem of interest is the steady state solution of a deformed
solid under a specified boundary deformations. Given the shape of a
deformed solid $\Omega$ as shown in figure~\ref{figure-domain}-a,
%========Figure
\begin{figure}
  \centering
  \includegraphics[width=0.35\linewidth]{img/domain.pdf} \hfil
  \includegraphics[width=0.35\linewidth]{img/grid.pdf}  \\
  \makebox[0.35\linewidth][c]{(a)} \hfil
  \makebox[0.35\linewidth][c]{(b)}
  \caption{Schematic of (a) the domain that the solid occupies in the
    physical space (b) the computational grid}
  \label{figure-domain}
\end{figure}
%========
the problem is stated as the steady state solution of
\begin{equation}
  \begin{aligned}
    & \nabla \cdot T(\xi(x)) = 0 \quad &&x \in \Omega   \\
     \text{subject to} \qquad
    & \xi(x) = \xi_b(x) \quad &&x \in \partial \Omega,
  \end{aligned}
\end{equation}
where $\xi$ is the reference map (or material coordinate), $T$ is the
Cauchy stress tensor, $x$ is the physical coordinate,
$\partial \Omega$ is the boundary of the solid, and $\xi_b$ is the
prescribed boundary conditions. For consistency, the notation has been
adapted from \cite{kamrin2012}. The stress tensor will be evaluated as
a function of the left Cauchy-Green deformation tensor,
$B=\nabla \xi^{-1}(\nabla \xi^{-1})^T$, using the Levinson-Burgess
constitutive relation
\begin{equation}
  T = f_1(I_3)B+f_2(I_1,I_3)I.
\end{equation}
Here, $I$ is the identity matrix, while $I_1=\tr(B)$ and $I_3=\det(B)$
are the first and third invariants of $B$, respectively. Finally,
\[
  f_1= G \left( 3+\frac{1}{I_3} \right)\left( 4\sqrt{I_3}\right)^{-1}%
  \quad \text{and} \quad %
  f_2=G \left( %
    \sqrt{I_3}\left( %
      \frac{5}{6}+\frac{1-I_1}{4I_3^2}%
    \right)-\frac{4}{3} \right), %
\]
where $G$ is the shear modulus. In this specific problem, the value of
$G$ will not affect the steady-state solution.

Rather than approaching the steady-state problem directly, I will
solve the transient version of the problem until changes in solution
become negligible. Writing a code for the transient problem, allows an
easier generalization to truly transient problems. The governing
equations of the transient problem are
\begin{equation}
  \label{eq-transient}
    \begin{aligned}
      &\rho (v_t + v \cdot \nabla v) = \nabla \cdot T + \rho b%
      \quad &&x \in \Omega\\
      &\rho = \rho_0 \det(\nabla \xi)%
      \quad &&x \in \Omega\\
      &\xi_t + v \cdot \nabla \xi = 0%
      \quad && x \in  \Omega \\
      \text{subject to}  \qquad  &\xi(x,t) = \xi_b(x), \quad v(x,t) = 0 %
      \quad  && x \in \partial \Omega \\
      \text{and}  \qquad  &\xi(x,0) = \xi_0(x), \quad v(x,0)=0. %
  \end{aligned}
\end{equation}
where $v$ is the velocity, $t$ is time, $\rho=\det(\nabla \xi)$ is
density, $b$ is the vector of body forces, and the subscript $(.)_0$
represents initial conditions.

%%==============================================
\subsection{Discretization Scheme}
%%==============================================

To discretize the problem, the physical domain will be placed on a
structured grid, as shown in figure~\ref{figure-domain}-b. Then, the
velocity and reference map values will be solved for at every grid
point and time level. This process is addressed is three main steps:
finite-difference discretization of the spacial terms, time advance,
and ghost cell extrapolation.

\subsubsection{Finite-Difference Discretization of the Spacial Terms}
Finite-difference approximations are used in this work to evaluate a
discrete approximation of the differential operators in
equation~(\ref{eq-transient}). The velocity and the reference map
values are stored at the grid vertices, and the differential operators
are applied to them at the same locations. The advective terms
$v\cdot\nabla v$ and $v\cdot\nabla \xi$ are evaluated using a
first-order upwind difference scheme
\begin{multline}
  \label{eq-upwind}
  % https://en.wikipedia.org/wiki/Upwind_scheme
  ( v \cdot \nabla \chi )_{ij} = %
  \max(v_x,0) \frac{\chi_{i,j}-\chi_{i-1,j}}{\Delta x} 
  + \min(v_x,0) \frac{\chi_{i+1,j}-\chi_{i,j}}{\Delta x}  \\  
  + \max(v_y,0) \frac{\chi_{i,j}-\chi_{i,j-1}}{\Delta y} 
  + \min(v_y,0) \frac{\chi_{i,j+1}-\chi_{i,j}}{\Delta y} \quad %
  \chi=v_x,v_y,\xi.
\end{multline}
The gradient of the reference map is evaluated at the cell centers
using central difference formulas
\begin{equation}
  \label{eq-xicenter}
  ( \nabla \xi )_{i+\frac{1}{2},j+\frac{1}{2}} = %
  \begin{bmatrix}
    \frac{1}{2\Delta x}(\xi_{i+1,j}+\xi_{i+1,j+1}-\xi_{i,j}-\xi_{i,j+1})^T \\
    \frac{1}{2\Delta y}(\xi_{i,j+1}+\xi_{i+1,j+1}-\xi_{i,j}-\xi_{i+1,j})^T
  \end{bmatrix}^T,
\end{equation}
which can be used to find the density and the Cauchy stress tensor at
the cell centers. The density is then interpolated at the grid
vertices, while the divergence of the Cauchy stress tensor at the
vertices is again found using central difference formulas
\begin{multline}
  \label{eq-tcenter}
  ( \nabla \cdot T)_{i,j} = %
  \frac{1}{2 \Delta x}%
  ((T_x)_{i+\oot,j-\oot}+(T_x)_{i+\oot,j+\oot} %
  -(T_x)_{i-\oot,j-\oot}-(T_x)_{i-\oot,j+\oot}) \\
  \frac{1}{2 \Delta y}%
  ((T_y)_{i-\oot,j+\oot}+(T_y)_{i+\oot,j+\oot} %
  -(T_y)_{i-\oot,j-\oot}-(T_y)_{i+\oot,j-\oot}),
\end{multline}
where $T_x$ and $T_y$ are the first and second columns of the stress
matrix, respectively.

Combining equations (\ref{eq-transient})--(\ref{eq-tcenter}) by
introducing the differential operators $D_1$ and $D_2$, and neglecting
the boundary conditions, yields the following spacial discretization
of the governing equations
\begin{equation}
\begin{aligned}
  \frac{d v_h}{d t} = D_1(v_h,\xi_h) \\
  \frac{d \xi_h}{d t} = D_2(v_h,\xi_h),
\end{aligned}
\end{equation}
where $v_h$ and $\xi_h$ are the vector of the nodal velocity and
reference map values, respectively.

\subsubsection{Time Advance}
Time advance is finding the vector of nodal values at time level
$n+1$, given the vector of nodal values at time level $n$.  The main
method used in this work is the explicit Euler method
\begin{equation}
  \label{eq-euler}
  v_h^{n+1}=v_h^{n}+\Delta t D_1(v^n_h,\xi^n_h), \quad %
  \xi_h^{n+1}=\xi_h^{n}+\Delta t D_2(v^n_h,\xi^n_h),
\end{equation}
where the superscript $(.)^n$ is used to denote the corresponding time
level of a solution vector.

\subsubsection{Ghost Cell Extrapolation}

To allow using the finite-difference formulas at all the active grid
vertices without worrying about boundary conditions, the method of
ghost values is used. Assuming that the grid domain is large enough
that the nodes on its boundaries are more than a cell away from
$\Omega$, every vertex can be denoted either as active, ghost, or
inactive. Vertices inside $\Omega$ which are sufficiently far from
$\partial \Omega$, will be denoted as active. Here, I will use a
threshold of $\min(\Delta x/10, \Delta y/10)$ as a metric to determine
being sufficiently far. Then, any vertex which shares a cell with an
active vertex, will be denoted as a ghost. All the other vertices are
considered as inactive. The idea is to use equation~(\ref{eq-euler})
only for the active vertices, and then extrapolate the solution value
from the active vertices and the boundary conditions to find the
solution values at the ghost vertices. To achieve this, the ray from
every ghost vertex to the nearest boundary point is followed until it
intersects the first edge of the mesh whose vertices are both
active. The values of the solution at the nearest boundary point, and
the vertices of the mentioned edge are then used for extrapolation.
For an annular domain, figure~\ref{figure-ghost} shows the ghost
vertices, the rays cast from them to the closest boundary point, and
the first internal edge that they intersect.

\begin{figure}
  \centering
  \includegraphics[width=0.45\linewidth]{img/ghost-rays.pdf} \hfil
  \includegraphics[width=0.45\linewidth]{img/ghost-close.pdf}  \\
  \makebox[0.45\linewidth][c]{(a)} \hfil
  \makebox[0.45\linewidth][c]{(b)} 
  \caption{The rays that are used for extrapolating the solution
    values at ghost vertices: (a) all the rays (b) a closeup}
  \label{figure-ghost}
\end{figure}

%%==========================================================================
%%                             Results
%%==========================================================================
\section{Results}
\label{section-results}

The test results for verifying the implementation of the
finite-difference operators and the ghost cell extrapolation procedure
will be presented. In addition, an attempt has been made to solve the
internal displacement of a circular washer under shear, although the
implementation still has bugs, and is not fully functional.

%%==============================================
\subsection{Testing the Finite Difference Operators}
%% ==============================================

To test the implementation of the finite-difference operators, the
method of manufactured solution is used. The manufactured solution is
initialized as
\begin{equation}
  v =
  \begin{bmatrix}
    \cos(x)\cos(y) \\
    sin(2y)\sin(x)
  \end{bmatrix} \quad
  \xi=
  \begin{bmatrix}
     \cos(2x) \cos(y) \\
     \sin(2y) \cos(x)
  \end{bmatrix}.
\end{equation}
Then, the source terms $f_1$ and $f_2$,
\begin{equation}
    \begin{aligned}
      &f_1  = - v \cdot \nabla v + \frac{1}{\rho} \nabla \cdot T  \\
      &f_2   = - v \cdot \nabla \xi,
  \end{aligned}
\end{equation}
can be evaluated both analytically, e.g., Matlab's symbolic engine, and
using the mentioned finite-difference operators. The norm of the
difference of the values obtained using these two methods should
approach zero with a rate of $O(h)$, where $h$ is the mesh length
scale. Figure~\ref{figure-manufactured} shows the $L_\infty$ norm of
the mentioned difference for a series of refined meshes starting from
$11 \times 9$ on the domain $[0,1]\times[0,1]$. The desired order of
accuracy is attained verifying the implementation of this part of the
project. For simplicity, the source terms are not evaluated at the
grid boundaries. Also, a very simple stress model
$T=\begin{bmatrix} 1 &5 \\ 5 &9 \end{bmatrix} (\nabla \xi)^{-1}$ has
been used.

\begin{figure}
  \centering
  \includegraphics[width=0.5\linewidth]{img/convergence-fd-operators.pdf}
  \caption{Discretization error of the finite-difference operators as
    a function of mesh size for a manufactured solution.}
  \label{figure-manufactured}
\end{figure}


%%==============================================
\subsection{Testing the Ghost Cell Extrapolation Routine}
%% ==============================================
To test the ghost cell extrapolation routine, a circular solid will be
placed inside a regular grid. Then, a dummy scalar value $\phi(x,y)$
is defined, and evaluated at every vertex. The norm of the difference
between the extrapolated and exact $\phi$ values at the ghost vertices
should converge to zero with a rate of $O(h^2)$. Moreover, this
difference should be exactly zero for a linearly varying $\phi$. Using
a circle of radius $0.8$ centered at $(0,0)$ as the solid, and a
domain size of $[-1,1]\times[-1,1]$, the extrapolation error is
evaluated on a series of nested meshes starting from a mesh size of
$11 \times 9$. As table~\ref{table-ghost} shows, the error shrinks
with the expected quadratic rate.

\begin{table}
\centering
  \caption{$L_\infty$ norm of ghost extrapolation error for different
    scalar functions $\phi$}
  \label{table-ghost}
  \begin{tabular}{c c c c c}
    \cline{2-5}
    &\multicolumn{4}{c}{Mesh size} \\
    \hline
    $\phi$  &$11\times9$ &$22\times18$ &$44\times36$ &$88\times72$ \\
    \hline
    $8x - 12y + 2$ &$3.5\ee{-15}$ &$1.4\ee{-14}$ &$1.6\ee{-14}$ &$2.13\ee{-14}$\\
    $\sin(x^2) - \cos(\sinh(y))$ &$4.7\ee{-2}$ &$2.4\ee{-2}$ &$6.1\ee{-3}$ &$1.5\ee{-3}$ \\
    \hline
  \end{tabular}
\end{table}

%%==============================================
\subsection{Circular Washer Shear}
%% ==============================================
I have tried to solve the circular washer problem of Kamrin \etal{}
\cite{kamrin2012}. The reference and physical solid domains are both
an annulus with inner radius of $0.1$ and an outer radius of
$0.4$. The outer boundary is rotated $\frac{\pi}{6}$ radians
counter-clockwise, yielding the boundary conditions
\[
  \begin{aligned}
    &\xi_x = r\cos(\theta) \quad &&\xi_y = r\sin(\theta) \quad &r=0.1 \\
    &\xi_x = r\cos(\theta-\pi/6) \quad &&\xi_y = r\sin(\theta-\pi/6) \quad &r=0.4 , \\
  \end{aligned}
\]
where $(r,\theta)$ is the polar coordinates of a point on the
boundary. The initial condition for $v$ is zero everywhere, while
$\xi_0$ is evaluated by linearly interpolating the boundary
conditions.

Using a mesh size of $43\times43$ and a time step of
$\Delta t = 10^{-4}$, the residual norms follow the trends shown in
figure~\ref{figure-convergence}. Unfortunately, convergence is not
attained, which shows that my implementation has bugs. A possible
problem can be observed in figure~\ref{figure-bug} where the velocity
magnitude is shown after $10^5$ explicit Euler iterations. It seems
that the velocity has a very high magnitude of $20$ at exactly four
ghost vertices, while it is almost zero at all the other
locations. This implies that the ghost vertex extrapolation routine
might be at fault.

\begin{figure}
  \centering
  \includegraphics[width=0.5\linewidth]{img/convergence-history.pdf}
  \caption{Convergence history for the erroneous implementation of the
    circular washer problem }
  \label{figure-convergence}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=0.5\linewidth]{img/convergence-velocity-problem.png}
  \caption{Velocity magnitude of the solution for the circular washer
    problem after $10^5$ explicit Euler iterations.}
  \label{figure-bug}
\end{figure}

%%==========================================================================
%%                        Plan for completion
%%==========================================================================
\section{Plan for Completion}
\label{section-plan}
The next step would be to pin down the bugs in the steady state
solver, and finally solve the circular washer problem
correctly. Another interesting verification case to try is the stretch
of a circular disk into a triangle. Next part would be warming-up for
an actual transient problem by solving a 1-D compression wave problem
from the paper of Kamrin \etal{}.

Next, I plan to implement a transient solver for a single solid
(circular and/or rectangular shape) with free boundaries. The
challenge will mostly lie in handling the free boundaries and
implementing a more generalized extrapolation routine for them. For
handling the grid boundaries, two alternatives are available. The
easier possibility is using periodic boundary conditions, which of
course helps ending this part of the project faster. The other option
is implementing rigid wall boundaries that would bounce the solid back
at impact. Although, this option would be harder to implement, it can
act as a checkpoint in case the next part of the project does not go
well.

The last part of the project would be simulating two circular and/or
rectangular solids with collision. This requires adding a linear
non-penetrating constraint to the velocity discretization through a
collision detection and a surface integration routine. The Petsc/Tao
framework \cite{petsc-web-page} would be used as the backend numerical
solver for this constrained minimization problem.




%%% Local Variables:
%%% TeX-master: "report-0-main"
%%% End: