template <typename DATATYPE, Index_t BLOCKSIZE, StorageType STORAGETYPE>
DOFVector<DATATYPE, BLOCKSIZE, STORAGETYPE>::DOFVector(Grid &grid_in) : _grid(grid_in), _values()
{
  _values.resize(n_values() * block_size);
}

template <typename DATATYPE, Index_t BLOCKSIZE, StorageType STORAGETYPE>
DOFVector<DATATYPE, BLOCKSIZE, STORAGETYPE>::~DOFVector()
{
}

template <typename DATATYPE, Index_t BLOCKSIZE, StorageType STORAGETYPE>
void
DOFVector<DATATYPE, BLOCKSIZE, STORAGETYPE>::print_on_grid(FILE *fid, int block)
{
  for (int j = n_values(1) - 1; j >= 0; --j)
    {
      for (int i = 0; i < n_values(0); ++i)
        {
          fprintf(fid, "%11.3e ", value_at({ i, j }, block));
        }
      fprintf(fid, "\n");
    }
}


template <typename DATATYPE, Index_t BLOCKSIZE, StorageType STORAGETYPE>
DATATYPE &
DOFVector<DATATYPE, BLOCKSIZE, STORAGETYPE>::value_at(Index2d id, int block)
{
  const int i = id(0);
  const int j = id(1);
  EULSIM_ASSERT(i < n_values(0));
  EULSIM_ASSERT(j < n_values(1));
  EULSIM_ASSERT(block < block_size);
  return _values[j * n_values(0) * block_size + i * block_size + block];
}

template <typename DATATYPE, Index_t BLOCKSIZE, StorageType STORAGETYPE>
const DATATYPE &
DOFVector<DATATYPE, BLOCKSIZE, STORAGETYPE>::value_at(Index2d id, int block) const
{
  const int i = id(0);
  const int j = id(1);
  EULSIM_ASSERT(i < n_values(0));
  EULSIM_ASSERT(j < n_values(1));
  EULSIM_ASSERT(block < block_size);
  return _values[j * n_values(0) * block_size + i * block_size + block];
}


template <typename DATATYPE, Index_t BLOCKSIZE, StorageType STORAGETYPE>
int
DOFVector<DATATYPE, BLOCKSIZE, STORAGETYPE>::n_values() const
{
  return n_values(0) * n_values(1);
}


template <typename DATATYPE, Index_t BLOCKSIZE, StorageType STORAGETYPE>
int
DOFVector<DATATYPE, BLOCKSIZE, STORAGETYPE>::n_values(int dim) const
{
  switch (storage_type)
    {
      case STORAGE_CELLS: return grid().n_cells(dim);
      case STORAGE_VERTICES: return grid().n_verts(dim);
      case STORAGE_EDGES_X: return grid().n_edges(dim, 0);
      case STORAGE_EDGES_Y: return grid().n_edges(dim, 1);
    }
}

template <typename DATATYPE, Index_t BLOCKSIZE, StorageType STORAGETYPE>
Real
DOFVector<DATATYPE, BLOCKSIZE, STORAGETYPE>::origin(int dim) const
{
  switch (storage_type)
    {
      case STORAGE_CELLS: return grid().cell_center_xyz({ 0, 0 })(dim);
      case STORAGE_VERTICES: return grid().vertex_xyz({ 0, 0 })(dim);
      case STORAGE_EDGES_X: return grid().edge_center_xyz({ 0, 0 }, 0)(dim);
      case STORAGE_EDGES_Y: return grid().edge_center_xyz({ 0, 0 }, 1)(dim);
    }
}

template <typename DATATYPE, Index_t BLOCKSIZE, StorageType STORAGETYPE>
template <typename DATATYPE2>
void
DOFVector<DATATYPE, BLOCKSIZE, STORAGETYPE>::make_equal_to(DOFVector<DATATYPE2, BLOCKSIZE, STORAGETYPE> &other)
{
  EULSIM_FORCE_ASSERT(&grid() == &other.grid());
  for (unsigned int i = 0; i < _values.size(); ++i)
    {
      get_values()[i] = (DATATYPE)other.get_values()[i];
    }
}


template <typename DATATYPE, Index_t BLOCKSIZE, StorageType STORAGETYPE>
void
DOFVector<DATATYPE, BLOCKSIZE, STORAGETYPE>::set(const DataType &v)
{
  std::fill(_values.begin(), _values.end(), v);
}

template <typename DATATYPE, Index_t BLOCKSIZE, StorageType STORAGETYPE>
void
DOFVector<DATATYPE, BLOCKSIZE, STORAGETYPE>::set_axby(const double a, const CurrentType &x, const double b,
                                                      const CurrentType &y)
{
  EULSIM_FORCE_ASSERT(&x._grid == &this->_grid);
  EULSIM_FORCE_ASSERT(&y._grid == &this->_grid);
  for (arma::uword i = 0; i < _values.size(); ++i)
    {
      this->_values[i] = a * x._values[i] + b * y._values[i];
    }
}


template <typename DATATYPE, Index_t BLOCKSIZE, StorageType STORAGETYPE>
const Grid &
DOFVector<DATATYPE, BLOCKSIZE, STORAGETYPE>::grid() const
{
  return _grid;
}

template <typename DATATYPE, Index_t BLOCKSIZE, StorageType STORAGETYPE>
Grid &
DOFVector<DATATYPE, BLOCKSIZE, STORAGETYPE>::grid()
{
  return _grid;
}

template <typename DATATYPE, Index_t BLOCKSIZE, StorageType STORAGETYPE>
std::vector<DATATYPE> &
DOFVector<DATATYPE, BLOCKSIZE, STORAGETYPE>::get_values()
{
  return _values;
}

template <Index_t BLOCKSIZE, StorageType STORAGETYPE>
typename DOFVector_R<BLOCKSIZE, STORAGETYPE>::VectorType
DOFVector_R<BLOCKSIZE, STORAGETYPE>::vec_at(Index2d id) const
{
  VectorType ans;
  for (uint i = 0; i < BaseType::block_size; ++i)
    {
      ans(i) = this->value_at(id, i);
    }
  return ans;
}

template <typename DATATYPE, Index_t BLOCKSIZE, StorageType STORAGETYPE>
void
DOFVector<DATATYPE, BLOCKSIZE, STORAGETYPE>::read_ascii(std::string fname)
{
  std::fstream fstr(fname, std::fstream::in);
  EULSIM_FORCE_ASSERT_MSG(fstr.is_open(), "File " << fname << " could not be found.");

  for (int i = 0; i < n_values(0); ++i)
    for (int j = 0; j < n_values(1); ++j)
      for (int b = 0; b < block_size; ++b)
        {
          fstr >> value_at({ i, j }, b);
        }
}

template <typename DATATYPE, Index_t BLOCKSIZE, StorageType STORAGETYPE>
void
DOFVector<DATATYPE, BLOCKSIZE, STORAGETYPE>::write_ascii(std::string fname)
{
  std::fstream fstr(fname, std::fstream::out);
  EULSIM_FORCE_ASSERT_MSG(fstr.is_open(), "File " << fname << " could not be found.");

  for (int i = 0; i < n_values(0); ++i)
    for (int j = 0; j < n_values(1); ++j)
      for (int b = 0; b < block_size; ++b)
        {
          fstr << std::scientific << std::setprecision(16) << value_at({ i, j }, b) << "\n";
        }
  fstr << "\n";
  fstr << "# Block size: " << block_size << "\n";
  fstr << "# NX:         " << n_values(0) << "\n";
  fstr << "# NY:         " << n_values(1) << "\n";
}

template <Index_t BLOCKSIZE, StorageType STORAGETYPE>
void
DOFVector_R<BLOCKSIZE, STORAGETYPE>::set_vec_at(Index2d id, const VectorType &v)
{
  for (uint i = 0; i < BaseType::block_size; ++i)
    {
      this->value_at(id, i) = v(i);
    }
}

template <Index_t BLOCKSIZE, StorageType STORAGETYPE>
void
DOFVector_R<BLOCKSIZE, STORAGETYPE>::add_vec_at(Index2d id, const VectorType &v)
{
  for (uint i = 0; i < BaseType::block_size; ++i)
    {
      this->value_at(id, i) += v(i);
    }
}

template <Index_t BLOCKSIZE, StorageType STORAGETYPE>
void
DOFVector_R<BLOCKSIZE, STORAGETYPE>::get_norms(double *l1, double *l2, double *linf) const
{
  NormCalculator nc;

  for (std::vector<Real>::const_iterator it = this->_values.begin(); it != this->_values.end(); ++it)
    {
      nc.add_value(*it);
    }
  nc.norms(l1, l2, linf);
}
