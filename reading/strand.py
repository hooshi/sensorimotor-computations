#!/usr/bin/env python3

# coding: utf-8

# ### Finite Element Modeling of Simplest 1D strand in 2D
# 
# #### CPSC 530P Sensorimotor Computation, Spring 2018  
# #### Dinesh K. Pai, UBC
# 
# Large deformation non-linear elastostatics, Lagrangian discretization  
# Worked example using dense matrices for illustration; for real world implementations, dealing with sparsity is key  
# 

# In[ ]:

import numpy as np
import matplotlib.pyplot as plt
import math
from scipy.optimize import minimize

def plot_strand(x):
    plt.plot(x[:,0],x[:,1],'-o')
    plt.axis('equal')
    plt.grid()
    plt.xlabel('$x^1$')
    plt.ylabel('$x^2$')
    plt.show()


# Problem specs

# In[ ]:

n = 10 # number of elements
L = 0.1 # length of the strand in meters (we'll use SI units)


# Lagrangian (Material Space) discretization
# 
# Assume strand occupies $\Omega = [0,1] * L$ in material space
# 
# Use a mesh of n+1 nodes (with n elements)
# So the strand extends from $X_0 = 0$ to $X_{n} = L$

# In[ ]:

X = np.transpose([np.linspace(0,L,n+1)])
# Can use arbitrary mesh too! E.g., 
# X = np.transpose([[0, 0.0025, 0.005, 0.01, 0.02, 0.04, 0.06, 0.08, 0.09, 0.095, 0.1]])
#np.shape(X)


# Initialize x to X, but embedded in 2D

# In[ ]:

x0 = np.append(X,  np.zeros((n+1,1)), axis = 1)


# In[ ]:

plot_strand(x0)


# #### Strain Energy
# 

# In[ ]:

# We'll build up this function interactively first... 
#def W (X,x):
# n = np.size(X)-1 # number of elements

## constants and material properties
G = np.array([[0], [-9.81]]) # acceleration due to gravity, m/s^2. Note (2,1) array
PCSA = 1e-4 # physiological cross-section area, m^2
RHO = 1e3 # density, approximately of water. kg/m^3
# so 
print('total mass of strand =', PCSA*L*RHO, 'kg')

MU = 1e-4 # stifffness/Lame' constant. J/m^3 
#This is very small, for illustration. Real muscle is much stiffer.


# In[ ]:

# test deformation 1
x = 2 * x0
plot_strand(x)


# In[ ]:

# test deformation 2
u = L/10*(1 + np.sin(np.linspace(0,2*math.pi,n+1)))
plt.plot(u)
plt.grid()
plt.title('displacement')
plt.figure() # new figure
x = x0 + np.transpose([u])
plot_strand(x)


# In[ ]:

np.transpose([u])


# In[ ]:

## Difference matrix
D = np.diag(np.ones(n+1)) - np.diag(np.ones(n),-1) #square matrix, backward difference
#D = np.transpose(D[1:,:]) # remove extra first row, transpose for later convenience
D = D[1:,:] # remove extra first row
print(D, '\n', np.shape(D))


# In[ ]:

## Differential kinematics
DX = D @ X
Dx = D @ x
print('DX:\n', DX)
print('Dx:\n', Dx)


# In[ ]:

# Deformation gradients for each element.
# Note: using * and not @
F = Dx * (1/DX) # Dx * DX^{-1} in this special case has a simple form

print(F)


# In[ ]:

# right Cauchy-Green tensor
C = np.diag(F @ np.transpose(F)) # inefficient, but this is just to illustrate ideas..
# note that since we're storing coordinates as rows, this is computing F^T F per element
print(C)


# In[ ]:

# Green strain
Eg = C-1

print(Eg)


# In[ ]:

# St.VK (St. Venant - Kirchoff) Material (behaves well under large stretch,
# but poorly under compression)
We = 0.5*MU*(Eg @ np.transpose(Eg))

print(We)


##### Gravitational Potential Energy

# In[ ]:

## Element center matrix
B = np.diag(np.ones(n+1)) + np.diag(np.ones(n),-1) #square backward difference
B = 0.5 * B[1:,:]
print(B, '\n', np.shape(B))


# In[ ]:

# center of mass of each element
c = B @ x

print(c)
plt.plot(c[:,0],c[:,1],'o')
plot_strand(x)


# Gravity potential is also defined so that f = - dW/dx
# So potential should increase with height, W = - mass x accn due to gravity x height

# In[ ]:

Wg = np.sum(- RHO*PCSA*DX * (c @ G))

print(Wg)


#### Total potential energy of strand

# In[1]:

def W (X,x):
    ## Differential kinematics
    DX = D @ X
    Dx = D @ x
    
    # Deformation gradients for each element.
    # Note: using * and not @
    F = Dx * (1/DX) # Dx * DX^{-1} in this special case has a simple form
    
    # right Cauchy-Green tensor
    C = np.diag(F @ np.transpose(F)) # inefficient, but this is just to illustrate ideas..
    # note that since we're storing coordinates as rows, this is computing F^T F per element
    
    # Green strain
    Eg = C-1
    
    # St.VK (St. Venant - Kirchoff) Material (behaves well under large stretch,
    # but poorly under compression)
    We = 0.5*MU*(Eg @ np.transpose(Eg))
    
    # #### Gravitational Potential Energy
    
    # center of mass of each element
    c = B @ x
    
    # Gravity potential is also defined so that f = - dW/dx
    # So potential should increase with height, W = - mass x accn due to gravity x height
    Wg = np.sum(- RHO*PCSA*DX * (c @ G))
    
    # ### Total potential energy of strand
    
    return We + Wg


# In[ ]:

# check
print(We+Wg,W(X,x0))
print(We+Wg,W(X,x))


# #### Case 1: Boundary condition: fixed-free, fixed at left node
# 
# x1 defines the configuration of the strand, given this BC. These define
# the independent variables for our optimization.
# 
# x1 is the submatrix of x after eliminating the first column of x
# 

# In[ ]:

# Energy function with only non-fixed nodes as variables
x_left = [x0[0,:]]
def W1 (x1): 
    # TODO remove hack to reshape array due to scipy bug!
    return W(X, np.append(x_left, np.reshape(x1, (n, 2)), axis = 0))


# In[ ]:

# Initial guess
x10 = x0[1:,:]
# perturb to get out local minimum, if needed
x10


# In[ ]:

x1opt = minimize(W1, x10, method = 'BFGS', options={'disp': True, 'maxiter': 50000})
xopt = np.append(x_left, np.reshape(x1opt.x, (n, 2)), axis = 0)
plot_strand(xopt)


# #### Case 2: Boundary condition: fixed-fixed
# 
# x2 is the configuration variable, with both ends removed.  It is the submatrix of x after eliminating the two end columns
# 

# In[ ]:

x_right = [x0[n,:]]
def W2 (x2): 
    # TODO remove hack to reshape array due to scipy bug!
    return W(X, np.concatenate((x_left, np.reshape(x2, (n-1, 2)), x_right), axis = 0))


# In[ ]:

# Initial guess
x20 = x0[1:n,:]
# perturb to get out local minimum, if needed
x20


# In[ ]:

x2opt = minimize(W2, x20, method = 'BFGS', options={'disp': True, 'maxiter': 50000})
xopt = np.concatenate((x_left, np.reshape(x2opt.x, (n-1, 2)), x_right), axis = 0)
plot_strand(xopt)


# In[ ]:



