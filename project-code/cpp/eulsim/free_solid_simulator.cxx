#include <eulsim/bridson_utils.hxx>
#include <eulsim/free_solid_simulator.hxx>
#include <eulsim/geometry.hxx>

namespace eulsim
{

FreeSolidSimulator::FreeSolidSimulator(Grid &grid_in,                              //
                                       FreeSolid &solid_in,                        //
                                       std::vector<const Obstacle *> obstacles_in, //
                                       const SimulationInfo info_in)
  : _info(info_in)
  , _grid(grid_in)
  , _solid(solid_in)
  , _obstacles(obstacles_in)
  , _mass(grid_in)
  , _divstressarea(grid_in)
  , _laplacianksiarea(grid_in)
  , _nablavarea(grid_in)
  , _euler_buffer(grid_in)
  , _extrapolation_v_buffer(grid_in)
  , _extrapolation_is_valid_buffer(grid_in)
{
}

void
FreeSolidSimulator::apply_initial_conditions(SolutionState *state)
{
  for (Index2d vid = grid().first_vertex(); !grid().is_end_vertex(vid); grid().advance_vertex(vid))
    {
      Vec2r v, k;
      solid().initial_solution(grid().vertex_xyz(vid), &v, &k);
      state->v.set_vec_at(vid, v);
      state->k.set_vec_at(vid, k);
    } // End of for
  compute_mass(state->k, &state->is_valid, &_mass);
  extrapolate(state->v, state->is_valid, 2);
}


void
FreeSolidSimulator::extrapolate(DOFVector_2RV &velocity, DOFVector_1CV &is_valid, const int n_layers)
{
  Geometry_extrapolate_closest_point(velocity, _extrapolation_v_buffer, is_valid, _extrapolation_is_valid_buffer,
                                     n_layers);
}

bool
FreeSolidSimulator::is_mass_at(const DOFVector_2RV &refmap, const Vec2r pos)
{
  const Vec2r ksi = bridson::interpolate_value(pos, refmap);
  return solid().is_in_refmap(ksi);
}

// C++ boilerplate code for function binding
struct WRAPPER_is_mass_at : public ImplicitFunction
{
  WRAPPER_is_mass_at(FreeSolidSimulator &father, DOFVector_2RV &refmapin) : fa(father), refmap(refmapin) {}
  double
  eval(Vec2r pos)
  {
    return (fa.is_mass_at(refmap, pos) ? -1 : 1);
  }

private:
  FreeSolidSimulator &fa;
  DOFVector_2RV &refmap;
};

ImplicitFunction *
FreeSolidSimulator::is_mass_at_functor(DOFVector_2RV &refmap)
{
  return new WRAPPER_is_mass_at(*this, refmap);
}

double
FreeSolidSimulator::rho_at(const DOFVector_2RV &refmap, const Vec2r pos)
{
  Vec2r ksi;
  Mat2r ksi_grad;
  if (is_mass_at(refmap, pos))
    {
      bridson::interpolate_gradient(ksi, ksi_grad, pos, refmap);
      // printf("%lf \n", arma::det(ksi_grad) * solid().material().rho_0() );
      return arma::det(ksi_grad) * solid().material().rho_0();
    }
  else
    {
      return 0;
    }
}

// C++ boilerplate code for function binding
struct WRAPPER_rho_at : public ImplicitFunction
{
  WRAPPER_rho_at(FreeSolidSimulator &father, DOFVector_2RV &refmapin) : fa(father), refmap(refmapin) {}
  double
  eval(Vec2r pos)
  {
    // printf("%lf \n", fa.rho_at(refmap, pos) );
    return fa.rho_at(refmap, pos);
  }

private:
  FreeSolidSimulator &fa;
  DOFVector_2RV &refmap;
};

void
FreeSolidSimulator::compute_mass(DOFVector_2RV &refmap, DOFVector_1CV *is_valid, DOFVector_1RV *mass)
{
  const int SUBSAMPLE_LEVEL = 3;
  const double MASS_TOL = 1e-8;

  WRAPPER_rho_at fn_value(*this, refmap);
  WRAPPER_is_mass_at fn_isin(*this, refmap);

  _total_mass = 0;
  for (Index2d vid = grid().first_vertex(); !grid().is_end_vertex(vid); grid().advance_vertex(vid))
    {
      double mass_local;
      if (grid().is_boundary_vertex(vid))
        mass_local = 0;
      else
        mass_local = Geometry_subsample_via_voxels(grid(), vid, fn_value, fn_isin, SUBSAMPLE_LEVEL);

      //EULSIM_FORCE_ASSERT_MSG(mass_local >= 0, "Mass is " << mass_local );

      if (mass_local < MASS_TOL)
        is_valid->value_at(vid) = 0;
      else
        is_valid->value_at(vid) = 1;

      _total_mass += mass_local;

      if (mass)
        mass->value_at(vid) = mass_local;
    }
}


void
FreeSolidSimulator::compute_rhs_velocity(SolutionState &state, DOFVector_1RV &mass, DOFVector_2RV *rhs)
{
  const Real dx = grid().delta(0);
  const Real dy = grid().delta(1);
  const int nx = grid().n_verts(0);
  const int ny = grid().n_verts(1);
  const double darea = dx * dy;
  const Vec2r gravity = { 0, -_info.g };

  //
  // First loop over cells and integrate some handy information
  // Here we use interim vector buffecrs
  //
  _nablavarea.set(0);
  _divstressarea.set(0);
  _laplacianksiarea.set(0);

  //
  for (Index2d cid = grid().first_cell(); !grid().is_end_cell(cid); grid().advance_cell(cid))
    {
      const Vec2r cell_pos = grid().cell_center_xyz(cid);

      const Index2d vid_0 = { cid(0), cid(1) };
      const Index2d vid_1 = { (cid(0) + 1), cid(1) };
      const Index2d vid_2 = { (cid(0) + 1), (cid(1) + 1) };
      const Index2d vid_3 = { cid(0), (cid(1) + 1) };

      // Ignore non valid vertices
      // if ((!state.is_valid.value_at(vid_0))    //
      //     && (!state.is_valid.value_at(vid_1)) //
      //     && (!state.is_valid.value_at(vid_2)) //
      //     && (!state.is_valid.value_at(vid_3)))
      //   {
      //     continue;
      //   }

      // No stress if the point is outside the solid
      // TODO: Must do subsampling for surface integral
      if (is_mass_at(state.k, cell_pos))
        {
          Mat2r dksidx, F;
          Vec2r ksi;
          bridson::interpolate_gradient(ksi, dksidx, cell_pos, state.k);
          // std::cout << dksidx << std::endl;
          const bool is_invertible = arma::inv(F, dksidx);
          EULSIM_FORCE_ASSERT_MSG(is_invertible, "dksi: " << dksidx << " not invertible");
          const Mat2r matT = solid().material().stress(F);
          // std::cout << arma::vectorise(matF - matT).t() ;

          // Assemble the divT vector
          const Vec2r Tcol0dy = matT.col(0) * dy * 0.5;
          const Vec2r Tcol1dx = matT.col(1) * dx * 0.5;
          _divstressarea.add_vec_at(vid_0, +Tcol0dy + Tcol1dx);
          _divstressarea.add_vec_at(vid_1, -Tcol0dy + Tcol1dx);
          _divstressarea.add_vec_at(vid_2, -Tcol0dy - Tcol1dx);
          _divstressarea.add_vec_at(vid_3, +Tcol0dy - Tcol1dx);

          // std::cout << arma::vectorise( matT ).t();
        }

    } // End of integrating divstressarea

  //
  // Now loop over vertices, find v . \nabla v and the rhs
  //
  for (Index2d vid = grid().first_vertex(); !grid().is_end_vertex(vid); grid().advance_vertex(vid))
    {
      // Ignore non valid vertices, and boundary vertices
      if ((!state.is_valid.value_at(vid)) || grid().is_boundary_vertex(vid))
        {
          rhs->set_vec_at(vid, { 0, 0 });
          continue;
        }

      const Vec2r v = state.v.vec_at(vid);

      //
      // Find nablavarea here rahter than the cell iteration
      //
      {
        const Vec2r vr = state.v.vec_at({ vid(0) + 1, vid(1) });
        const Vec2r vl = state.v.vec_at({ vid(0) - 1, vid(1) });
        const Vec2r vu = state.v.vec_at({ vid(0), vid(1) + 1 });
        const Vec2r vb = state.v.vec_at({ vid(0), vid(1) - 1 });
        Mat2r nablavarea;
        nablavarea.col(0) = (v(0) < 0 ? (v - vl) : (vr - v)) * dy;
        nablavarea.col(1) = (v(1) < 0 ? (v - vb) : (vu - v)) * dx;
        //nablavarea.col(0) =  (vr - vl)  / 2. * dy;
        //nablavarea.col(1) =  (vu - vb)  / 2. * dx;
        
        _nablavarea.set_vec_at(vid, arma::vectorise(nablavarea));
      }

      const double massvertex = mass.value_at(vid);
      const Mat2r nablav = arma::reshape(_nablavarea.vec_at(vid), 2, 2) / darea;
      const Vec2r divTarea = _divstressarea.vec_at(vid);

      const Vec2r rhsofthisvertex = massvertex * (-nablav * v + gravity) + divTarea;

      rhs->set_vec_at(vid, rhsofthisvertex);
    }
} // ALl done rhs velocity

void
FreeSolidSimulator::compute_rhs_refmap(SolutionState &state, DOFVector_2RV *rhs)
{
  const int nx = grid().n_verts(0);
  const int ny = grid().n_verts(1);
  const Real dx = grid().delta(0);
  const Real dy = grid().delta(1);
  const int dimx = 0;
  const int dimy = 1;

  // Loop over all vertices and find the rhs
  // If the vertex is not active then ignore it.
  for (Index2d vid = grid().first_vertex(); !grid().is_end_vertex(vid); grid().advance_vertex(vid))
    {
      // Ignore non valid vertices
      if ((!state.is_valid.value_at(vid)) || grid().is_boundary_vertex(vid))
        {
          rhs->set_vec_at(vid, { 0, 0 });
          continue;
        }

      // Get adjacent vertex handles
      const Index2d vid_r = { (vid(0) + 1) % nx, vid(1) };
      const Index2d vid_l = { (vid(0) - 1 + nx) % nx, vid(1) };
      const Index2d vid_t = { vid(0), (vid(1) + 1) % ny };
      const Index2d vid_b = { vid(0), (vid(1) - 1 + ny) % ny };

      // Get adjacent refmap and velocity
      const Vec2r ksi_c = state.k.vec_at(vid);
      const Vec2r ksi_r = state.k.vec_at(vid_r);
      const Vec2r ksi_l = state.k.vec_at(vid_l);
      const Vec2r ksi_t = state.k.vec_at(vid_t);
      const Vec2r ksi_b = state.k.vec_at(vid_b);
      //
      const Vec2r vel_c = state.v.vec_at(vid);
      const Vec2r vel_r = state.v.vec_at(vid_r);
      const Vec2r vel_l = state.v.vec_at(vid_l);
      const Vec2r vel_t = state.v.vec_at(vid_t);
      const Vec2r vel_b = state.v.vec_at(vid_b);

      // Get the derivative of the refmap at cell centers by
      const Vec2r dksidx_c = (vel_c(0) > 0 ? ksi_c - ksi_l : ksi_r - ksi_c) / dx;
      const Vec2r dksidy_c = (vel_c(1) > 0 ? ksi_c - ksi_b : ksi_t - ksi_c) / dy;

      // Set the rhs
      rhs->value_at(vid, 0) = -vel_c(dimx) * dksidx_c(0) - vel_c(dimy) * dksidy_c(0);
      rhs->value_at(vid, 1) = -vel_c(dimx) * dksidx_c(1) - vel_c(dimy) * dksidy_c(1);
    }
} // All done rhs ksi

void
FreeSolidSimulator::update_velocity(SolutionState *state, arma::mat *update_norm)
{
  NormCalculator nc;

  // assumption mass is already calculated and velocity is already extrapolated
  compute_rhs_velocity(*state, _mass, &_euler_buffer);

  for (Index2d vid = grid().first_vertex(); !grid().is_end_vertex(vid); grid().advance_vertex(vid))
    {  
      // EULSIM_FORCE_ASSERT( _mass.value_at(vid) >= 0 );
      if (_mass.value_at(vid) < 1e-12)
        {
          state->v.set_vec_at(vid, { 0,0 });
          state->is_valid.value_at(vid) = 0;
        }
      else
        {
          const double mass = _mass.value_at(vid);
          const Vec2r vold = state->v.vec_at(vid);
          const Vec2r rhs = _euler_buffer.vec_at(vid);
          const Vec2r update = _info.dt / mass * rhs;
          const Vec2r vnew = vold + update;
          nc.add_value(update(0));
          nc.add_value(update(1));
          state->v.set_vec_at(vid, vnew);
        }
    }

  update_norm->resize(1, 3);
  nc.norms(&update_norm->at(0, 0), &update_norm->at(0, 1), &update_norm->at(0, 2));
}

void
FreeSolidSimulator::update_refmap(SolutionState *state, arma::mat *update_norm)
{
  NormCalculator nc;

  // velocity is already extrapolated
  compute_rhs_refmap(*state, &_euler_buffer);

  for (Index2d vid = grid().first_vertex(); !grid().is_end_vertex(vid); grid().advance_vertex(vid))
    {
      if (!state->is_valid.value_at(vid))
        {
          // their rhs is already zero
          // What should be done? Don't touch
          // state->k.set_vec_at(vid, { 0, 0 });
        }
      else
        {
          const Vec2r kold = state->k.vec_at(vid);
          const Vec2r rhs = _euler_buffer.vec_at(vid);
          const Vec2r update = _info.dt * rhs;
          const Vec2r knew = kold + update;
          nc.add_value(update(0));
          nc.add_value(update(1));
          state->k.set_vec_at(vid, knew);
        }
    }

  update_norm->resize(1, 3);
  nc.norms(&update_norm->at(0, 0), &update_norm->at(0, 1), &update_norm->at(0, 2));
}

void
FreeSolidSimulator::update_all(SolutionState *state, arma::mat *update_norm)
{
  arma::mat update_norm_v, update_norm_k;

  compute_mass(state->k, &state->is_valid, &_mass);
  extrapolate(state->v, state->is_valid, 10);
  update_velocity(state, &update_norm_v);
  extrapolate(state->v, state->is_valid, 10);
  update_refmap(state, &update_norm_k);

  if (update_norm)
    {
      update_norm->resize(2, 3);
      update_norm->row(0) = update_norm_v;
      update_norm->row(1) = update_norm_k;
    }
}


} // End of namespace eulsim