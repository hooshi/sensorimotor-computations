/** predicates.hxx
 *
 * Predicates for shoving indices(2D) into std containers.
 * Now that I think of it, I wrote all these template stuff to avoid a simple for loop!!!!!
 */

#ifndef EULSIM_PREDICATES_HXX
#define EULSIM_PREDICATES_HXX

#include <utility>
#include <vector>

#include <eulsim/defs.hxx>

namespace eulsim
{

template <uint N>
class IsIndexEqual
{
  typedef arma::Col<Index_t>::fixed<N> argument_type;
  typedef bool result_type;
  const argument_type &_index;

public:
  IsIndexEqual(const argument_type &in) : _index(in) {}

  result_type
  operator()(const argument_type &in) const
  {
    for (uint i = 0; i < N; ++i)
      {
        if (_index(i) != in(i))
          return false;
      }
    return true;
  }
};

template <uint N>
class IsIndexLesserOrdered
{
  typedef arma::Col<Index_t>::fixed<N> argument_type;
  typedef bool result_type;

public:
  result_type
  operator()(const argument_type &a, const argument_type &b) const
  {
    for (uint i = 0; i < N; ++i)
      {
        if (a(i) != b(i))
          return a(i) < b(i);
      }
    return false;
  }
};

template <uint N>
class IsIndexLesserUnordered
{
  typedef arma::Col<Index_t>::fixed<N> argument_type;
  typedef bool result_type;

public:
  result_type
  operator()(argument_type a, argument_type b) const
  {
    for (uint i = 0; i < N; ++i)
      {
        std::sort(a.begin(), a.end());
        std::sort(b.begin(), b.end());
        return IsIndexLesserOrdered<N>(a, b);
      }
    return false;
  }
};

template <typename T1, typename T2, uint IDX>
class IsPairMemberLesser
{
};

template <typename T1, typename T2>
class IsPairMemberLesser<T1, T2, 0> 
{
  typedef std::pair<T1, T2> argument_type;

public:
  bool
  operator()(const argument_type &a, const argument_type &b) const
  {
    return a.first < b.first;
  }
};

template <typename T1, typename T2>
class IsPairMemberLesser<T1, T2, 1> 
{
  typedef std::pair<T1, T2> argument_type;

public:
  bool
  operator()(const argument_type &a, const argument_type &b) const
  {
    return a.second < b.second;
  }
};



template <uint N>
class VectorFind
{
  typedef arma::Col<Index_t>::fixed<N> index_type;
  typedef std::vector<index_type> vector_type;
  typedef typename vector_type::iterator iterator_type;
  typedef typename vector_type::const_iterator const_iterator_type;

public:
  iterator_type
  operator()(vector_type &vector, const index_type &v)
  {
    IsIndexEqual<N> predicate(v);
    return std::find_if(vector.begin(), vector.end(), predicate);
  }

  const_iterator_type
  operator()(const vector_type &vector, const index_type &v)
  {
    IsIndexEqual<N> predicate(v);
    return std::find_if(vector.begin(), vector.end(), predicate);
  }
};

} // namespace eulsim

#endif /*EULSIM_PREDICATES_HXX*/
