clear
close all

domain = [-3 3];
% domain = [-5 5];
alpha = 0.02;
t_end = 50;
offset = -2;
method = "upwind_fd";
% method = "center";
stretch = 0.1;
v0 = 0.;
g = 0.;
C = 10;
params = sfb.default_params();
msize=400;
x = linspace(domain(1), domain(2), msize); x = x(:);
params.dx = (domain(2)- domain(1))/(msize-1);
params.dt = alpha * params.dx ;
params.x = x;
params.solid = sfb.Solid(1, v0, stretch, [-1 1], C, 1);
params.n_sample = 32;
write_step = 0.025;
n_steps = round(write_step / params.dt);
params.dt = write_step / double(n_steps);
params.g = g;

params.advm = method;
sfb.check_params(params);

[v,xi] = params.solid.initial_solution(x);
[v2,xi2] = params.solid.initial_solution(x);

%params.barriers{end+1}= sfb.Barrier(1.2,  1);
%params.barriers{end+1}= sfb.Barrier(-2,  0);

t = 0;
step = 0;
while t < t_end
    
    % [v,xi] = wave.advance_euler(v, xi, params);
    % mass = sfb.compute_mass_nleq(x, xi, params);
    [mass, J, n_contact] = sfb.compute_mass_nleq(x, xi, params);
    % mass = sfb.compute_mass_nleq(x, xi, params);
    is_valid = (mass > 0);
    v = sfb.extrapolate(v, is_valid);
    %
    
    %
    % Solve for v
    % Unconstrained
    if( n_contact == 0)
        rhs_v = sfb.rhs_v_fd(v, xi, mass, is_valid, params);
        v(is_valid) = v(is_valid) + params.dt * rhs_v(is_valid) ./ mass(is_valid);
    else % Constrained        
        
        %[~, col] = find(J);
        %is_valid(coI was talking about the figure in a response to ``I don't think we need to do anything for he3 and he6 in this diagraml) = 1;
        indices = 1:msize;
        indices_valid = indices(is_valid);
        rhs_v = sfb.rhs_v_fd(v, xi, mass, [], params);
        
        v_valid = v( indices_valid );
        n_valid = size(v_valid, 1);
        rhs_v_valid = rhs_v( indices_valid );
        J_valid = J(n_contact, indices_valid);
        M_valid = mass( indices_valid );
        rhs_v_star = params.dt .* rhs_v_valid + M_valid .* v_valid;
        M_valid = spdiags(M_valid, [0], n_valid, n_valid);
        b =  zeros(n_contact,1);
        
        %size(M_valid)
        %size(rhs_v_star)
        %size(J_valid)
        %size(b)
        %M_valid
        %b
        %rhs_v_star
        % J
        % J_valid
        options = optimoptions('quadprog', 'Display', 'off');
        % options = [];
        v_valid = quadprog(M_valid, -rhs_v_star, J_valid,  b, [], [], [], [], [], options);
        % size( indices_valid )
        % size(v_valid)
        v( indices_valid ) = v_valid;
    end
    
    %
    v = sfb.extrapolate(v, is_valid);
    rhs_xi = sfb.rhs_xi_fd(v, xi, params);
    xi = xi + params.dt * rhs_xi;
    %
    t = t + params.dt;
    
    %
    %
    %
    % [v,xi] = wave.advance_euler(v, xi, params);
    % mass2 = sfb.compute_mass_nleq(x, xi2, params);
    % mass = sfb.compute_mass_nleq(x, xi, params);
    % is_valid2 = (mass2 > 0);
    % v2 = sfb.extrapolate(v2, is_valid2);
    %
    % rhs_v2 = sfb.rhs_v_fd(v2, xi2, mass2, params);
    % v2(is_valid) = v2(is_valid) + params.dt * rhs_v2(is_valid) ./ mass2(is_valid);
    %
    % v2 = sfb.extrapolate(v2, is_valid2);
    % rhs_xi2 = sfb.rhs_xi_fd(v2, xi2, params);
    % xi2 = xi2 + params.dt * rhs_xi2;
    if( (mod(t, write_step) < 1e-7) | ...
            (abs(mod(t, write_step)-write_step) < 1e-7 ) )
        hold off, plot(x, v, 'b', 'linewidth', 2);
        hold on, plot(x, xi, 'r', 'linewidth', 2);
        hold on, plot(x, mass ./ params.dx, 'k', 'linewidth', 2);
        % hold on, plot(x, mass2, 'r', 'linewidth', 2);
        % hold on, plot(x, is_valid, 'k', 'linewidth', 2);
        fprintf("Total mass: %.6f \n", sum(mass)); 
        ylim([-4 4])
        for j=1:length(params.barriers)
            hold on, plot([params.barriers{j}.x, params.barriers{j}.x], [-2, 2], 'm--');
        end
        data = {};
        data{1} = t;
        data{2} = x;
        data{3} = v;
        data{4} = xi;
        data{5} = mass;   
        save(sprintf('out/bounce/%d.dat', step),'data');
        pause(0.0005)
        step = step+1;
    end

end

