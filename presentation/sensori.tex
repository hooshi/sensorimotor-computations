% Copyright 2004 by Till Tantau <tantau@users.sourceforge.net>.
%
% In principle, this file can be redistributed and/or modified under
% the terms of the GNU Public License, version 2.
%
% However, this file is supposed to be a template to be modified
% for your own needs. For this reason, if you use this file as a
% template and not specifically distribute it as part of a another
% package/program, I grant the extra permission to freely copy and
% modify this file as you see fit and even to delete this copyright
% notice. 

\documentclass[xcolor={usenames,dvipsnames}]{beamer}
\beamertemplatenavigationsymbolsempty

% path to graphics

% write code
\usepackage{listings}
\usepackage{graphicx}
\usepackage{caption}
\usepackage[export]{adjustbox}
\usepackage{algorithm,algpseudocode}
\usepackage{amsmath}
\usepackage{bm}


\definecolor{mblue}{RGB}{0, 0, 125}
\definecolor{mgreen}{RGB}{0, 100, 0}
\definecolor{mred}{RGB}{200, 0, 0}

% THEME
\usetheme{Frankfurt}
\usecolortheme{seagull}

%% Show the page number
\setbeamertemplate{footline}[frame number]
\setbeamercolor{footline}{fg=black}
%% No header
\addtobeamertemplate{frametitle}{\vspace*{-0.9\baselineskip}}{}

\title{Eulerian Simulation of Large Deformations}

% A subtitle is optional and this may be deleted
%\subtitle{}

\author{Shayan Hoshyari}

\institute[University of British Columbia]{}

\date{April, 2018}

% subject for pdf file
\subject{CPSC530P}

% add some logo and stuff
% \pgfdeclareimage[height=1cm]{remesherlogo}{../image/logo.jpg}
% \logo{\pgfuseimage{remesherlogo}}

\newcommand{\vvec}[1]{\boldsymbol{\mathbf{#1}}}
\newcommand{\mmat}[1]{\bm{#1}}

% Let's get started
\begin{document}

% ****************************************************************************
% *                             Title page
% ****************************************************************************
\begin{frame}[plain]
  \titlepage
\end{frame}

% ****************************************************************************
% *                             Motivation
% ****************************************************************************
\begin{frame}{Some Applications}
  \begin{enumerate}
  \item<1-> Biomechanical Engineering
  \item<2-> Muscle Animation
  \item<3-> Structural Engineering
  \end{enumerate}

  \vspace{1cm}
  \begin{overlayarea}{\linewidth}{5cm}
    \only<1>{
      \centering
      \includegraphics[width=0.4\linewidth]{img/biomechanics.jpg}
    }
    \only<2>{
      \centering
      \includegraphics[width=0.4\linewidth]{img/muscle-animation-1.jpg}
      \includegraphics[width=0.4\linewidth]{img/muscle-animation-2.png}
    }
    \only<3>{
      \centering
      \includegraphics[width=0.4\linewidth]{img/structural-1.png}
      \includegraphics[width=0.4\linewidth]{img/structural-2.png}
    }
  \end{overlayarea}
\end{frame}

% ****************************************************************************
% *                             A very general problem
% ****************************************************************************
\begin{frame}{A very general problem}
  \begin{overlayarea}{\linewidth}{5cm}
  \centering
  \includegraphics<1>[width=0.7\linewidth]{img/general-problem.pdf}
  \includegraphics<2->[width=0.7\linewidth]{img/general-problem-grid.pdf}
  \end{overlayarea}

  \begin{enumerate}
  \item<3-> Fluid variables: level set $\phi$, pressure $p$,
    velocity $v$, (density $\rho$)
  \item<4-> Solid variables:  velocity $v$, reference map $\xi$
  \end{enumerate}
\end{frame}

% ****************************************************************************
% *                             A very general problem
% ****************************************************************************
\begin{frame}{A more specific problem}
  \begin{block}{Eulerian Solid Simulation with Contact}
    \small
    D. I. Levin , J. Litven, G. L. Jones, S. Sueda, D. K. Pai,
    Siggraph 2011
  \end{block}
      
  \begin{center}
    \includegraphics<2->[width=0.7\linewidth]{img/specific-problem.pdf}
  \end{center}
  
  \begin{itemize}
  \item<3-> No clamped boundaries.
  \item<4-> Frictionless contact.
  \item<5-> Large deformations.
  \item<6-> This presentation: formulation in 1-D.
  \end{itemize}
\end{frame}

% ****************************************************************************
% *                                 Equations
% ****************************************************************************
\begin{frame}{Equations}
  \only<1-2>{
  \[
     \frac{\partial}{\partial t}%
    \begin{bmatrix} 
      \rho             \\
      \rho v           \\
    \end{bmatrix} %
    + \nabla \cdot %
    \begin{bmatrix}
      \rho v^T            \\
      \rho vv^T - \sigma  \\
    \end{bmatrix} % 
    =  \begin{bmatrix} %
      0            \\
      \rho g  \\
    \end{bmatrix} %
  \]
  }
  \only<3-4>{
  \[
     \frac{\partial}{\partial t}%
    \begin{bmatrix} 
      \rho             \\
      \rho v           \\
      \textcolor{mred}{\rho \xi}         \\
    \end{bmatrix} %
    + \nabla \cdot %
    \begin{bmatrix}
      \rho v^T            \\
      \rho vv^T - \sigma  \\
      \textcolor{mred}{\rho \xi v^T}
    \end{bmatrix} % 
    =  \begin{bmatrix} %
      0            \\
      \rho g       \\
      \textcolor{mred}{0}   \\
    \end{bmatrix} %
  \]
  }
  \only<5->{
    \[
      \begin{aligned}
      &\rho v_t + (v\cdot\nabla)v  = \nabla \cdot \sigma + \rho g \\
      &\xi_t + (v\cdot\nabla)\xi  = 0 \\
      &\rho = \rho_0 \det( \nabla\xi) \\
      \end{aligned}
      \]
  }

  \begin{itemize}
    \item<1-> Conservation of mass and momentum.
  \item<2->{Fluids: $\sigma=\sigma(\nabla v)$.}
  \item<3->{Solids: $\sigma=\sigma(F=(\nabla \xi)^{-1})$.}
  \item<4->{No need to solve for $\rho$ anymore, $\rho\det(F) = \rho_0 $}
  \item<6-> Let's use a very simple stress model: $\sigma=\kappa(\sqrt{F^TF}-1)$, $\kappa=10$.
    \item<7-> In 1-D: $\sigma=\kappa(\frac{1}{\xi_x}-1)$
  \end{itemize}
  
\end{frame}

% ****************************************************************************
% *                           Discretization
% ****************************************************************************
\begin{frame}{Discretization}
  %% Images
  \only<1>{\fbox{\includegraphics[width=0.98\linewidth]{img/discretization-plain.pdf}}}
  \only<2>{\fbox{\includegraphics[width=0.98\linewidth]{img/discretization-v.pdf}}}
  \only<3>{\fbox{\includegraphics[width=0.98\linewidth]{img/discretization-xi.pdf}}}
  \only<4>{\fbox{\includegraphics[width=0.98\linewidth]{img/discretization-omega.pdf}}}
  \only<5->{\fbox{\includegraphics[width=0.98\linewidth]{img/discretization-plain-small.pdf}}}
  %% Formulas
  \only<5>{\[
    \begin{cases}
      &\rho v_t = -\rho v v_x + \sigma_x + \rho g \\
      &\xi_t = -v\xi_x
    \end{cases}
  \]}
\only<6>{\[
    \begin{cases}
      &\int_{\Omega_i}\left( \rho v_t = -\rho v v_x + \sigma_x + \rho g \right) \\
      &\xi_t = -v\xi_x
    \end{cases}
  \]}
\only<7->{\[
    \begin{cases}
      & m_i \frac{dv_i}{dt} = -m_i (v v_x)_i + (\sigma_{i+1/2}-\sigma_{i-1/2}) + m_i g + O(\Delta x) \\
      &\frac{d\xi_i}{dt} = -(v\xi_x)_i
    \end{cases}
  \]}
\begin{itemize}
\item<8-> $m_i$: subsampling, bisection, non-linear solve, etc.
\item<9-> $(vv_x)_i = \max(v_i,0)\frac{v_i - v_{i-1}}{\Delta x} + \min(v_i,0)\frac{v_{i+1}-v_i}{\Delta x}$
\item<10-> $(\xi_x)_{i+1/2}=\frac{\xi_{i+1}-\xi_{i}}{\Delta x}$
\item<11-> $\sigma_{i+1/2}=\begin{cases} \sigma((\xi_x)_{i+1/2}) &x_{i+1/2} \text{ inside solid} \\ 0 &\text{otherwise} \end{cases}$
\end{itemize}

\end{frame}

% ****************************************************************************
% *                           Discretization
% ****************************************************************************
\begin{frame}{Discretization---Continued}
  \only<1->{\begin{block}{System of ODEs}
      \[
        \begin{aligned}
          \mmat{M}\vvec{v}_t = \vvec{R}_v(\vvec{v},\vvec{\xi}) \\
          \vvec{\xi}_t = \vvec{R}_\xi(\vvec{v},\vvec{\xi})
        \end{aligned}
      \]
    \end{block}}
    \begin{overlayarea}{\linewidth}{3cm}
  \only<2->{\begin{block}{Explicit Euler}
      \begin{itemize}
      \item<2-> Find $\mmat M$
      \item<3-> $ \mmat{M}\vvec{v}^{(t+\Delta t)} = \mmat{M}\vvec{v}^{(t)} + \Delta t \vvec{R}_v(\vvec{v}^{(t)},\vvec{\xi}^{(t)}) = \vvec{R}_{v*}(\vvec{v}^{(t)},\vvec{\xi}^{(t)})$
      \item<4-> Extrapolate $\vvec v^{(t+\Delta t)}$ to all the domain.
      \item<5-> $ \vvec{\xi}^{(t+\Delta t)} = \vvec{\xi}^{(t)} + \Delta t\vvec{R}_\xi(\vvec{v}^{(t+\Delta t)},\vvec{\xi}^{(t)})$
      \end{itemize}
    \end{block}}
  \end{overlayarea}
\end{frame}

  
% ****************************************************************************
% *                              Squeezing an object
% ****************************************************************************
\begin{frame}{Squeezing an Object}
  \begin{columns}[T] % align columns
    \begin{column}{.65\textwidth}
      \fbox{\includegraphics[width=0.98\linewidth]{img/squeeze.pdf}}
    \end{column}%
    \begin{column}{.30\textwidth}
      \vspace{1.5cm}
      \[
        \begin{aligned}
          &v(x, t=0) = 0 \\
          &\xi(x, t=0) = 10x
        \end{aligned}
      \]
      
      Solution: \href{https://h00shi.github.io/FILES/sensori_pres/bounce.gif}{\beamergotobutton{Link}}
    \end{column}%
  \end{columns}
\end{frame}

% ****************************************************************************
% *                          Collision Handling
% ****************************************************************************
\begin{frame}{Collision}
  \begin{center}
    \fbox{\includegraphics[width=0.75\linewidth]{img/collision.pdf}}
  \end{center}
  \begin{itemize}
  \item<2-> Constraint: $g_t=v_b\cdot n_b+v_s\cdot n_s \leq 0$
  \item<3-> Matrix form: $\mmat J \vvec v \leq \vvec b$
  \item<4-> Old time advance: $\mmat{M}\vvec{v}^{(t+\Delta t)}=\vvec{R}_{v*}(\vvec{v}^{(t)},\vvec{\xi}^{(t)})$
  \item<5-> New time advance: %
    $\vvec{v}^{(t+\Delta t)}= \arg\min_{\vvec{v}}(\frac{1}{2}\vvec{v}^T\mmat{M}\vvec{v} + \vvec{R}^T_{v*}\vvec{v}), \text{ subject to } \mmat{J}\vvec{v} \leq \vvec{b}$
  \end{itemize}
\end{frame}

% ****************************************************************************
% *                          Collision Example
% ****************************************************************************
\begin{frame}{Collision---Example}
  \begin{center}
  \fbox{\includegraphics[width=0.75\linewidth]{img/sandwich.pdf}}
\end{center}

\begin{minipage}{0.45\linewidth}
  \[
    \begin{aligned}
      &v(x, t=0) = 1 \\
      &\xi(x, t=0) = x
    \end{aligned}
  \]
\end{minipage}
\begin{minipage}{0.45\linewidth}
  \begin{itemize}
  \item Solution: \href{https://h00shi.github.io/FILES/sensori_pres/sandwich_2.gif}{\beamergotobutton{Link}} 
  \item Mesh dependence:  \href{https://h00shi.github.io/FILES/sensori_pres/sandwich_1.gif}{\beamergotobutton{Link}}
  \end{itemize}
\end{minipage}

\end{frame}

% ****************************************************************************
% *                          Challenges in Higher-Dimensions
% ****************************************************************************
\begin{frame}{Challenges in Higher-Dimensions}
  \begin{itemize}
  \item<1-> Finding $M$ can be more complicated.
  \item<2-> Integrating $Jv \leq b$ requires surface reconstruction
    and collision detection.
  \item<3-> Velocity extrapolation needs finding the closest point to
    the surface.
  \item<4-> The advection terms $(v\cdot\nabla)(.)$ should be
    discretized carefully.
  \end{itemize}
\end{frame}

\end{document}