#!/usr/bin/env python3

import pandas
import matplotlib.pyplot as plt
import numpy as np

def process(filename):
    plt.figure(0)
    plt.clf()
    data = pandas.read_csv(filename+'.csv')
    xi = np.array([data['xi:0'],data['xi:1'],data['xi:2']]).transpose()
    xi = np.linalg.norm(xi, axis=1)
    x = np.array(data['Points:0'])
    l1, = plt.plot(x, data['density'],  color='k')
    l2, = plt.plot(x, xi, color='r' )
    l3, = plt.plot(x, data['v:0'], color='b')
    plt.ylim([0, 2])
    plt.xlabel('x')
    plt.gca().set_aspect('equal')
    return (l1,l2,l3)
    #print(data)
    
(density, xi, v) = process('run-1-t0')
plt.legend([v, xi, density], ['v',r'$\|xi\|$',r'$\rho$'], loc='upper left')
plt.savefig('run-1-t0.svg', bbox_inches='tight')

process('run-1-t20')
plt.gca().get_yaxis().set_ticks([])
plt.savefig('run-1-t20.svg', bbox_inches='tight')

process('run-1-t40')
plt.gca().get_yaxis().set_ticks([])
plt.savefig('run-1-t40.svg', bbox_inches='tight')
