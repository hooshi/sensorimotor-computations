#define GETPOT_NAMESPACE pot
#include <contrib/getpot.hxx>
#include <contrib/visit_writer.h>

#include <eulsim/clamped_solid_simulator.hxx>
#include <eulsim/io.hxx>
#include <eulsim/solid.hxx>

using namespace eulsim;

class LevingsonBurgess : public Material
{
  double _kappa, _rho0;

public:
  LevingsonBurgess(double rho0in = 1., double kappain = 1.) : _rho0(rho0in), _kappa(kappain) {}

  double
  rho_0() override
  {
    return _rho0;
  }

  Mat2r
  stress(Mat2r &F) override
  {
    const Mat2r B = F * F.t();
    const double I1 = arma::trace(B);
    const double I3 = arma::det(B);
    const double f1 = _kappa * (3 + 1. / I3) / 4. / sqrt(I3);
    const double f2 = _kappa * sqrt(I3) * (5. / 6 + (1. - I1) / (4 * I3 * I3)) - 4. / 3 * _kappa;
    return f1 * B + f2 * arma::eye(arma::size(B));
  }
};

class CircularWasher : public ClampedSolid
{
  double _r0, _r1, _outer_rot, _inner_rot;
  Material &_material;

public:
  CircularWasher(double r0in, double r1in, double thetain, Material &materialin)
    : _r0(r0in), _r1(r1in), _inner_rot(thetain), _material(materialin), _outer_rot(0)
  {
  }

  void
  project_point(const Vec2r &pt, SurfacePoint *spt) override
  {
    int surface = -100000;
    double t = -10000;

    const double r = arma::norm(pt);

    if (r < (_r0 + _r1) / 2.)
      surface = 0;
    else
      surface = 1;

    t = atan2(pt(1), pt(0));

    spt->t = t;
    spt->surface_id = surface;
  }

  Vec2r
  surface_point_xy(const SurfacePoint &spt) override
  {
    switch (spt.surface_id)
      {
        case 0: return { _r0 * cos(spt.t), _r0 * sin(spt.t) };
        case 1: return { _r1 * cos(spt.t), _r1 * sin(spt.t) };
        default: EULSIM_FORCE_ASSERT_MSG(0, "YOU ARE DOOMED");
      }
  }

  void
  boundary_solution(const Real /* time */, const SurfacePoint &spt, Vec2r *velocity, Vec2r *refmap) override
  {
    if (velocity)
      {
        *velocity = { 0, 0 };
      } // End of velocity
    if (refmap)
      {
        switch (spt.surface_id)
          {
            case 0:
              {
                const double theta0 = spt.t - _inner_rot;
                *refmap = { _r0 * cos(theta0), _r0 * sin(theta0) };
                break;
              }
            case 1:
              {
                const double theta0 = spt.t;
                *refmap = { _r1 * cos(theta0), _r1 * sin(theta0) };
                break;
              }
            default: EULSIM_FORCE_ASSERT_MSG(0, "YOU ARE DOOMED");
          }
      } // End of refmap
  }     // All done

  bool
  is_in_initially(const Vec2r &pt) override
  {
    return (arma::norm(pt) >= _r0) && (arma::norm(pt) <= _r1);
  }


  void
  initial_solution(const Vec2r &pt, Vec2r *velocity, Vec2r *refmap) override
  {
    EULSIM_FORCE_ASSERT(is_in_initially(pt));


    if (velocity)
      {
        *velocity = { 0, 0 };
      } // End of velocity
    if (refmap)
      {
        const double r = arma::norm(pt);
        const double theta = atan2(pt(1), pt(0));
        const double rotation = _inner_rot * (1 - (r - _r0) / (_r1 - _r0));
        const double theta0 = theta - rotation;
        *refmap = { r * cos(theta0), r * sin(theta0) };
      } // End of refmap
  }     // All done


  bool
  is_in_refmap(const Vec2r &refmap) override
  {
    return is_in_initially(refmap);
  }

  Material &
  material() override
  {
    return _material;
  }

  void
  write_vtk(const std::string filename)
  {
    // number of points per circle
    const int n_pts_per = 500;
    const int n_pts = n_pts_per * 2;
    std::vector<float> pts(n_pts * 3);
    std::vector<int> connectivity(n_pts * 2);
    std::vector<int> cell_types(n_pts, VISIT_LINE);

    // Outer circle
    for (int i = 0; i < n_pts_per; ++i)
      {
        const float theta = float(i) / n_pts_per * 2 * EULSIM_PI;
        const float x = float(_r1) * cosf(theta);
        const float y = float(_r1) * sinf(theta);
        pts[i * 3 + 0] = x;
        pts[i * 3 + 1] = y;
        pts[i * 3 + 2] = 0;
        connectivity[i * 2 + 0] = i;
        connectivity[i * 2 + 1] = (i + 1) % n_pts_per;
      }

    // Inner circle
    for (int i = 0; i < n_pts_per; ++i)
      {
        const float theta = float(i) / n_pts_per * 2 * EULSIM_PI;
        const float x = float(_r0) * cosf(theta);
        const float y = float(_r0) * sinf(theta);
        pts[3 * n_pts_per + i * 3 + 0] = x;
        pts[3 * n_pts_per + i * 3 + 1] = y;
        pts[3 * n_pts_per + i * 3 + 2] = 0;
        connectivity[2 * n_pts_per + i * 2 + 0] = i + n_pts_per;
        connectivity[2 * n_pts_per + i * 2 + 1] = (i + 1) % n_pts_per + n_pts_per;
      }


    VISIT_write_unstructured_mesh(filename.c_str(), 0, n_pts, pts.data(), n_pts, cell_types.data(), connectivity.data(),
                                  0, NULL, NULL, NULL, NULL);
  }
};


int
driver_error_measure(int argc, char *argv[])
{
  pot::GetPot getpot(argc, argv);

  const double r0 = getpot("r0", double(0.1));
  const double r1 = getpot("r1", double(0.4));
  const double inner_rotation = getpot("theta", double(EULSIM_PI / 6.));
  const double dt = getpot("dt", double(0.01));
  const double gravity = getpot("g", double(0));
  const double kappa = getpot("kappa", double(1));
  const double damping = getpot("mu", double(0));
  std::string ifname = getpot.follow("XXX", 1, "-i");

  // Read grid
  Grid grid(1, 1, { -1, 1 }, { -1, 1 });
  grid.read_ascii(ifname + ".grid");
  grid.write_stream(std::cout);

  // Create the solid
  LevingsonBurgess material(1., kappa);
  CircularWasher solid(r0, r1, inner_rotation, material);

  ClampedSolidSimulator::SimulationInfo simulation_info;
  simulation_info.dt = dt;
  simulation_info.damping_velocity = damping;
  simulation_info.g = gravity;

  ClampedSolidSimulator simulator(grid, solid, simulation_info);
  simulator.init();

  ClampedSolidSimulator::SolutionState solution(grid), error(grid);


  solution.k.read_ascii(ifname + ".ref");
  solution.v.read_ascii(ifname + ".vel");

  error.v.make_equal_to(solution.v);
  error.k.set(0);

  const double A = -6.25 / 6 / 93.75 * EULSIM_PI;
  const double B = -1.00 / 6 / 93.75 * EULSIM_PI;

  NormCalculator normv, normksi;

  for (Index2d vid = grid.first_vertex(); !grid.is_end_vertex(vid); grid.advance_vertex(vid))
    {
      if (simulator.vertex_status(vid).vtype != ClampedSolidSimulator::VERTEX_ACTIVE)
        continue;

      Vec2r pos = grid.vertex_xyz(vid);
      const double r = arma::norm(pos);
      const double theta = atan2(pos(1), pos(0));
      const double dtheta = A - B / r / r;
      const double theta0 = theta - dtheta;
      const double ksix = r * cos(theta0);
      const double ksiy = r * sin(theta0);

      const double ksix_fd = solution.k.value_at(vid, 0);
      const double ksiy_fd = solution.k.value_at(vid, 1);
      const double vx_fd = solution.v.value_at(vid, 0);
      const double vy_fd = solution.v.value_at(vid, 1);

      error.k.value_at(vid, 0) = EULSIM_ABS(ksix_fd - ksix);
      error.k.value_at(vid, 1) = EULSIM_ABS(ksiy_fd - ksiy);

      normksi.add_value(EULSIM_ABS(ksix_fd - ksix));
      normksi.add_value(EULSIM_ABS(ksiy_fd - ksiy));
      normv.add_value(vx_fd);
      normv.add_value(vy_fd);
    }

  simulator.write_vtk_solution_str(ifname + ".error.vtk", &error);

  double l1, l2, linf;
  normv.norms(&l1, &l2, &linf);
  fprintf(stderr, "%15.6e %15.6e %15.6e \n", l1, l2, linf);
  normksi.norms(&l1, &l2, &linf);
  fprintf(stderr, "%15.6e %15.6e %15.6e \n", l1, l2, linf);

  return 0;
}

int
driver_solver(int argc, char *argv[])
{
  pot::GetPot getpot(argc, argv);

  const double nx = getpot("nx", int(20));
  const double ny = getpot("ny", int(20));
  const double r0 = getpot("r0", double(0.1));
  const double r1 = getpot("r1", double(0.4));
  const double inner_rotation = getpot("theta", double(EULSIM_PI / 6.));
  const double dt = getpot("dt", double(0.01));
  const double gravity = getpot("g", double(0));
  const int nt = getpot("nt", int(100));
  const double kappa = getpot("kappa", double(1));
  const double damping = getpot("mu", double(0));
  std::string ifname = getpot("iname", "");
  const int do_first_order_velocity_extrapolation = getpot("ep1v", 0);


  std::string ofname = (ifname == "" ? getpot("oname", "circularwasher") : ifname);
  {
    int ono = IO_latest_number_on_disk_for_filename(ofname + ".grid");
    ofname = IO_enumerate_filename(ofname, ono);
  }

  const double dxapprox = 2. * r1 / nx;
  const double dyapprox = 2. * r1 / ny;
  const int offset = 2;

  // Printf information
  printf("theta=%lf \n", inner_rotation);

  Grid grid(nx, ny, { -r1 - offset * dxapprox, -r1 - offset * dyapprox },
            { r1 + offset * dxapprox, r1 + offset * dyapprox });
  if (ifname != "")
    {
      grid.read_ascii(ifname + ".grid");
    }
  grid.write_stream(std::cout);
  grid.write_ascii(ofname + ".grid");

  LevingsonBurgess material(1., kappa);

  CircularWasher solid(r0, r1, inner_rotation, material);

  ClampedSolidSimulator::SimulationInfo simulation_info;
  simulation_info.dt = dt;
  simulation_info.damping_velocity = damping;
  simulation_info.g = gravity;
  simulation_info.do_first_order_velocity_extrapolation = do_first_order_velocity_extrapolation;

  ClampedSolidSimulator simulator(grid, solid, simulation_info);
  simulator.init();

  ClampedSolidSimulator::SolutionState solution(grid);
  if (ifname != "")
    {
      solution.k.read_ascii(ifname + ".ref");
      solution.v.read_ascii(ifname + ".vel");
    }
  else
    {
      simulator.apply_initial_conditions(&solution);
    }


  // Write the mesh
  solid.write_vtk(ofname + ".boundary.vtk");
  simulator.write_vtk_ghost_rays(ofname + ".ghost-rays.vtk");
  simulator.write_vtk_solution_str(ofname + ".solution-0.vtk", &solution);
  simulator.write_vtk_solution_unstr(ofname + ".solutionu-0.vtk", &solution);

  //
  // DEBUGGING FOR THE EXTRAPOLATION
  //
  if (0)
    {
      simulator.extrapolate_values(0, solution.v, ClampedSolidSimulator::VAR_VELOCITY);
      simulator.extrapolate_values(0, solution.k, ClampedSolidSimulator::VAR_REFMAP);
      Index2d vbad({ 22, 42 });
      // For 22 42 print its ghost information
      ClampedSolidSimulator::VertexGhostStatus &stat = simulator.vertex_status(vbad);
      Vec2r velbad = solution.v.vec_at(vbad);
      Vec2r vel0;
      solid.boundary_solution(0, stat.p0, &vel0, NULL);
      Vec2r vel1 = solution.v.vec_at(stat.p1);
      Vec2r vel2 = solution.v.vec_at(stat.p2);


      printf("bad velocity: %lf %lf \n", velbad(0), velbad(1));
      printf("bad vertex p0: %d %lf %lf %lf %lf \n", stat.p0.surface_id, stat.p0.t, stat.w0, vel0(0), vel0(1));
      printf("bad vertex p1: %d %d  %lf %lf %lf \n", stat.p1(0), stat.p1(1), stat.w1, vel1(0), vel1(1));
      printf("bad vertex p2: %d %d  %lf %lf %lf \n", stat.p2(0), stat.p2(1), stat.w2, vel2(0), vel2(1));
    }

  // Print norm of initial conditions
  {
    double init_l1, init_l2, init_linf;
    solution.v.get_norms(&init_l1, &init_l2, &init_linf);
    printf("Initial of velocity: %.5g \n", init_l2);
    solution.k.get_norms(&init_l1, &init_l2, &init_linf);
    printf("Initial of refmap  : %.5g \n", init_l2);
  }

  {
    std::ofstream hstfile(ofname + ".hst", std::fstream::out | std::fstream::binary);
    hstfile << "# dt = " << simulator.info().dt << "\n";
    hstfile << "# mu = " << simulator.info().damping_velocity << "\n";
    hstfile << "# g = " << simulator.info().g << "\n";

    for (int i = 0; i < nt; ++i)
      {

        arma::mat residual_norms;
        simulator.update_rk3(&solution, &residual_norms);
        // simulator.update_euler(&solution, &residual_norms);

        std::cerr << i << std::scientific << std::setprecision(6)               //
                  << " " << residual_norms(1, 0) << " " << residual_norms(1, 1) //
                  << " " << residual_norms(1, 2) << " " << residual_norms(1, 3) << "\n";

        hstfile << i << std::scientific << std::setprecision(6)               //
                << " " << residual_norms(1, 0) << " " << residual_norms(1, 1) //
                << " " << residual_norms(1, 2) << " " << residual_norms(1, 3) << "\n";
      }
  }

  simulator.write_vtk_solution_str(ofname + ".solution-1.vtk", &solution);
  simulator.write_vtk_solution_unstr(ofname + ".solutionu-1.vtk", &solution);
  solution.k.write_ascii(ofname + ".ref");
  solution.v.write_ascii(ofname + ".vel");

  // Use for debugging.
  // Inputs
  // ./cpp/solvers/nave_circular_washer.exe nx=43 ny=43 dt=0.00005 nt=4000 mu=100 extrapolation error
  // ./cpp/solvers/nave_circular_washer.exe nx=43 ny=43 dt=0.00005 nt=10000        extrapolation+upwind errors
  return 0;
}

int
main(int argc, char *argv[])
{
  pot::GetPot getpot(argc, argv);
  if (getpot("obj", "solver") == std::string("solver"))
    return driver_solver(argc, argv);
  else
    return driver_error_measure(argc, argv);
}