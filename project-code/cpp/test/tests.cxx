extern int test_intersection();
extern int test_clamped_surface_projection();
extern int test_fd_operators();
extern int test_par_msquares();
extern int test_bridson_utils();


#include <cstdio>

int
main()
{
  printf("Testing intersection ... \n");
  //test_intersection();
  printf("Intersection passed ... \n");

  printf("Testing clamped solid ghost extrapolation ... \n");
 // test_clamped_surface_projection();
  printf("Clamped solid ghost extrapolation passed  ... \n");

  printf("Testing the finite difference operators ... \n");
  //test_fd_operators();
  printf("Finite difference operators passed ... \n ");

  printf("Testing marching squares ... \n");
  test_par_msquares();
  printf("End of marching squares test ... \n ");

  printf("Testing Bridson utilities ... \n");
  test_bridson_utils();
  printf("Testing Bridson utilities  ... \n ");

  return 0;
}
