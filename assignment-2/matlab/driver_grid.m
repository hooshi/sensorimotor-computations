clear all
close all

% Distance of the grid that we are looking at from the head
d = 1;

% Y axis of the head (also the grid).
% When we are looking at the center of the grid this should map
% to the x axis of the camera (eye).
gridh = [0, 1, 0];

% X axis of the camera.
gridc0 = [1, 0];

% Length of quivers when drawn.
qui = 0.3;

figure(1);
clf;
hold on;

% Loop over some points on the grid.
for i=-3:3
    for j=-3:3
        % Position of the point on the grid in the head coordinates.
        xh = [d, i, j];
        % Get the angle between the grid line and the camera x dir
        % both in the camera coordinate
        a = get_angle(xh, gridh, gridc0);
        % Draw a quiver with that angle.
        dir = qui * [cos(a), sin(a)];
        p = zeros(2,2);
        p(1,:) = [i,j] - dir;
        p(2,:) = [i,j] + dir;
        plot(p(:,1), p(:,2))
    end
end

% Show the grid 
grid on

% Revert the x-axis since in the camera the x-axis points to the left.
set(gca, 'XDir','reverse')
xlabel('x')
ylabel('y')
title(sprintf('what you see (grid is on a curtain at d=%.1f)',d))
set_tight_bbox()
print(gcf, '-dpdf', 'listings.pdf');

function theta=get_angle(xh, gridh, gridc0)
% Given gaze vector xh, finds the angle between grid_h and grid_c0 in
% camera coordinates.
    Ehl = e_hl(xh);
    gridl = Ehl * gridh(:);
    gridc = conv_lc(gridl);
    gridc = gridc ./ norm(gridc, 2);
    theta = acos( gridc(:)'*gridc0(:) );
    if(gridc(1)*gridc0(2)-gridc(2)*gridc0(1) < 0)
        theta = -theta; 
    end
end



function M = e_hl(gh)
% function M = e_hl(gaze)
% e_hl * x_h = x_l
% Moves from the head coordinates to the eye coordinates.

gh = gh / norm(gh, 2);
theta = acos(gh(1));

if( abs( sin(theta) ) > 1e-10 )
    nz =  gh(2) / sin(theta); 
    ny = -gh(3) / sin(theta);
else 
    ny = 1;
    nz = 0;
end

brn = [0, -nz, ny; nz, 0, 0; -ny, 0, 0];
M = eye(3) + sin(theta)*brn + (1-cos(theta))*brn*brn;

end

function xc = conv_lc(xl)
% function xc = e_lc(xl)
% convert eye coordinates to camera coordinates.
%xc = zeros(2,1);
%xc(1) = xl(2)/xl(1);
%xc(2) = xl(3)/xl(1);
    xc = xl(2:3);
end

function set_tight_bbox()
    ps = get(gcf, 'Position');
    ratio = (ps(4)-ps(2)) / (ps(3)-ps(1))
    paperWidth = 10;
    paperHeight = paperWidth*ratio;


    set(gcf, 'paperunits', 'centimeters');
    set(gcf, 'papersize', [paperWidth paperHeight]);
    set(gcf, 'PaperPosition', [0    0   paperWidth paperHeight]);
end