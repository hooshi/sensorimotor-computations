function rhs = rhs_xi_fd(v, xi, params)
% function rhs = rhs_v_upwind(v, xit, params)

if(nargin == 0 ) run_min_example(); return; end

wave.check_params(params);

dx = params.dx;

xi_plus = circshift(xi, -1);
xi_minus = circshift(xi, 1);

xi_plus(end)= 2*xi_plus(end-1)-xi_plus(end-2); %extrapolate from inside
xi_minus(1) = 2*xi_minus(2)    -xi_minus(3);      %extrapolate from inside


vdxidx = [];
switch params.advm
    case "center"
        vdxidx = v .* (xi_plus - xi_minus) / 2 / dx;
    case "upwind_fd"
        vpos = max(v, 0);
        vmin = min(v, 0);
        vdxidx = vpos .* (xi - xi_minus) / dx + vmin .* (xi_plus - xi) / dx;
    case "upwind_fv"
        % Try but probably not worth, go for semi-Lagrangian.
        assert(0);
end

rhs =  -vdxidx - v;

end

function run_min_example()


size=20;
c = 1;

syms  xs
v_sym =  symfun( sinh( 1.3*sin( 2*pi*xs ) ), xs);
xi_sym =  symfun( erf((xs-0.5)*5), xs);
dxi_sym = diff(xi_sym, xs);
vdxi_sym = v_sym*dxi_sym;

rhs_sym = -vdxi_sym - v_sym;
num_funs = matlabFunction(v_sym, xi_sym, rhs_sym);

params = wave.default_params();
params.advm = "center";
params.c = c;

for i=1:5
    x = linspace(0, 1, size); x = x(:);
    dx = 1 / (size - 1);
    params.dx = dx;

    
    [v, xi, rhs_exact] =  num_funs(x);
    rhs_fd = wave.rhs_xi_fd(v, xi, params);
    
    if(i==3)
        hold off, plot(x, v,'b','linewidth',2);
        hold on, plot(x, xi,'r','linewidth',2);
        hold on, plot(x(2:end-1), rhs_fd(2:end-1),'g','linewidth',2);
        hold on, plot(x(1:2:end), rhs_exact(1:2:end),'mo','linewidth',2);
    end
    
    rhs_exact = rhs_exact(2:end-1);
    rhs_fd = rhs_fd(2:end-1);
    fprintf("%5d %15.6e %15.6e %15.6e \n", size, ...
        max(rhs_exact), max(rhs_fd), max(rhs_exact - rhs_fd) );
    size = size * 2;
end

end