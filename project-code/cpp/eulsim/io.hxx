/** io.hxx
 *
 * All defintiions are here for now. I will then move them to separate files.
 */

#ifndef EULSIM_IO_H
#define EULSIM_IO_H

#include <string>

#include <eulsim/defs.hxx>
#include <eulsim/dof_vector.hxx>
#include <eulsim/grid.hxx>

namespace eulsim
{

struct IsoSurface;
struct Triangulation;

void IO_write_vtk_grid(const std::string filename, Grid &grid);
void IO_write_vtk_grid(const std::string filename, Grid &grid,    //
                       const std::vector<DOFVector_1RV *> &data1, //
                       const std::vector<DOFVector_2RV *> &data2, //
                       const std::vector<std::string> &data_names);
void IO_write_vtk_isurface(std::string fname, IsoSurface &iso);
void IO_write_vtk_triangulation(std::string fname, Triangulation &tri);

void IO_butcher_filename(const std::string fname, std::string &f1, int &no, std::string &f2);
int IO_butcher_filename(const std::string fname);
std::string IO_glue_filename(const std::string f1, const int no, const std::string f2);
std::string IO_enumerate_filename(const std::string fname, const int no);
int IO_latest_number_on_disk_for_filename(const std::string fname);


} // namespace eulsim

#endif /*EULSIM_IO_H*/
