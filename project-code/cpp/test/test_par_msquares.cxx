#include <contrib/visit_writer.h>

#include <eulsim/defs.hxx>
#include <eulsim/geometry.hxx>
#include <eulsim/grid.hxx>
#include <eulsim/io.hxx>

using namespace eulsim;

namespace  test_par_msquares_nsp
{

class Circle : public ImplicitFunction
{
  Real radiusx;
  Real radiusy;

public:
  Circle(Real rx = 0.8, Real ry = 0.8) : radiusx(rx), radiusy(ry) {}



  void
  write_vtk(const std::string filename)
  {
    const int n_pts = 500;
    std::vector<float> pts(n_pts * 3);
    std::vector<int> connectivity(n_pts * 2);
    std::vector<int> cell_types(n_pts, VISIT_LINE);

    for (int i = 0; i < n_pts; ++i)
      {
        const float theta = float(i) / n_pts * 2 * EULSIM_PI;
        const float x = float(radiusx) * cosf(theta);
        const float y = float(radiusy) * sinf(theta);
        pts[i * 3 + 0] = x;
        pts[i * 3 + 1] = y;
        pts[i * 3 + 2] = 0;
        connectivity[i * 2 + 0] = i;
        connectivity[i * 2 + 1] = (i + 1) % n_pts;
      }

    VISIT_write_unstructured_mesh(filename.c_str(),                                                    //
                                  0, n_pts, pts.data(), n_pts, cell_types.data(), connectivity.data(), //
                                  0, NULL, NULL, NULL, NULL);
  }

  double
  eval(Vec2r x)
  {
    // printf("%f %f \n", x(0), x(1) );
    Vec2r z = (x) / Vec2r({ radiusx, radiusy });
    return arma::dot(z, z) - 1;
  }
};

}


int
test_par_msquares()
{
  test_par_msquares_nsp::Circle circle(0.5, 0.5);
  ImplicitFunction *ifn = &circle;
  Grid grid(32, 32, { -1, -1 }, { 1, 1 });
  IsoSurface zero_surface;
  Triangulation tri;


  DOFVector_1RV values(grid);
  for (Index2d vid = grid.first_vertex(); !grid.is_end_vertex(vid); grid.advance_vertex(vid))
    {
      values.value_at(vid) = circle.eval(grid.vertex_xyz(vid));
    }
   IO_write_vtk_grid("test-msquares-grid.vtk", grid, { &values }, {}, { "implicit" });


  Geometry_extract_zero_surface(circle, grid, NULL, &tri);
  std::cout << circle.eval({-1,-1}) << "\n"; 
  std::cout << circle.eval({1,1}) << "\n";
  std::cout << ((ImplicitFunction*)(&circle))->eval({-1,-1}) << "\n"; 
  std::cout << ((ImplicitFunction*)(&circle))->eval({1,1}) << "\n";
  std::cout << ((test_par_msquares_nsp::Circle*)(&circle))->eval({-1,-1}) << "\n"; 
  std::cout << ((test_par_msquares_nsp::Circle*)(&circle))->eval({1,1}) << "\n";
  std::cout << ifn->eval({-1,-1}) << "\n"; 
  std::cout << ifn->eval({1,1}) << "\n";
  
  // IO_write_vtk_isurface("test-msquares-zs.vtk", zero_surface);
  IO_write_vtk_triangulation("test-msquares-tri.vtk", tri);
  circle.write_vtk("test-msquares-exact.vtk");

  return 0;
}

