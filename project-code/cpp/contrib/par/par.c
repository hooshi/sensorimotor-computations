#if defined(__GNUC__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wunused-variable"
#pragma GCC diagnostic ignored "-Wformat-security"
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#endif /*defined(__GNUC__)*/

#include "par_msquares.h"

#include <assert.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>

typedef struct
{
  uint16_t *values;
  size_t count;
  size_t capacity;
} par__uint16list;

typedef struct
{
  float *points;
  int npoints;
  uint16_t *triangles;
  int ntriangles;
  int dim;
  uint32_t color;
  int nconntriangles;
  uint16_t *conntri;
  par__uint16list *tjunctions;
} par_msquares__mesh;

struct par_msquares_meshlist_s
{
  int nmeshes;
  par_msquares__mesh **meshes;
};

static int **par_msquares_binary_point_table = 0;
static int **par_msquares_binary_triangle_table = 0;

static void
par_init_tables()
{
  char const *BINARY_TABLE = "0"
                             "1017"
                             "1123"
                             "2023370"
                             "1756"
                             "2015560"
                             "2123756"
                             "3023035056"
                             "1345"
                             "4013034045057"
                             "2124451"
                             "3024045057"
                             "2734467"
                             "3013034046"
                             "3124146167"
                             "2024460";
  char const *binary_token = BINARY_TABLE;

  par_msquares_binary_point_table = PAR_CALLOC(int *, 16);
  par_msquares_binary_triangle_table = PAR_CALLOC(int *, 16);
  for (int i = 0; i < 16; i++)
    {
      int ntris = *binary_token - '0';
      binary_token++;
      par_msquares_binary_triangle_table[i] = PAR_CALLOC(int, (ntris + 1) * 3);
      int *sqrtris = par_msquares_binary_triangle_table[i];
      sqrtris[0] = ntris;
      int mask = 0;
      int *sqrpts = par_msquares_binary_point_table[i] = PAR_CALLOC(int, 7);
      sqrpts[0] = 0;
      for (int j = 0; j < ntris * 3; j++, binary_token++)
        {
          int midp = *binary_token - '0';
          int bit = 1 << midp;
          if (!(mask & bit))
            {
              mask |= bit;
              sqrpts[++sqrpts[0]] = midp;
            }
          sqrtris[j + 1] = midp;
        }
    }

}

par_msquares_mesh const *
par_msquares_get_mesh(par_msquares_meshlist *mlist, int mindex)
{
  assert(mlist && mindex < mlist->nmeshes);
  return (par_msquares_mesh const *)mlist->meshes[mindex];
}

int
par_msquares_get_count(par_msquares_meshlist *mlist)
{
  assert(mlist);
  return mlist->nmeshes;
}

void
par_msquares_free(par_msquares_meshlist *mlist)
{
  if (!mlist)
    {
      return;
    }
  par_msquares__mesh **meshes = mlist->meshes;
  for (int i = 0; i < mlist->nmeshes; i++)
    {
      free(meshes[i]->points);
      free(meshes[i]->triangles);
      free(meshes[i]);
    }
  free(meshes);
  free(mlist);
}


par_msquares_meshlist *
par_msquares_function(int width, int height, int cellsize, int flags, void *context, par_msquares_inside_fn insidefn,
                      par_msquares_height_fn heightfn)
{
  assert(width > 0 && width % cellsize == 0);
  assert(height > 0 && height % cellsize == 0);

  // Create the two code tables if we haven't already.  These are tables of
  // fixed constants, so it's embarassing that we use dynamic memory
  // allocation for them.  However it's easy and it's one-time-only.
  if (!par_msquares_binary_point_table)
    {
      par_init_tables();
    }

  // Allocate the meshlist and the first mesh.
  par_msquares_meshlist *mlist = PAR_CALLOC(par_msquares_meshlist, 1);
  mlist->nmeshes = 1;
  mlist->meshes = PAR_CALLOC(par_msquares__mesh *, 1);
  mlist->meshes[0] = PAR_CALLOC(par_msquares__mesh, 1);
  par_msquares__mesh *mesh = mlist->meshes[0];
  mesh->dim = (flags & PAR_MSQUARES_HEIGHTS) ? 3 : 2;
  int ncols = width / cellsize;
  int nrows = height / cellsize;

  // Worst case is four triangles and six verts per cell, so allocate that
  // much.
  int maxtris = ncols * nrows * 4;
  int maxpts = ncols * nrows * 6;
  int maxedges = ncols * nrows * 2;

  // However, if we include extrusion triangles for boundary edges,
  // we need space for another 4 triangles and 4 points per cell.
  uint16_t *conntris = 0;
  int nconntris = 0;

  uint16_t *tris = PAR_CALLOC(uint16_t, maxtris * 3);
  int ntris = 0;
  float *pts = PAR_CALLOC(float, maxpts * mesh->dim);
  int npts = 0;

  // The "verts" x/y/z arrays are the 4 corners and 4 midpoints around the
  // square, in counter-clockwise order.  The origin of "triangle space" is at
  // the lower-left, although we expect the image data to be in raster order
  // (starts at top-left).
  float vertsx[8], vertsy[8];
  float normalization = 1.0f / PAR_MAX(width, height);
  float normalized_cellsize = cellsize * normalization;
  int maxrow = (height - 1) * width;
  uint16_t *ptris = tris;
  uint16_t *pconntris = conntris;
  float *ppts = pts;
  uint8_t *prevrowmasks = PAR_CALLOC(uint8_t, ncols);
  int *prevrowinds = PAR_CALLOC(int, ncols * 3);


  // Do the march!
  for (int row = 0; row < nrows; row++)
    {
      vertsx[0] = vertsx[6] = vertsx[7] = 0;
      vertsx[1] = vertsx[5] = 0.5 * normalized_cellsize;
      vertsx[2] = vertsx[3] = vertsx[4] = normalized_cellsize;
      vertsy[0] = vertsy[1] = vertsy[2] = normalized_cellsize * (row + 1);
      vertsy[4] = vertsy[5] = vertsy[6] = normalized_cellsize * row;
      vertsy[3] = vertsy[7] = normalized_cellsize * (row + 0.5);

      int northi = row * cellsize * width;
      int southi = PAR_MIN(northi + cellsize * width, maxrow);
      int northwest = insidefn(northi, context);
      int southwest = insidefn(southi, context);
      int previnds[8] = { 0 };
      uint8_t prevmask = 0;

      for (int col = 0; col < ncols; col++)
        {
          northi += cellsize;
          southi += cellsize;
          if (col == ncols - 1)
            {
              northi--;
              southi--;
            }

          int northeast = insidefn(northi, context);
          int southeast = insidefn(southi, context);
          int code = southwest | (southeast << 1) | (northwest << 2) | (northeast << 3);

          int const *pointspec = par_msquares_binary_point_table[code];
          int ptspeclength = *pointspec++;
          int currinds[8] = { 0 };
          uint8_t mask = 0;
          uint8_t prevrowmask = prevrowmasks[col];
          while (ptspeclength--)
            {
              int midp = *pointspec++;
              int bit = 1 << midp;
              mask |= bit;

              // The following six conditionals perform welding to reduce the
              // number of vertices.  The first three perform welding with the
              // cell to the west; the latter three perform welding with the
              // cell to the north.
              if (bit == 1 && (prevmask & 4))
                {
                  currinds[midp] = previnds[2];
                  continue;
                }
              if (bit == 128 && (prevmask & 8))
                {
                  currinds[midp] = previnds[3];
                  continue;
                }
              if (bit == 64 && (prevmask & 16))
                {
                  currinds[midp] = previnds[4];
                  continue;
                }
              if (bit == 16 && (prevrowmask & 4))
                {
                  currinds[midp] = prevrowinds[col * 3 + 2];
                  continue;
                }
              if (bit == 32 && (prevrowmask & 2))
                {
                  currinds[midp] = prevrowinds[col * 3 + 1];
                  continue;
                }
              if (bit == 64 && (prevrowmask & 1))
                {
                  currinds[midp] = prevrowinds[col * 3 + 0];
                  continue;
                }

              ppts[0] = vertsx[midp];
              ppts[1] = vertsy[midp];

              // Adjust the midpoints to a more exact crossing point.
              if (midp == 1)
                {
                  int begin = southi - cellsize / 2;
                  int previous = 0;
                  for (int i = 0; i < cellsize; i++)
                    {
                      int offset = begin + i / 2 * ((i % 2) ? -1 : 1);
                      int inside = insidefn(offset, context);
                      if (i > 0 && inside != previous)
                        {
                          ppts[0] = normalization * (col * cellsize + offset - southi + cellsize);
                          break;
                        }
                      previous = inside;
                    }
                }
              else if (midp == 5)
                {
                  int begin = northi - cellsize / 2;
                  int previous = 0;
                  for (int i = 0; i < cellsize; i++)
                    {
                      int offset = begin + i / 2 * ((i % 2) ? -1 : 1);
                      int inside = insidefn(offset, context);
                      if (i > 0 && inside != previous)
                        {
                          ppts[0] = normalization * (col * cellsize + offset - northi + cellsize);
                          break;
                        }
                      previous = inside;
                    }
                }
              else if (midp == 3)
                {
                  int begin = northi + width * cellsize / 2;
                  int previous = 0;
                  for (int i = 0; i < cellsize; i++)
                    {
                      int offset = begin + width * (i / 2 * ((i % 2) ? -1 : 1));
                      int inside = insidefn(offset, context);
                      if (i > 0 && inside != previous)
                        {
                          ppts[1] = normalization * (row * cellsize + (offset - northi) / (float)width);
                          break;
                        }
                      previous = inside;
                    }
                }
              else if (midp == 7)
                {
                  int begin = northi + width * cellsize / 2 - cellsize;
                  int previous = 0;
                  for (int i = 0; i < cellsize; i++)
                    {
                      int offset = begin + width * (i / 2 * ((i % 2) ? -1 : 1));
                      int inside = insidefn(offset, context);
                      if (i > 0 && inside != previous)
                        {
                          ppts[1] = normalization * (row * cellsize + (offset - northi - cellsize) / (float)width);
                          break;
                        }
                      previous = inside;
                    }
                }

              if (mesh->dim == 3)
                {
                  ppts[2] = heightfn(ppts[0], ppts[1], context);
                }

              ppts += mesh->dim;
              currinds[midp] = npts++;
            }

          int const *trianglespec = par_msquares_binary_triangle_table[code];
          int trispeclength = *trianglespec++;

          // Add triangles.
          while (trispeclength--)
            {
              int a = *trianglespec++;
              int b = *trianglespec++;
              int c = *trianglespec++;
              *ptris++ = currinds[c];
              *ptris++ = currinds[b];
              *ptris++ = currinds[a];
              ntris++;
            }

          // Prepare for the next cell.
          prevrowmasks[col] = mask;
          prevrowinds[col * 3 + 0] = currinds[0];
          prevrowinds[col * 3 + 1] = currinds[1];
          prevrowinds[col * 3 + 2] = currinds[2];
          prevmask = mask;
          northwest = northeast;
          southwest = southeast;
          for (int i = 0; i < 8; i++)
            {
              previnds[i] = currinds[i];
              vertsx[i] += normalized_cellsize;
            }
        }
    }
  free(prevrowmasks);
  free(prevrowinds);
  
  // Append all extrusion triangles to the main triangle array.
  // We need them to be last so that they form a contiguous sequence.
  pconntris = conntris;
  for (int i = 0; i < nconntris; i++)
    {
      *ptris++ = *pconntris++;
      *ptris++ = *pconntris++;
      *ptris++ = *pconntris++;
      ntris++;
    }
  free(conntris);

  // Final cleanup and return.
  assert(npts <= maxpts);
  assert(ntris <= maxtris);
  mesh->npoints = npts;
  mesh->points = pts;
  mesh->ntriangles = ntris;
  mesh->triangles = tris;
  mesh->nconntriangles = nconntris;
  return mlist;
}



#if defined(__GNUC__)
#pragma GCC diagnostic pop
#endif /*defined(__GNUC__)*/
