### Directly related work
Eulerian solid simulation with contact
[Siggraph](./organized/levin-siggraph-2011.pdf),
[Thesis](./organized/levin-phd-thesis.pdf),

Eulerian on Lagrangian 
[TOG](./organized/eulerian-on-lagrangian.pdf),
[Thesis](./organized/y-fan-thesis.pdf),

Kamrin reference map
[2009](./organized/kamrin-2009.pdf),
[2012](./organized/kamrin-2012.pdf)

Batty variational framework for pressure
[Thesis](./organized/batty-thesis.pdf), [Siggraph](./organized/batty-variational-siggraph.pdf),
[Code](https://cs.uwaterloo.ca/~c2batty/code/variationalplusgfm.zip)

[Ritchit (more theory)](./organized/ritcher-2012.pdf)


### Extrapolation
[Second order level set of Gibou et al](./organized/gibou-second-order-level-set.pdf)    
[Second order level set of Enright et al](./organized/enright-second-order-level-set.pdf)    
[Ghost fluid method](./organized/ghost-fluid-method.pdf)
