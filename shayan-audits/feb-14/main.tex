%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% - Scribe notes for CS530P 25th January 2018.
% - Instructor: Prof. Dinesh Pai, Scribe: Shayan Hoshyari
% - Adapted from: Template for Scribe Notes -- CS500, Will Evans (Spring 2012)
% - Adapted from: Erik Demaine at MIT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[letter, 11pt]{article}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage[margin=1in]{geometry}
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{tikz, tikz-3dplot}
\usepackage{pgfplots}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Fonts
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage[T1]{fontenc}
\usepackage{kpfonts}%  for math    
\usepackage{libertine}%  serif and sans serif
\usepackage[scaled=0.85]{beramono}%% mono


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Extra commands
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\scribeTitle}[3]{
  \begin{centering}
  \framebox{ \vbox{
      \textbf{CPSC 530P Sensorimotor Computation}
      
      \vspace{-1mm}
      { #1 }
      
      \vspace{-1mm}
      \emph{#2, Scribe: #3}
    }}
  \end{centering}
  \vspace*{4mm}
}

\newcommand{\R}{\mathbb{R}}     % use for capital R real number set symbol
\newcommand{\Z}{\mathbb{Z}}     % use for capital Z integer set symbol


\newtheorem{theorem}{Theorem}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{observation}[theorem]{Observation}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{fact}[theorem]{Fact}
\newtheorem{assumption}[theorem]{Assumption}

\usepackage[bookmarks,bookmarksnumbered,%
allbordercolors={0.8 0.8 0.8},%
linktocpage%
]{hyperref}

\parindent 0in
\parskip 1.5ex
%\renewcommand{\baselinestretch}{1.25}

\newcommand{\reffig}[1]{Fig.~\ref{#1}}

\newcommand{\vvec}[1]{#1}
\newcommand{\nvec}[1]{#1}        
\newcommand{\mmat}[1]{#1}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Document Body
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

% ---------------------------------------------
% Title
% ---------------------------------------------
\scribeTitle{Lecture: February 14th, 2018}{Prof.\ Dinesh Pai}{Shayan Hoshyari}

% ---------------------------------------------
% Goals
% ---------------------------------------------
\section*{Goals}

Previously, we saw that a rigid motion, $x = \phi(X)$, can be expressed as:
\begin{equation}
  \label{eq:rigid}
  x = R(q)X + P(q),
\end{equation}
where $R$ is a rotation matrix, $P$ is a translation vector, and $q$
is the vector of configurations (degrees of freedom).

In this lecture, the goal is to answer the following questions.
\begin{itemize}
\item How can the velocity of a rigid motion be described?
\item What does the parameterization of $R$ and $P$ look like with
  respect to $q$?
\end{itemize}

% ---------------------------------------------
% Velocity for Rigid Motion
% ---------------------------------------------
\section*{Velocity for Rigid Motion}

For a particle moving in the path $x=x(t)$, the velocity is found as
$v(t)=\dot{x}=\frac{d}{dt}x(t)$. A schematic of this concept is shown in
below.
\begin{center}
\includegraphics[width=0.3\columnwidth]{img/particle-velocity.pdf}
\end{center}
For a continuous medium, the velocity has the more
general form of
\[
   v(X,t) = \dot{x} = \frac{D}{Dt}x(X,t),
\]
where $D$ represents the material derivative, i.e. it is evaluated
when the Lagrangian coordinate $X$ is constant rather than the
Eulerian coordinate $x$.

In order to evaluate the velocity field for the special case of the
rigid motion, we recall that this type of motion has the form of
Eq.~(\ref{eq:rigid}), where the rotation matrix $R$ must satisfy the
following constraints.
\begin{equation}
  \label{eq:r-cons}
  R^TR = I, \qquad \det(R)=+1.
\end{equation}
Although, Eq.~(\ref{eq:r-cons}) represents a series of non-linear dependent
constraints, we will show that they can be greatly simplified when one
looks at the velocity rather than the location. For simplicity, let us
assume that there is no translation, $P=0$. Taking the material
derivative of $x$ we would get:
\begin{equation}
  \label{eq:omega-deriv}
  \begin{cases}
    x = RX  \\
    R^TR = I
  \end{cases}
  \Rightarrow
  \begin{cases}
    \dot{x} = \dot{R}X \\
    X = R^Tx
  \end{cases} \rightarrow
  \dot{x} = \dot{R}R^T x,
\end{equation}
where $[\omega]=\dot{R}R^T$ is known as the angular velocity matrix.

Lets us know transfer the constraints on $R$ to $[\omega]$:
\[
    RR^T = I \Rightarrow \dot{R}R^T+R\dot{R}^T = 0
    \Rightarrow [\omega] = -[\omega]^T.  
\]
This means that any skew-symmetric matrix can compactly describe the
velocity of a rotational motion. Also, we can see that in $n$
dimensions $\frac{n(n-1)}{2}$ degrees of freedom are required to
describe an angular velocity matrix ($1$, $3$, and $6$ values for two,
three, and four dimensions, respectively). As a special case
$[\omega]$ is represented as following in two and three dimensions:
\[
  [\omega]_{2D} =%
  \begin{bmatrix} 0 &-\omega \\
    \omega &0 \end{bmatrix},%
  \qquad \qquad%
  [\omega]_{3D} = \begin{bmatrix}
    0  &-\omega_z &\omega_y \\
    \omega_z  &0 &-\omega_x \\
    -\omega_y &\omega_z &0
    \end{bmatrix}
\]

In 3-D, the angular velocity vector $\omega$ is defined as
$(\omega_x, \omega_y, \omega_z)$. We can observe that
$[\omega]x = \omega \times x$, where $\times$ represents the vector
cross product. Therefore, the ``$[]$'' operator can be thought of as
change of format from $\mathcal R^3$ to $\mathcal R^9$, transforming
the angular velocity vector to the angular velocity matrix. This
simplification, however, does not generalize to higher dimensions.

Having defined $[\omega]$, we now have the machinery to parameterize
rotation.

% ---------------------------------------------
% Parametrizing Rotation
% ---------------------------------------------
\section*{Parameterizing Rotation}

To parameterize rotation, we start by recalling
Eq.~(\ref{eq:omega-deriv}). This equation represents a differential
equation for position $x$. For a fixed $[\omega]$, this equation can
be solved using our knowledge of matrix exponential:
\begin{equation}
  \label{eq:omega-to-r}
  x =  \exp([\omega] t)x(t=0) = \exp([\omega]t)X.
\end{equation}

We can assume that any rotation $R$ has happened during some imaginary
time $t \in [0,1]$ in the form of $R(t) = Rt$, which gives
$[\omega]=RR^T$. Then, plugging $t=1$ in Eq.~(\ref{eq:omega-to-r})
this rotation can be written as $R=\exp([\omega])$. Thus, we have
worked out the inverse of Eq.~(\ref{eq:omega-deriv}) and expressed the
rotation in terms of the angular velocity. Schematically:

\begin{center}
  \includegraphics[width=0.6\linewidth]{img/rot-param.pdf}
\end{center}


This gives us a nice way to parameterize finite rotations by only using
a skew-symmetric matrix. This parameterization simplifies to a vector in
three dimensions with the direction of the vector representing the
axis of rotations and its magnitude the angle of rotation.

With the presented machinery, parameterizing a rigid motion in three
dimension boils down to selecting three degrees of freedom
$\theta = (q_1, q_2, q_3)$ that can represent the rotation part of the
motion as $\exp([\theta])$, and another three degrees of freedom
$(q_4, q_5, q_6)$ that can be the coordinates of the translation
vector $p$.

\section*{Other ways to parameterize rotation}

\paragraph{Complex numbers} In 2-D, a rotation can be represented
using complex numbers. A vector $x=(x_1,x_2)$ can be represented by
the complex number $x_1+ i x_2$, and a rotation equal to $\theta$ can
be presented as $\exp(i \theta) = \cos(\theta)+i\sin(\theta)$. This is
equivalent to associating the number $1$ to the matrix
$\begin{bmatrix} 1 &0 \\ 0 &1 \end{bmatrix}$ and the number $i$ to the
matrix $\begin{bmatrix} 0 &-1 \\ 1 &0 \end{bmatrix}$.

\paragraph{Unit quaternions} In 3-D a rotation can be represented
using unit quaternions (four scalars plus a constraint). This is not
quite surprising since quaternions are somewhat a generalization of
complex numbers (tuples of two real numbers) to tuples of four real
numbers.


\paragraph{Euler angles} A 3-D rotation can be expressed in terms of
Euler angles. First, a rotation of angle $\psi$ (or $\alpha$) around
the z axis, then a rotation of angle $\theta$ (or $\beta$) around the
new $x$ axis, and finally a rotation of angle $\phi$ (or $\gamma$)
around the new $z$ axis. A schematic is shown below.

\begin{center}
\includegraphics{img/latex_coordinate_transformations.pdf}
\end{center}

\paragraph{Other systems of angles} Other systems include
roll-yaw-pitch and Fick angles.






\end{document}
