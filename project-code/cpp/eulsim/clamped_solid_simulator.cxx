#include <contrib/getpot.hxx>
#include <contrib/visit_writer.h>

#include <eulsim/clamped_solid_simulator.hxx>
#include <eulsim/predicates.hxx>
#include <eulsim/io.hxx>
#include <eulsim/geometry.hxx>

namespace eulsim
{

ClampedSolidSimulator::SimulationInfo::SimulationInfo()
  : dt(0.01), g(0), damping_velocity(0), do_first_order_velocity_extrapolation(false)
{
}

ClampedSolidSimulator::ClampedSolidSimulator(Grid &grid_in, ClampedSolid &solid_in, const SimulationInfo info_in)
  : _info(info_in)
  , _grid(grid_in)
  , _solid(solid_in)
  , _rhoarea(_grid)
  , _divstressarea(_grid)
  , _nablavarea(_grid)
  , _vertex_status(_grid)
  , _laplacianksiarea(_grid)
  , _euler_interim(grid_in)
  , _rk3_interim_0(grid_in)
  , _rk3_interim_1(grid_in)
  , _rk3_interim_2(grid_in)
  , _is_initialized(false)
{
}

ClampedSolidSimulator::~ClampedSolidSimulator() {}

//
// We need an implicit function that is the solid
//
class ClampedSolidImplicitFunction : public ImplicitFunction
{
public:
  ClampedSolidImplicitFunction(ClampedSolid &csin, const Real timein) : cs(csin), time(timein) {}

  double
  eval(Vec2r pt) override
  {
    SurfacePoint spt;
    cs.project_point(pt, &spt);
    Vec2r pt0 = cs.surface_point_xy(spt);
    int dist_sign = (cs.is_in_initially(pt) ? -1 : 1);
    return dist_sign * arma::norm(pt - pt0);
  }

  Real time;
  ClampedSolid &cs;
};

void
ClampedSolidSimulator::init_vertex_status()
{
  // Damn you KMARIN for suggesting 0.1, and wasting 2 days of my time.
  const double closeness_thr = 0.5;
  // Things that have to be done.
  // 1- Initialize solution
  // 2- Initialize vertex status and interpolation weights

  const int s_undecided = 0;
  const int s_active = 1;
  const int s_ghostinner = 2;
  const int s_ghostouter = 3;
  const int s_inactive = 4;

  DOFVector<int, 1, STORAGE_VERTICES> vstatus(grid());
  vstatus.set(s_undecided);

  //
  // Loop over all vertices
  // Find those that are either active or inner ghost
  //
  ClampedSolidImplicitFunction solid_implicit_function(solid(), 0);

  for (int i = 0; i < grid().n_verts(0); ++i)
    {
      for (int j = 0; j < grid().n_verts(1); ++j)
        {
          Index2d vid({ i, j });

          // Check if the point is in or out.
          if (solid().is_in_initially(grid().vertex_xyz(vid)))
            {
              // Check if any of the neighbours are too close
              bool is_too_close = false;
              std::vector<Index2d> neighs = grid().vertex_neighbour_vertices(vid);

              for (int nidl = 0; nidl < (int)neighs.size(); ++nidl)
                {
                  double t;
                  bool does_intersect;
                  Index2d nid = neighs[nidl];

                  does_intersect = Geometry_signed_distance_intersection(
                      solid_implicit_function, grid().vertex_xyz(vid), grid().vertex_xyz(nid), &t);

                  if (does_intersect && (t < closeness_thr))
                    {
                      is_too_close = true;
                      // std::cout << t << " " << vid.t() << nid.t() << std::endl;
                      break;
                    }
                } // End of checking neighbours

              // If too close mark as inner ghost, otherwise mark as active
              if (is_too_close)
                vstatus.value_at(vid) = s_ghostinner;
              else
                vstatus.value_at(vid) = s_active;
            } // End of is_initially_in
        }     // End of j
    }         // End of i

  //
  // Loop again, if anyone is touching (in star stencil) it would become an outer ghost
  //
  for (int i = 0; i < grid().n_verts(0); ++i)
    {
      for (int j = 0; j < grid().n_verts(1); ++j)
        {
          Index2d vid({ i, j });
          if (vstatus.value_at(vid) == s_ghostinner)
            continue;
          if (vstatus.value_at(vid) == s_active)
            continue;

          // Check if any of the neighbours are too close
          bool does_touch_active = false;
          std::vector<Index2d> neighs = grid().vertex_star_vertices(vid);

          for (int nidl = 0; nidl < (int)neighs.size(); ++nidl)
            {
              Index2d nid = neighs[nidl];
              if (vstatus.value_at(nid) == s_active)
                {
                  does_touch_active = true;
                  break;
                }
            }

          if (does_touch_active)
            vstatus.value_at(vid) = s_ghostouter;
          else
            vstatus.value_at(vid) = s_inactive;

        } // End of j
    }     // End of i

  //
  // Update the status of all the vertices according to vstatus
  //
  for (int i = 0; i < grid().n_verts(0); ++i)
    {
      for (int j = 0; j < grid().n_verts(1); ++j)
        {
          Index2d idv({ i, j });
          switch (vstatus.value_at(idv))
            {
              case s_ghostinner: vertex_status(idv).vtype = VERTEX_GHOST; break;
              case s_ghostouter: vertex_status(idv).vtype = VERTEX_GHOST; break;
              case s_active: vertex_status(idv).vtype = VERTEX_ACTIVE; break;
              case s_inactive: vertex_status(idv).vtype = VERTEX_INACTIVE; break;
              default: EULSIM_FORCE_ASSERT_MSG(0, "YOU ARE DOOMED.");
            } // End of swtich
        }     // End of for over j
    }         // End of for over i

} // All done

void
ClampedSolidSimulator::init_ghost_extrapolation_weights(Index2d vid, VertexGhostStatus *vgs, Vec2r *p_intersection)
{
  EULSIM_FORCE_ASSERT(vgs);
  const int max_search_depth = 4;
  const Real closeness_threshold = 1e-12;

  // Only works for ghosts
  if (vgs->vtype != VERTEX_GHOST)
    return;

  // Find nearby edges
  std::vector<Index3d> edges = grid().vertex_nearby_edges(vid, max_search_depth);
  const Vec2r posv = grid().vertex_xyz(vid);
  SurfacePoint projv;
  solid().project_point(posv, &projv);
  const Vec2r posprojv = solid().surface_point_xy(projv);

  //
  // If intersection is very close, then just set the point to
  // have the boundary value
  //
  if (arma::norm(posv - posprojv) < closeness_threshold)
    {
      vgs->p0 = projv;
      vgs->w0 = 1;
      // dummy values
      vgs->p1 = vgs->p2 = vid;
      vgs->w1 = vgs->w2 = 0;
      return;
    }

  // Loop over all nearby edges, Find all that:
  // 1- intersect with the edge
  // 2- Both intersected vertices must be inside the solid
  typedef std::vector<std::pair<double, Index3d>> vector_ie_t;
  vector_ie_t intersecting_edges;
  intersecting_edges.reserve(edges.size());

  for (uint eidl = 0; eidl < edges.size(); ++eidl)
    {
      const Index3d eid = edges[eidl];

      // Get the vertices and their position
      Index2d v0, v1;
      grid().edge_vertices(eid, &v0, &v1);
      const Vec2r posv0 = grid().vertex_xyz(v0);
      const Vec2r posv1 = grid().vertex_xyz(v1);

      // Are both vertices inside
      if (!((vertex_status(v0).vtype == VERTEX_ACTIVE) && (vertex_status(v1).vtype == VERTEX_ACTIVE)))
        continue;

      // Is the edge intersected ?
      double t_intersection;
      bool does_intersect;
      does_intersect = Geometry_line_intersection(posv, posprojv, posv0, posv1, &t_intersection);

      // If intersection is between 1 and -1 then save the t and its edge
      does_intersect = does_intersect && (t_intersection >= 0.) && (t_intersection <= 1.);
      if (does_intersect)
        {
          double t_ray;
          does_intersect = Geometry_line_intersection(posv0, posv1, posv, posprojv, &t_ray);
          // printf("%lf %lf, %5d %5d -- %5d %5d %lf \n", posprojv(0), posprojv(1), v0(0), v0(1), v1(0), v1(1),
          //       t_intersection);
          EULSIM_FORCE_ASSERT(does_intersect);
          intersecting_edges.push_back(std::make_pair(EULSIM_ABS(t_ray), eid));
        }
    }

  //
  // Find the closest edge
  //
  if (!intersecting_edges.size())
    {
      vgs->vtype = VERTEX_INACTIVE;
      printf("(%d, %d) DOOOOOOOOOOOOOOOOOOOOM!!!!!!!!!!!!!!!!!!!!!!!!! \n", vid(0), vid(1));
      return;
    }
  EULSIM_FORCE_ASSERT(intersecting_edges.size());
  vector_ie_t::iterator it_min =
      std::min_element(intersecting_edges.begin(), intersecting_edges.end(), IsPairMemberLesser<Real, Index3d, 0>());

  //
  // Find the intersection again and initialize the interpolation weights
  //
  {
    const Index3d eid = it_min->second;

    // Get the vertices and their position
    Index2d v0, v1;
    grid().edge_vertices(eid, &v0, &v1);
    const Vec2r posv0 = grid().vertex_xyz(v0);
    const Vec2r posv1 = grid().vertex_xyz(v1);

    // Find the intersections
    bool does_intersect;

    double tedge, tray;

    does_intersect = Geometry_line_intersection(posv, posprojv, posv0, posv1, &tedge);
    EULSIM_FORCE_ASSERT(does_intersect);
    does_intersect = Geometry_line_intersection(posv0, posv1, posv, posprojv, &tray);
    EULSIM_FORCE_ASSERT(does_intersect);
    EULSIM_ASSERT(EULSIM_ABS(tray) == it_min->first);

    vgs->p0 = projv;
    vgs->p1 = v0;
    vgs->p2 = v1;
    const Real invoneminustr = 1. / (1 - tray);
    vgs->w0 = 1 - invoneminustr;
    vgs->w1 = (1 - tedge) * invoneminustr;
    vgs->w2 = tedge * invoneminustr;
  }

} // All done

void
ClampedSolidSimulator::init(bool for_test)
{
  if (for_test)
    {
      for (Index2d vid = grid().first_vertex(); !grid().is_end_vertex(vid); grid().advance_vertex(vid))
        {

          if (grid().is_boundary_vertex(vid))
            {
              vertex_status(vid).vtype = VERTEX_GHOST;
            }
          else
            {
              vertex_status(vid).vtype = VERTEX_ACTIVE;
            }
          vertex_status(vid).p0.surface_id = -10000;
          vertex_status(vid).p0.t = -10000;
          vertex_status(vid).p1 = { -1000, -1000 };
          vertex_status(vid).p2 = { -1000, -1000 };
          vertex_status(vid).w0 = -100000;
          vertex_status(vid).w1 = -100000;
          vertex_status(vid).w2 = -100000;
        }
    }
  else
    {

      init_vertex_status();

      for (int i = 0; i < grid().n_verts(0); ++i)
        {
          for (int j = 0; j < grid().n_verts(1); ++j)
            {
              init_ghost_extrapolation_weights({ i, j }, &vertex_status({ i, j }), NULL);
            }
        }
    }

  _is_initialized = true;
} // All done

void
ClampedSolidSimulator::extrapolate_values(Real time, DOFVector_2RV &quantity, VariableType variable_type)
{
  EULSIM_FORCE_ASSERT(_is_initialized);

  // Extrapolate a quantity to find the values of the ghost vectors
  for (Index2d i2d = grid().first_vertex(); !grid().is_end_vertex(i2d); grid().advance_vertex(i2d))
    {
      if (vertex_status(i2d).vtype == VERTEX_GHOST)
        {
          VertexGhostStatus vs = vertex_status(i2d);
          Vec2r val0, val1, val2, valnew; //, valmax, valmin;
          switch (variable_type)
            {
              case VAR_VELOCITY:
                {
                  solid().boundary_solution(time, vs.p0, &val0, NULL);
                  if (info().do_first_order_velocity_extrapolation)
                    {
                      valnew = val0;
                    }
                  else
                    {
                      val1 = quantity.vec_at(vs.p1);
                      val2 = quantity.vec_at(vs.p2);
                      valnew = vs.w0 * val0 + vs.w1 * val1 + vs.w2 * val2;
                    }
                  break;
                }
              case VAR_REFMAP:
                {
                  solid().boundary_solution(time, vs.p0, NULL, &val0);
                  val1 = quantity.vec_at(vs.p1);
                  val2 = quantity.vec_at(vs.p2);
                  valnew = vs.w0 * val0 + vs.w1 * val1 + vs.w2 * val2;
                  break;
                }
              default: EULSIM_FORCE_ASSERT(0);
            }

          // CLAMP
          // valmax = arma::max(val0, arma::max(val1, val2));
          // valmin = arma::min(val0, arma::min(val1, val2));
          // valnew(0) = EULSIM_MAX(EULSIM_MIN(valnew(0), valmax(0)), valmin(0));
          // valnew(1) = EULSIM_MAX(EULSIM_MIN(valnew(1), valmax(1)), valmin(1));

          // FIRST ORDER SET TO BOUNDARY
          // EULSIM_UNUSED(val1);
          // EULSIM_UNUSED(val2);
          // EULSIM_UNUSED(valmin);
          // EULSIM_UNUSED(valmax);
          // valnew = val0;

          quantity.set_vec_at(i2d, valnew);
        } // End of if(ghost)
    }     // End of i
} // All done


void
ClampedSolidSimulator::compute_rhs_velocity(const DOFVector_2RV &velocity, const DOFVector_2RV &refmap,
                                            DOFVector_2RV *rhs)
{
  EULSIM_FORCE_ASSERT(_is_initialized);

  const Real dx = grid().delta(0);
  const Real dy = grid().delta(1);
  const int dimx = 0;
  EULSIM_UNUSED(dimx);
  const int dimy = 1;
  EULSIM_UNUSED(dimy);

  // FOR DEBUGGING TRY TO FIND LAPLACIAN OF KSI
  enum
  {
    LAPLACIAN_KSI_FV = 0,
    LAPLACIAN_KSI_FD_STAR = 1,
    LAPLACIAN_KSI_FD_PLUS = 2,
    LAPLACIAN_KSI_NONE = 3
  };
  const int SHOULD_FIND_LAPLACIAN_KSI = LAPLACIAN_KSI_FV;

  //
  // First loop over cells and integrate some handy information
  //

  _nablavarea.set(0);
  _rhoarea.set(0);
  _divstressarea.set(0);
  _laplacianksiarea.set(0);

  for (Index2d cid = grid().first_cell(); !grid().is_end_cell(cid); grid().advance_cell(cid))
    {
      // clang-format off
      const Index2d vid_0 = { cid(0)  , cid(1)     };
      const Index2d vid_1 = { cid(0)+1, cid(1)     };
      const Index2d vid_2 = { cid(0)+1, cid(1)+  1 };
      const Index2d vid_3 = { cid(0)  , cid(1) + 1 };

      // Only cells with active vertices
      if (    (vertex_status(vid_0).vtype != VERTEX_ACTIVE)
         &&   (vertex_status(vid_1).vtype != VERTEX_ACTIVE)
         &&   (vertex_status(vid_2).vtype != VERTEX_ACTIVE)
         &&   (vertex_status(vid_3).vtype != VERTEX_ACTIVE)  )
              continue;

      // There should be no inactive vertices
      EULSIM_FORCE_ASSERT (   
              (vertex_status(vid_0).vtype != VERTEX_INACTIVE)
         &&   (vertex_status(vid_1).vtype != VERTEX_INACTIVE)
         &&   (vertex_status(vid_2).vtype != VERTEX_INACTIVE)
         &&   (vertex_status(vid_3).vtype != VERTEX_INACTIVE)  );
      // clang-format on

      // No cell on the actual grid boundary
      EULSIM_ASSERT_MSG(!(grid().is_boundary_vertex(vid_0) && (vertex_status(vid_0).vtype == VERTEX_ACTIVE)),
                        "No active boundary vertex, increase grid lengths.");
      EULSIM_ASSERT_MSG(!(grid().is_boundary_vertex(vid_1) && (vertex_status(vid_1).vtype == VERTEX_ACTIVE)),
                        "No active boundary vertex, increase grid lengths.");
      EULSIM_ASSERT_MSG(!(grid().is_boundary_vertex(vid_2) && (vertex_status(vid_2).vtype == VERTEX_ACTIVE)),
                        "No active boundary vertex, increase grid lengths.");
      EULSIM_ASSERT_MSG(!(grid().is_boundary_vertex(vid_3) && (vertex_status(vid_3).vtype == VERTEX_ACTIVE)),
                        "No active boundary vertex, increase grid lengths.");

      // Get vertex refmap and velocity
      const Vec2r ksi_0 = refmap.vec_at(vid_0);
      const Vec2r ksi_1 = refmap.vec_at(vid_1);
      const Vec2r ksi_2 = refmap.vec_at(vid_2);
      const Vec2r ksi_3 = refmap.vec_at(vid_3);

      // Find the deformation tensor
      const Vec2r dksidx = 0.5 * (ksi_2 + ksi_1 - ksi_3 - ksi_0) / dx;
      const Vec2r dksidy = 0.5 * (ksi_3 + ksi_2 - ksi_0 - ksi_1) / dy;
      Mat2r dksi, matF;
      dksi.col(0) = dksidx;
      dksi.col(1) = dksidy;
      const bool is_invertible = arma::inv(matF, dksi);
      EULSIM_FORCE_ASSERT_MSG(is_invertible, "dksi: " << dksi << " not invertible");
      const Mat2r matT = solid().material().stress(matF);
      // std::cout << arma::vectorise(matF - matT).t() ;

      // =================================
      // An idea to discretize the velocity derivative based on an upwind method.
      // Find the edge velocities
      // TODO: use a pointwise non-conservative upwind method...
      // const Vec2r vel_0 = velocity.vec_at(vid_0);
      // const Vec2r vel_1 = velocity.vec_at(vid_1);
      // const Vec2r vel_2 = velocity.vec_at(vid_2);
      // const Vec2r vel_3 = velocity.vec_at(vid_3);
      //
      // const Vec2r vel_e0ave = 0.5 * (vel_0 + vel_1);
      // const Vec2r vel_e1ave = 0.5 * (vel_1 + vel_2);
      // const Vec2r vel_e2ave = 0.5 * (vel_2 + vel_3);
      // const Vec2r vel_e3ave = 0.5 * (vel_3 + vel_0);
      // const Vec2r vel_e0 = (vel_e0(dimx) > 0 ? vel_0 : vel_1);
      // const Vec2r vel_e1 = (vel_e1(dimy) > 0 ? vel_1 : vel_2);
      // const Vec2r vel_e2 = (vel_e2(dimx) > 0 ? vel_3 : vel_2);
      // const Vec2r vel_e3 = (vel_e3(dimy) > 0 ? vel_0 : vel_3);


      // Assemble the nablavarea vectors
      // Mat2r nablavarea;
      // nablavarea.col(0) = +0.5 * vel_e0 * dy;
      // nablavarea.col(1) = +0.5 * vel_e3 * dx;
      // _nablavarea.add_vec_at(vid_0, arma::vectorise(nablavarea));
      //
      // nablavarea.col(0) = -0.5 * vel_e0 * dy;
      // nablavarea.col(1) = +0.5 * vel_e1 * dx;
      // _nablavarea.add_vec_at(vid_1, arma::vectorise(nablavarea));
      //
      // nablavarea.col(0) = -0.5 * vel_e2 * dy;
      // nablavarea.col(1) = -0.5 * vel_e1 * dx;
      // _nablavarea.add_vec_at(vid_2, arma::vectorise(nablavarea));
      //
      // nablavarea.col(0) = +0.5 * vel_e2 * dy;
      // nablavarea.col(1) = -0.5 * vel_e3 * dx;
      // _nablavarea.add_vec_at(vid_3, arma::vectorise(nablavarea));
      // =========================================


      // Assemble the divT vector
      const Vec2r Tcol0dy = matT.col(0) * dy * 0.5;
      const Vec2r Tcol1dx = matT.col(1) * dx * 0.5;
      _divstressarea.add_vec_at(vid_0, +Tcol0dy + Tcol1dx);
      _divstressarea.add_vec_at(vid_1, -Tcol0dy + Tcol1dx);
      _divstressarea.add_vec_at(vid_2, -Tcol0dy - Tcol1dx);
      _divstressarea.add_vec_at(vid_3, +Tcol0dy - Tcol1dx);

      // For testing, Find laplacian ksi
      if (SHOULD_FIND_LAPLACIAN_KSI == LAPLACIAN_KSI_FV)
        {
          const Vec2r dksicol0dy = dksi.col(0) * dy * 0.5;
          const Vec2r dksicol1dx = dksi.col(1) * dx * 0.5;
          _laplacianksiarea.add_vec_at(vid_0, +dksicol0dy + dksicol1dx);
          _laplacianksiarea.add_vec_at(vid_1, -dksicol0dy + dksicol1dx);
          _laplacianksiarea.add_vec_at(vid_2, -dksicol0dy - dksicol1dx);
          _laplacianksiarea.add_vec_at(vid_3, +dksicol0dy - dksicol1dx);
        }

      // assemble rho vector
      EULSIM_ASSERT(EULSIM_ABS(arma::det(dksi) * arma::det(matF) - 1.) < 1e-10);
      const double rhoatvertex = (arma::det(dksi) * solid().material().rho_0());
      _rhoarea.value_at(vid_0) += rhoatvertex;
      _rhoarea.value_at(vid_1) += rhoatvertex;
      _rhoarea.value_at(vid_2) += rhoatvertex;
      _rhoarea.value_at(vid_3) += rhoatvertex;
    }

  //
  // Now loop over vertices and plug in the gathered info
  //
  for (Index2d vid = grid().first_vertex(); !grid().is_end_vertex(vid); grid().advance_vertex(vid))
    {
      // Only update active vertices
      if (vertex_status(vid).vtype != VERTEX_ACTIVE)
        continue;

      const Vec2r v = velocity.vec_at(vid);

      // ========================
      // Find nablavarea here rahter than the cell iteration
      //
      Vec2r laplacianvelocity(arma::fill::zeros);

      {
        const Vec2r vr = velocity.vec_at({ vid(0) + 1, vid(1) });
        const Vec2r vl = velocity.vec_at({ vid(0) - 1, vid(1) });
        const Vec2r vu = velocity.vec_at({ vid(0), vid(1) + 1 });
        const Vec2r vb = velocity.vec_at({ vid(0), vid(1) - 1 });
        Mat2r nablavarea;
        nablavarea.col(0) = (v(0) > 0 ? (v - vl) : (vr - v)) * dy;
        nablavarea.col(1) = (v(1) > 0 ? (v - vb) : (vu - v)) * dx;
        _nablavarea.set_vec_at(vid, arma::vectorise(nablavarea));

        // Find laplacian of velocity for damping
        laplacianvelocity = (vr + vl - 2 * v) / dx / dx / 2.;
        laplacianvelocity += (vu + vb - 2 * v) / dy / dy / 2.;
      }
      // =========================


      const double area = dx * dy;
      const double rho = _rhoarea.value_at(vid) / 4.;
      const Mat2r nablav = arma::reshape(_nablavarea.vec_at(vid), 2, 2) / area;
      const Vec2r gravity = { 0, -_info.g };
      const Vec2r divT = _divstressarea.vec_at(vid) / area;

      const Vec2r rhsofthisvertex = //
          -nablav * v               //
          + divT / rho              //
          + gravity                 //
          + laplacianvelocity * _info.damping_velocity;

      rhs->set_vec_at(vid, rhsofthisvertex);
    }
}

void
ClampedSolidSimulator::compute_rhs_refmap(const DOFVector_2RV &velocity, const DOFVector_2RV &refmap,
                                          DOFVector_2RV *rhs)
{
  const Real dx = grid().delta(0);
  const Real dy = grid().delta(1);
  const int dimx = 0;
  const int dimy = 1;

  for (Index2d vid = grid().first_vertex(); !grid().is_end_vertex(vid); grid().advance_vertex(vid))
    {
      // Only active vertices
      if (vertex_status(vid).vtype != VERTEX_ACTIVE)
        continue;
      // No boundary active verts.
      EULSIM_FORCE_ASSERT_MSG(!grid().is_boundary_vertex(vid), "No active boundary vertex, increase grid lengths.");

      // Get adjacent vertex handles
      // clang-format off
      const Index2d vid_r = {vid(0) + 1, vid(1)};
      const Index2d vid_l = {vid(0) - 1, vid(1)};
      const Index2d vid_t = {vid(0)    , vid(1) + 1};
      const Index2d vid_b = {vid(0)    , vid(1) - 1};
      // clang-format on

      // Get adjacent refmap and velocity
      const Vec2r ksi_c = refmap.vec_at(vid);
      const Vec2r ksi_r = refmap.vec_at(vid_r);
      const Vec2r ksi_l = refmap.vec_at(vid_l);
      const Vec2r ksi_t = refmap.vec_at(vid_t);
      const Vec2r ksi_b = refmap.vec_at(vid_b);
      //
      const Vec2r vel_c = velocity.vec_at(vid);
      const Vec2r vel_r = velocity.vec_at(vid_r);
      const Vec2r vel_l = velocity.vec_at(vid_l);
      const Vec2r vel_t = velocity.vec_at(vid_t);
      const Vec2r vel_b = velocity.vec_at(vid_b);

      // Get the velocity at edge centers by averaging
      const Vec2r vel_er = 0.5 * (vel_c + vel_r);
      const Vec2r vel_el = 0.5 * (vel_c + vel_l);
      const Vec2r vel_et = 0.5 * (vel_c + vel_t);
      const Vec2r vel_eb = 0.5 * (vel_c + vel_b);

      // Get the derivative of the refmap at cell centers by
      // first order upwind.
      // Upwind 1
      const bool USE_UPWIND_METHOD_1 = false;
      Vec2r dksidx_c, dksidy_c;
      if (USE_UPWIND_METHOD_1)
        {
          const Vec2r ksi_er = (vel_er(0) > 0 ? ksi_c : ksi_r);
          const Vec2r ksi_el = (vel_el(0) > 0 ? ksi_l : ksi_c);
          const Vec2r ksi_et = (vel_et(1) > 0 ? ksi_c : ksi_t);
          const Vec2r ksi_eb = (vel_eb(1) > 0 ? ksi_b : ksi_c);

          // Find the gradient of refmap
          dksidx_c = (ksi_er - ksi_el) / dx;
          dksidy_c = (ksi_et - ksi_eb) / dy;
        }
      // Upwind 2
      else /* USE_UPWIND_METHOD_2 */
        {
          dksidx_c = (vel_c(0) > 0 ? ksi_c - ksi_l : ksi_r - ksi_c) / dx;
          dksidy_c = (vel_c(1) > 0 ? ksi_c - ksi_b : ksi_t - ksi_c) / dy;
        }

      // Set the rhs
      rhs->value_at(vid, 0) = -vel_c(dimx) * dksidx_c(0) - vel_c(dimy) * dksidy_c(0);
      rhs->value_at(vid, 1) = -vel_c(dimx) * dksidx_c(1) - vel_c(dimy) * dksidy_c(1);
    }
}

void
ClampedSolidSimulator::apply_initial_conditions(SolutionState *state)
{
  EULSIM_FORCE_ASSERT(_is_initialized);

  for (Index2d vid = grid().first_vertex(); !grid().is_end_vertex(vid); grid().advance_vertex(vid))
    {

      if (grid().is_boundary_vertex(vid))
        {
          EULSIM_FORCE_ASSERT(vertex_status(vid).vtype == VERTEX_INACTIVE);
        }
      if (vertex_status(vid).vtype == VERTEX_ACTIVE)
        {
          Vec2r v, k;
          solid().initial_solution(grid().vertex_xyz(vid), &v, &k);
          state->v.set_vec_at(vid, v);
          state->k.set_vec_at(vid, k);
        }
    } // End of for
} // All done

void
ClampedSolidSimulator::update_euler(SolutionState *state, arma::mat *residual)
{
  EULSIM_FORCE_ASSERT(_is_initialized);

  // Everything should be steady state so time should not matter.
  const double time = 0;

  // Update velocity normal euler
  // extrapolate_values(time, state->v, VAR_VELOCITY);
  // extrapolate_values(time, state->k, VAR_REFMAP);
  // compute_rhs_velocity(state->v, state->k, &_euler_interim.v);
  // compute_rhs_refmap(state->v, state->k, &_euler_interim.k);
  // state->k.set_axby(1, state->k, _info.dt, _euler_interim.k);
  // state->v.set_axby(1, state->v, _info.dt, _euler_interim.v);

  // sequential update
  extrapolate_values(time, state->v, VAR_VELOCITY);
  extrapolate_values(time, state->k, VAR_REFMAP);
  //
  compute_rhs_velocity(state->v, state->k, &_euler_interim.v);
  state->v.set_axby(1, state->v, _info.dt, _euler_interim.v);
  //
  extrapolate_values(time, state->v, VAR_VELOCITY);
  compute_rhs_refmap(state->v, state->k, &_euler_interim.k);
  state->k.set_axby(1, state->k, _info.dt, _euler_interim.k);


  if (residual)
    {
      NormCalculator nc_velocity_0, nc_velocity_1, nc_refmap_0, nc_refmap_1;
      for (Index2d vid = grid().first_vertex(); !grid().is_end_vertex(vid); grid().advance_vertex(vid))
        {
          if (vertex_status(vid).vtype == VERTEX_ACTIVE)
            {
              nc_velocity_0.add_value(_euler_interim.v.value_at(vid, 0));
              nc_velocity_1.add_value(_euler_interim.v.value_at(vid, 1));
              nc_refmap_0.add_value(_euler_interim.k.value_at(vid, 0));
              nc_refmap_1.add_value(_euler_interim.k.value_at(vid, 1));
            }
        }
      residual->resize(3, 4);
      nc_velocity_0.norms(&residual->at(0, 0), &residual->at(1, 0), &residual->at(2, 0));
      nc_velocity_1.norms(&residual->at(0, 1), &residual->at(1, 1), &residual->at(2, 1));
      nc_refmap_0.norms(&residual->at(0, 2), &residual->at(1, 2), &residual->at(2, 2));
      nc_refmap_1.norms(&residual->at(0, 3), &residual->at(1, 3), &residual->at(2, 3));
    }
} // All done

void
ClampedSolidSimulator::update_rk3(SolutionState *state, arma::mat *solutionchange)
{
  EULSIM_FORCE_ASSERT(_is_initialized);

  _rk3_interim_0.v.make_equal_to(state->v);
  _rk3_interim_0.k.make_equal_to(state->k);
  update_euler(&_rk3_interim_0);

  _rk3_interim_1.v.make_equal_to(_rk3_interim_0.v);
  _rk3_interim_1.k.make_equal_to(_rk3_interim_0.k);
  update_euler(&_rk3_interim_1);

  _rk3_interim_2.v.set_axby(0.75, state->v, 0.25, _rk3_interim_1.v);
  _rk3_interim_2.k.set_axby(0.75, state->k, 0.25, _rk3_interim_1.k);
  update_euler(&_rk3_interim_2);

  // state->v.set_axby(1. / 3, _rk3_interim_0.v, 2. / 3, _rk3_interim_2.v);
  // state->k.set_axby(1. / 3, _rk3_interim_0.k, 2. / 3, _rk3_interim_2.k);

  // Compute solution change if we were asked to
  const double onethird = 1. / 3;
  const double twothirds = 2. / 3;
  {
    NormCalculator nc_velocity_0, nc_velocity_1, nc_refmap_0, nc_refmap_1;
    for (Index2d vid = grid().first_vertex(); !grid().is_end_vertex(vid); grid().advance_vertex(vid))
      {
        if (vertex_status(vid).vtype == VERTEX_ACTIVE)
          {
            const double dvx = onethird * _rk3_interim_0.v.value_at(vid, 0) +
                               twothirds * _rk3_interim_2.v.value_at(vid, 0) - state->v.value_at(vid, 0);
            const double dvy = onethird * _rk3_interim_0.v.value_at(vid, 1) +
                               twothirds * _rk3_interim_2.v.value_at(vid, 1) - state->v.value_at(vid, 1);
            const double dkx = onethird * _rk3_interim_0.k.value_at(vid, 0) +
                               twothirds * _rk3_interim_2.k.value_at(vid, 0) - state->k.value_at(vid, 0);
            const double dky = onethird * _rk3_interim_0.k.value_at(vid, 1) +
                               twothirds * _rk3_interim_2.k.value_at(vid, 1) - state->k.value_at(vid, 1);
            //
            nc_velocity_0.add_value(dvx);
            nc_velocity_1.add_value(dvy);
            nc_refmap_0.add_value(dkx);
            nc_refmap_1.add_value(dky);
            //
            state->v.value_at(vid, 0) += dvx;
            state->v.value_at(vid, 1) += dvy;
            state->k.value_at(vid, 0) += dkx;
            state->k.value_at(vid, 1) += dky;
          }
      }
    solutionchange->resize(3, 4);
    nc_velocity_0.norms(&solutionchange->at(0, 0), &solutionchange->at(1, 0), &solutionchange->at(2, 0));
    nc_velocity_1.norms(&solutionchange->at(0, 1), &solutionchange->at(1, 1), &solutionchange->at(2, 1));
    nc_refmap_0.norms(&solutionchange->at(0, 2), &solutionchange->at(1, 2), &solutionchange->at(2, 2));
    nc_refmap_1.norms(&solutionchange->at(0, 3), &solutionchange->at(1, 3), &solutionchange->at(2, 3));
    (*solutionchange) /= _info.dt;
  } // End of computing update and the norms
}


void
ClampedSolidSimulator::write_vtk_ghost_rays(const std::string filename)
{
  // Find ghost points
  std::vector<Index2d> ghosts;
  for (Index2d vid = grid().first_vertex(); !grid().is_end_vertex(vid); grid().advance_vertex(vid))
    {
      if (vertex_status(vid).vtype == VERTEX_GHOST)
        {
          ghosts.push_back(vid);
        }
    }


  // Write the boundary projections
  std::vector<float> pts;
  std::vector<int> connectivity;
  std::vector<int> cell_types;

  for (int i = 0; i < (int)ghosts.size(); ++i)
    {
      VertexGhostStatus &vgs = vertex_status(ghosts[i]);

      // If the other weights are zero, it means that the point is on the bdry.
      if ((vgs.w1 == 0.) && (vgs.w2 == 0.))
        {
          EULSIM_FORCE_ASSERT(EULSIM_ABS(vgs.w0 - 1.) < 1e-10);
          continue;
        }

      // Make sure that weights make sense.
      const double tr = 1 - 1. / (1 - vgs.w0);
      const double te = (1 - tr) * vgs.w2;
      const double onemte = (1 - tr) * vgs.w1;
      EULSIM_FORCE_ASSERT(EULSIM_ABS(te + onemte - 1.) < 1e-10);

      Vec2r vg, vb, vstar, v0, v1;
      vg = grid().vertex_xyz(ghosts[i]);
      v0 = grid().vertex_xyz(vgs.p1);
      v1 = grid().vertex_xyz(vgs.p2);
      vb = solid().surface_point_xy(vgs.p0);
      vstar = v0 * onemte + v1 * te;

      connectivity.push_back(pts.size() / 3);
      connectivity.push_back(pts.size() / 3 + 1);
      connectivity.push_back(pts.size() / 3 + 2);
      cell_types.push_back(VISIT_TRIANGLE);
      pts.push_back(vg(0));
      pts.push_back(vg(1));
      pts.push_back(0);
      pts.push_back(vb(0));
      pts.push_back(vb(1));
      pts.push_back(0);
      pts.push_back(vstar(0));
      pts.push_back(vstar(1));
      pts.push_back(0);
    }


  VISIT_write_unstructured_mesh(filename.c_str(), 0, pts.size() / 3, pts.data(), cell_types.size(), cell_types.data(),
                                connectivity.data(), 0, NULL, NULL, NULL, NULL);
}

void
ClampedSolidSimulator::write_vtk_solution_str(const std::string filename, SolutionState *state)
{

  if (state)
    {
      SolutionState residual(grid());
      residual.k.set(0);
      residual.v.set(0);
      extrapolate_values(0, state->v, VAR_VELOCITY);
      extrapolate_values(0, state->k, VAR_REFMAP);
      compute_rhs_velocity(state->v, state->k, &residual.v);
      compute_rhs_refmap(state->v, state->k, &residual.k);

      std::vector<DOFVector_2RV*> data2 = { &state->v, &state->k, &residual.v, &residual.k };
      std::vector<DOFVector_1RV*> data1;
      std::vector<std::string> data_names = { "v", "ksi", "v_residual", "ksi_residual" };
      IO_write_vtk_grid(filename, grid(), data1, data2, data_names);
    }
  else
    {
      IO_write_vtk_grid(filename, grid());
    }
}

void
ClampedSolidSimulator::write_vtk_solution_unstr(const std::string filename, SolutionState *state)
{
  const int n_cells_max = grid().n_cells(0) * grid().n_cells(1);
  std::vector<float> pts;
  std::vector<int> connectivity;
  std::vector<int> cell_types;
  //
  pts.reserve(3 * n_cells_max);
  connectivity.reserve(4 * n_cells_max);
  cell_types.reserve(n_cells_max);
  //
  std::vector<float> vidf;
  std::vector<float> velf;
  std::vector<float> ksif;
  std::vector<float> xminusksi;
  std::vector<float> velerrf;
  std::vector<float> ksierrf;
  //
  vidf.reserve(n_cells_max * 3);
  velf.reserve(n_cells_max * 3);
  ksif.reserve(n_cells_max * 3);
  xminusksi.reserve(n_cells_max * 3);
  velerrf.reserve(n_cells_max * 3);
  ksierrf.reserve(n_cells_max * 3);
  //
  int n_vars = 1;
  int vardim[] = { 3, 3, 3, 3, 3, 3 };
  int centering[] = { 1, 1, 1, 1, 1, 1 };
  const char *const varnames[] = { "vid", "v", "ksi", "x-ksi", "v_residual", "ksi_residual" };
  if (state)
    {
      n_vars += 5;
    }


  SolutionState residual(grid());
  if (state)
    {
      residual.k.set(0);
      residual.v.set(0);
      extrapolate_values(0, state->v, VAR_VELOCITY);
      extrapolate_values(0, state->k, VAR_REFMAP);
      compute_rhs_velocity(state->v, state->k, &residual.v);
      compute_rhs_refmap(state->v, state->k, &residual.k);
    }

  for (Index2d cid = grid().first_cell(); !grid().is_end_cell(cid); grid().advance_cell(cid))
    {

      Index2d vid[4];
      vid[0] = { cid(0), cid(1) };
      vid[1] = { cid(0) + 1, cid(1) };
      vid[2] = { cid(0) + 1, cid(1) + 1 };
      vid[3] = { cid(0), cid(1) + 1 };

      if ((vertex_status(vid[0]).vtype != VERTEX_ACTIVE) && (vertex_status(vid[1]).vtype != VERTEX_ACTIVE) &&
          (vertex_status(vid[2]).vtype != VERTEX_ACTIVE) && (vertex_status(vid[3]).vtype != VERTEX_ACTIVE))
        continue;

      const int id = cell_types.size();
      //
      connectivity.push_back(4 * id);
      connectivity.push_back(4 * id + 1);
      connectivity.push_back(4 * id + 2);
      connectivity.push_back(4 * id + 3);
      //
      cell_types.push_back(VISIT_QUAD);
      //
      for (int i = 0; i < 4; ++i)
        {
          for (int j = 0; j < 2; j++)
            {
              pts.push_back(grid().vertex_xyz(vid[i])[j]);
              vidf.push_back(vid[i](j));
              velf.push_back(state->v.value_at(vid[i], j));
              ksif.push_back(state->k.value_at(vid[i], j));
              xminusksi.push_back(grid().vertex_xyz(vid[i])[j] - state->k.value_at(vid[i], j));
              velerrf.push_back(residual.v.value_at(vid[i], j));
              ksierrf.push_back(residual.k.value_at(vid[i], j));
            }
          pts.push_back(0);
          vidf.push_back(0);
          velf.push_back(0);
          ksif.push_back(0);
          xminusksi.push_back(0);
          velerrf.push_back(0);
          ksierrf.push_back(0);
        }
    }

  float *vars[] = { vidf.data(), velf.data(), ksif.data(), xminusksi.data(), velerrf.data(), ksierrf.data() };
  VISIT_write_unstructured_mesh(filename.c_str(), 1 /*binary*/, pts.size() / 3, pts.data(), cell_types.size(),
                                cell_types.data(), connectivity.data(), n_vars, vardim, centering, varnames, vars);
}



DOFVector_1RV &
ClampedSolidSimulator::get_rhoarea_dofvector()
{
  return _rhoarea;
}

DOFVector_2RV &
ClampedSolidSimulator::get_divstressarea_dofvector()
{
  return _divstressarea;
}

DOFVector_2RV &
ClampedSolidSimulator::get_laplacianksiarea_dofvector()
{
  return _laplacianksiarea;
}


DOFVector_4RV &
ClampedSolidSimulator::get_nablavarea_dofvector()
{
  return _nablavarea;
}

Grid &
ClampedSolidSimulator::grid()
{
  return _grid;
}

ClampedSolidSimulator::VertexGhostStatus &
ClampedSolidSimulator::vertex_status(Index2d i2d)
{
  return _vertex_status.value_at(i2d);
}

ClampedSolid &
ClampedSolidSimulator::solid()
{
  return _solid;
}

} // namespace eulsim
