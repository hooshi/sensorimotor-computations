\section{Results}
\label{sec:results}

%%=============================================================
%% Testing the Finite Difference Operators
%% =============================================================
\subsection{Testing the Finite Difference Operators}

To test the implementation of the finite-difference operators, the
manufactured solution,
\begin{equation}
  v =
  \begin{bmatrix}
    \cos(x)\cos(y) \\
    sin(2y)\sin(x)
  \end{bmatrix} \quad
  \xi=
  \begin{bmatrix}
     \cos(2x) \cos(y) \\
     \sin(2y) \cos(x)
  \end{bmatrix},
\end{equation}
is used to construct source terms $f_1$ and $f_2$,
\begin{equation}
    \begin{aligned}
      &f_1  = - v \cdot \nabla v + \frac{1}{\rho} \nabla \cdot T  \\
      &f_2   = - v \cdot \nabla \xi,
  \end{aligned}
\end{equation}
which can be evaluated both analytically, e.g., Matlab's symbolic engine, and
using the mentioned finite-difference operators. The norm of the
difference of the values obtained using these two methods should
approach zero with a rate of $O(h)$, where $h$ is the mesh length
scale. Figure~\ref{fig:manufactured} shows the $L_\infty$ norm of
the mentioned difference for a series of refined meshes starting from
$11 \times 9$ on the domain $[0,1]\times[0,1]$. The desired order of
accuracy is attained verifying the implementation of this part of the
project. For simplicity, the source terms are not evaluated at the
grid boundaries. Also, a very simple stress model
$T=[1, 5; 5, 9](\nabla \xi)^{-1}$ has
been used.


\begin{figure}
  \centering
  \begin{minipage}{0.46\textwidth}
    \includegraphics[width=0.98\textwidth]{img/convergence-fd-operators.pdf}
    \caption{Truncation error of the finite-difference operators with
      mesh refinement}
    \label{fig:manufactured}
  \end{minipage} \hfil
  % 
  \begin{minipage}{0.46\textwidth}
    \includegraphics[width=0.98\textwidth]{img/2d-washer/error-refinement.pdf}
    \caption{Discretization error in the solution of the annular
      washer problem with mesh refinement }
    \label{fig:washer-error}
  \end{minipage}
\end{figure}

%%=============================================================
%% 2-D Steady State -- Circular Washer Shear
%% =============================================================
\subsection{A 2-D Steady State Problem -- Annular Washer Shear}
In this problem which is taken from Kamrin \etal{} \cite{kamrin2012},
the outer boundary of a annular shear is rotated while its inner
boundary is held fixed. The solid and the mesh resemble those of
figure~\ref{fig:clamped-ghost}-a. The reference and physical solid
domains are both an annulus with inner radius of $0.1$ and an outer
radius of $0.4$. The outer boundary is rotated $\frac{\pi}{6}$ radians
counter-clockwise, yielding the boundary conditions
\[
  \begin{aligned}
    &\xi_x = r\cos(\theta) \quad &&\xi_y = r\sin(\theta) \quad &r=0.1 \\
    &\xi_x = r\cos(\theta-\pi/6) \quad &&\xi_y = r\sin(\theta-\pi/6) \quad &r=0.4 , \\
  \end{aligned}
\]
where $(r,\theta)$ is the polar coordinates of a point on the
boundary. The initial condition for $v$ is zero everywhere, while
$\xi_0$ is evaluated by linearly interpolating the boundary
conditions. The Levinson-Burgess model is used as the stress
constitutive relation
\begin{equation}
  T = f_1(I_3)B+f_2(I_1,I_3)I.
\end{equation}
Here, $B=(\nabla \xi)^{-1}(\nabla \xi)^{-T}$, $I$ is the identity
matrix, $I_1=\tr(B)$ and $I_3=\det(B)$ are the first and third
invariants of $B$, respectively, and
\[
  f_1=  \left( 3+\frac{1}{I_3} \right)\left( 4\sqrt{I_3}\right)^{-1}%
  \quad \quad %
  f_2= \left( %
    \sqrt{I_3}\left( %
      \frac{5}{6}+\frac{1-I_1}{4I_3^2}%
    \right)-\frac{4}{3} \right). %
\]

The problem is solved on a series of nested meshes, and the error in
the numerical solution is evaluated by comparison to its exact
counterpart $v=0$ and $\xi = (A - B/r^2) e_{\theta}$. As desired,
figure~\ref{fig:washer-error} shows that the discretization error is
reduced with mesh refinement.

The effect of mesh size and damping on the solver convergence are also
studied. Figure~\ref{fig:washer-convergence}-a shows the effect the
damping term for a mesh size of $43\times43$ and
$\Delta t = 0.1 \Delta x$. It is observed that the convergence is
considerably slow with no or small damping coefficients $\mu$, while
$\mu=10\Delta x \Delta y$ seems to accelerate the convergence the
most. Figure~\ref{fig:washer-convergence}-b shows the solver
convergence with $\Delta t=0.1 \Delta x$ and $\mu=10\Delta x\Delta y$
over different mesh sizes. Not surprisingly, the finer the mesh, the more iterations are required.


\begin{figure}
  \centering
  \includegraphics[width=0.48\linewidth]{img/2d-washer/convergence-experiments.pdf} \hfil
  \includegraphics[width=0.48\linewidth]{img/2d-washer/norms-with-refinement.pdf} \\
  \makebox[0.48\linewidth][c]{(a)} \hfil
  \makebox[0.48\linewidth][c]{(b)}  \\
  \caption{Solver convergence for the annular washer problem: (a) effect of damping (b) effect of mesh size}
  \label{fig:washer-convergence}
\end{figure}


%% =============================================================
%% 1-D Transient -- Oscillating Solid
%% =============================================================
\subsection{A 1-D Transient Problem -- Oscillating Solid}

In this problem, a 1-D solid is compressed to 0.1 of its original
volume and is then released with zero initial velocity, as shown in
figure~\ref{fig:1d-oscillating-schematic}. The simple stress model
$T= 1/\xi_x -1$ is used, along with a mesh size of 200, and a
time-step of $\Delta t = 0.1 \Delta x$. The solution at various times
is shown in figure~\ref{fig:1d-oscillating-solution}. As expected, the
solid keeps oscillating due to lack of damping.

\begin{figure}
  \centering
  \includegraphics[width=0.99\linewidth]{img/1d-oscillation/schematic.pdf}
  \caption{Schematic of the 1-D oscillating solid problem.}
  \label{fig:1d-oscillating-schematic}
  \vspace{0.5cm}
  \includegraphics[width=0.99\linewidth]{img/1d-oscillation/snapshots.pdf}
  \caption{Solution of the 1-D oscillating solid problem at various times.}
  \label{fig:1d-oscillating-solution}
\end{figure}

%% =============================================================
%% 1-D Transient -- Collisions
%% =============================================================
\subsection{A 1-D Transient Problem -- Collisions}
In this problem, a 1-D solid is placed between two fixed barriers
(shown in figure~\ref{fig:1d-sandwich-schematic}). By setting the
initial velocity to the uniform value of one, and using no initial
deformation, i.e., $\xi(x,t=0)=x$, the solid will keep colliding with
and bouncing off the barriers. This is consistent with the obtained
solution shown in figure~\ref{fig:1d-sandwich-solution}.

In this problem, the effect of mesh size on the solution is also
studied by solving the same problem on mesh sizes $200$, $400$, and
$800$. Figure~\ref{fig:1d-sandwich-mesh} shows the density profile at
various times when the solid is bouncing off the right barrier on the
finest mesh. Interestingly, the shape of the density profile seems to
be the same between the different solutions. On the other hand, the
coarse mesh solutions accumulate considerable amounts of phase lag
over time.

\begin{figure}
  \centering
  \includegraphics[width=0.99\linewidth]{img/1d-sandwich/schematic.pdf}
  \caption{Schematic of the 1-D collision problem.}
  \label{fig:1d-sandwich-schematic}
  \vspace{0.5cm}
  \includegraphics[width=0.99\linewidth]{img/1d-sandwich/snapshot.pdf}
  \caption{Solution of the 1-D collision problem at various times.}
  \label{fig:1d-sandwich-solution}
  \vspace{0.5cm}
  \includegraphics[width=0.99\linewidth]{img/1d-sandwich/mesh-size.pdf}
  \caption{Effect of mesh size on the solution to the 1-D collision
    problem. The density is shown at various times when the object on
    the finest mesh has collided with the right barrier and is being
    bounced back. As more time passes, the solution on the coarse
    meshes accumulate larger amounts of phase lag.}
  \label{fig:1d-sandwich-mesh}
\end{figure}

%% =============================================================
%% 2-D Transient -- Uniform Translation
%% =============================================================
\subsection{A 2-D Transient Problem -- Uniform Translation}
My attempts in dealing with 2-D transient problems have not been as
successful in the time frame of the project. The only problem I have
been able to touch is that of a circular solid with a uniform initial
velocity, no gravity, and no initial deformation. As one would expect,
the exact solution of this problem would only include uniform
translation according to the initial velocity, independent of the
stress model. A circular solid defined with the density field,
$\rho_0(\xi) = 1$ for $\|\xi\|_2^2 \leq 1$ and $0$ otherwise, is
placed in the domain $[-2,6]\times[-1.25,1.25]$ with a uniform
initial velocity $v_0=1$, and an initial reference map distribution of
$\xi=x$. To prevent additional hidden bugs affecting the results, the
stress term in the equations has been dropped, i.e., $T=0$.

Figure~\ref{fig:2d-translation} shows the solutions obtained from a
grid size of $160 \times 50$ and a time-step of $\Delta t = 0.01$. The
velocity extrapolation was only performed for vertices located within
a topological distance of 10 from the object. As expected, the
velocity field stays constant within the extrapolation range. The
reference map, however, does not seem to preserve its linear profile
within the solid, and builds up an increasing slope at the front of
the object. This in turn results in a non-constant density profile
with a growing kink at the front of the object. As a possible remedy,
the velocity can be extrapolated throughout the whole domain, rather
than just the vicinity of the object.

In addition to the previous bug, I have not been able to obtain any
results for a non-zero stress model. In such a case, the velocity
becomes oscillatory after some time, and the solution blows up. The
solver cannot be further developed until the culprit of these errors
are found and eliminated.

\begin{figure}
  \centering
  \includegraphics[width=0.99\linewidth]{img/2d-translation/compilation}
  \caption{Snapshots of the solution for the uniform translation
    problem of a 2-D circular solid. The figures from left to right
    show $t=0,0.2,$ and $0.4$, respectively. The upper figures show
    the density and velocity on the whole domain, while the lower ones
    are the plots of the quantities on the $x=0$ line.}
  \label{fig:2d-translation}
\end{figure}

%%% Local Variables:
%%% TeX-master: "report-0-main"
%%% End: