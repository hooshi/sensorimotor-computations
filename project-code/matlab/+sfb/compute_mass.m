function [mass] = compute_mass(x, xi, params)
% function [mass] = compute_mass(x, xi, params)

if( nargin == 0) run_min_example(); return; end

sfb.check_params(params);
n = size(x, 1);
dx = params.dx;
solid = params.solid; 
n_max_sample = params.n_sample;

mass = zeros(n, 1 );

vec = @(x) x(:);

%figure();
for i=1:(n-1)
    
    if( solid.is_refmap_inside( xi(i) ) ==  solid.is_refmap_inside( xi(i+1) ) )
        n_sample = 1;
    else
        n_sample = n_max_sample;
    end    
        
    dxtiny = dx/2/n_sample;
    dxidx = ( xi(i+1) - xi(i) ) / params.dx;
    grid0 = linspace( x(i), x(i)+dx/2, n_sample+1);
    grid0 = vec( grid0(1:end-1) + dxtiny/2 );
    grid1 = linspace( x(i)+dx/2, x(i+1), n_sample+1); 
    grid1 = vec( grid1(1:end-1) + dxtiny/2 );
    
    xi0 = ( (x(i+1)-grid0)*xi(i) + (grid0-x(i))*xi(i+1) ) / dx;
    xi1 = ( (x(i+1)-grid1)*xi(i) + (grid1-x(i))*xi(i+1) ) / dx;
    % norm(xi0 - grid0/0.39)
    % norm(xi1 - grid1/0.39)
    
    mass(i)   = mass(i)   + sum( solid.is_refmap_inside( xi0 ) * dxidx * dxtiny);
    mass(i+1) = mass(i+1) + sum( solid.is_refmap_inside( xi1 ) * dxidx * dxtiny);
    
    %hold on;
    %plot(grid0, solid.is_refmap_inside( xi0 ) , 'o')
    %plot(grid1, solid.is_refmap_inside( xi1 ) , 'o')
    %ylim([-0.5 1.5])
    %plot([0.39*-4 0.39*-4], [-0.5 1.5])
    %plot([0.39*4 0.39*4], [-0.5 1.5])
end

end

function run_min_example()
params = sfb.default_params();

n=33;
x = linspace(-6, 6, n);
x = x(:);
xi_interval = [-4, 4];
masse = xi_interval(2) - xi_interval(1);
params.solid = sfb.Solid(1, 0, 0.39, xi_interval, 1);
params.dx = ( x(end) - x(1) ) / (n-1);
params.x = x;

params.n_sample = 2;

for i=1:3
    [~,xi] = params.solid.initial_solution(x);
    mass = sfb.compute_mass(x, xi, params);
    fprintf("%4d %12.6e \n", params.n_sample, abs( masse-sum(mass) ) );
    params.n_sample = params.n_sample * 2;
end

end