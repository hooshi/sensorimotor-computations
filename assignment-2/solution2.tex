%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                            INCLUDES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[‎oneside‎,11pt]{article}

\usepackage[fleqn]{amsmath}
\usepackage{fullpage}
\usepackage[usenames,dvipsnames]{xcolor}
\usepackage{url}
\usepackage{graphicx}
\usepackage{parskip}
\usepackage{amssymb}
\usepackage{algorithm2e}
\usepackage{float}
\usepackage{listings} 
\usepackage{tikz}
\usepackage{bm}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                            MACROS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%  USE LIBERTINE
\usepackage[T1]{fontenc}
\usepackage{kpfonts}%  for math    
\usepackage{libertine}%  serif and sans serif
\usepackage[scaled=0.85]{beramono}%% mono
\setlength{\mathindent}{0pt}

%% hyperref
\usepackage[bookmarks,bookmarksnumbered,%
allbordercolors={0.2 0.6 0.6},%
linktocpage%
]{hyperref}

% Math
\DeclareMathOperator*{\argmin}{argmin}
\DeclareMathOperator*{\argmax}{argmax}

%% Vectors and matrices
\newcommand{\vvec}[1]{\boldsymbol{\mathbf{#1}}}
\newcommand{\nvec}[1]{\vvec {#1}}
\newcommand{\mmat}[1]{\bm{#1}}

%% Question
\definecolor{blu}{rgb}{0.2,0.0,0.7}
%%\newcommand{\question}[2]{\paragraph{#1}{\color{blu} #2}}
\newcommand{\question}[2]{\paragraph{#1}{\bf #2}}

\lstset{frame=single,
  language=Matlab,
  basicstyle=\ttfamily,
  morecomment=[s]{\%\{}{\%\}},
  }
  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
%%                            DOCUMENTS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\title{CPSC 530P Assignment 2/3}
\author{Shayan Hoshyari, 81382153}
\date{April 2018}
\maketitle

\section*{Question 2}

\question{2-1}{To simplify the problem, we will assume that
  Helmholtz's head is fixed in the world, with the $e_1$ axis of the
  head-fixed frame $h$ perpendicular to Listing's plane. An eye-fixed
  coordinate frame is attached to his left eye, $l$, and initially
  coincides with $h$.  Let us parameterize the rotation of the eye
  relative to the head using a rotation vector, $\bm\theta =
  n\theta$. That is, it is a rotation about the unit vector $n$ by an
  angle $\theta$.  

\begin{itemize}
\item[(a)] What is $^h_lE$ as a function of $\bm\theta$?
\item[(b)] What $\bm\theta$ if Listing's Law holds?
\end{itemize}
}

For (a), we know that $\exp[\bm\theta]$ would map the initial $l=h$
axis to the current $l$ axis. Therefore,
\[
  {}^he_{l,i}=\exp[\bm\theta]{}^he_{h,i} \quad i=1,2,3 \Rightarrow \exp[\bm\theta] =%
  [ {}^he_{l,1}| {}^he_{l,2}| {}^he_{l,3}]
\]
Also, from the definition of ${}^h_lE$ we have
\[
  {}^he_{l,i}=({}^h_lE)^{-1} {}^le_{l,i} \quad i=1,2,3 \Rightarrow%
  ({}^h_lE)^{-1} = [ {}^he_{l,1}| {}^he_{l,2}| {}^he_{l,3}]%
\]
Combining these two equalities yields: ${}^h_lE = \exp[\bm\theta]^{-1}$.


For (b), $\bm\theta=[0,\theta n_y, \theta n_z]$, when the head $x$
axis is perpendicular to the Listing's plane.

\question{2-2}{The eye as a camera. We can approximate the eye as a
  pinhole camera (this is a pretty good approximation, widely used in
  graphics and vision. For the purpose of this assignment, we can
  assume that all points in the world are at a fixed distance of 1
  unit from the eye. So, once the scene has been transformed to a
  “camera” coordinate frame c with the axes aligned properly with the
  image, the first two coordinates are the image coordinates. What is:
  \begin{itemize}
  \item[(a)] the transformmation matrix $^l_cE$
  \item[(b)] the $2\times3$ camera projection matrix from the $l$ coordinates
    to image coordinates?
  \end{itemize}
}

For (a), similar to part-1 we can find that:
$^l_cE=\left({}^{l}e_{c1} |{}^{l}e_{c2} |
  {}^{l}e_{c3}\right)$. Assuming that the camera coordinates is the
same as the eye coordinates, $^l_cE$ would just be unity.

For (b), we would only need to drop the depth entry of the vector.
Here we have assumed that $x$ represents the depth. Therefore, the
desired matrix would be:
\[
  ^l_iE=\begin{bmatrix}0 &1 &0 \\ 0 &0 &1 \\ \end{bmatrix}{}^l{}_cE
\]

\question{2-3}{ Rodrigues' formula. We saw in class that the
  exponential of a 3x3 skew-symmetric matrix is a rotation
  matrix. Rodrigues' formula is useful for explicitly computing it:
  $\exp[\bm\theta] = I + [n]\sin\theta + [n]2 (1 − \cos\theta)$.  To
  get a feel for it, verify this formula by comparing with the \texttt{expm}
  function in Matlab or SciPy. Describe your numerical experiment and
  results. No need for code.}

For various values of $\theta$, and $n$ I computed $\exp[n\theta]$ via
both methods and observed that the results were identical.

\question{2-4}{Listing-compatible Inverse Kinematics. This is the key
  step. Given a gaze vector $^hg$ in head coordinates, what is the
  rotation $\bm\theta$ that will point the eye at the target, and is
  compatible with Listing's law?}

We have to find $\bm \theta$ in terms of $^hg$. We know that
$\bm\theta=[0,n_y,n_z]^T\theta$, such that $n_x^2+n_z^2=1$. Replacing
these relations into the matrix exponential formula gives $\exp[\bm\theta]$ as:
\[
  \exp[\bm\theta] = \begin{bmatrix}
    \cos\theta &-n_z\sin\theta &n_y\sin\theta \\
    n_z\sin(\theta) &1-n_z^2(1-\cos\theta) &n_zn_y(1-\cos\theta) \\
    -n_y\sin(\theta) &-n_zn_y(1-\cos\theta) &1-n_y^2(1-\cos\theta) \\
  \end{bmatrix}
\]

We know that $\bm\theta$ describes the rotation of eye with respect to
the head, so applying $\exp[\bm\theta]$ to $^he_{h1}$ should give
$^hg$, i.e., $^hg$ would be the first column of $\exp[\bm\theta]$. Replacing in the derived formula for the matrix exponential gives:
\[
  \theta = \cos^{-1}(^hg_1); \quad n_y=\frac{^hg_2}{\sin\theta}; \quad -n_z=\frac{^hg_3}{\sin\theta}
\]

\question{2-5}{Write a short program in Matlab or Python (using SciPy)
  to compute the inverse kinematics solution, and the after-image seen
  at different locations in the visual field.  Your answer should 3
  include a brief mathematical description of what you are computing,
  and include an image of your results.}

Let's assume that we are looking at a grid printed on a curtain at
distance $d$ from our eye, such that when the $h$ and $l$ coordinate
frames are coinciding, we would be looking at the center of the grid.

For every grid point $^hx=(d,i,j)$, I find the correspoding gaze
vector $^hg=^hx/\|^hx\|$. Then, the horizontal grid
direction,$^he_{h,2}$ , will be transferred to the camera coordinates
$^ce_{h,2}={^l_cE}\ {^h_lE}\ {^he_{h,2}}$. The angle between the
quiver and the grid would then be then angle between the $x$ axis in
the camera $^ce_{c,1}$ and $^ce_{h,2}$. I finally draw a line with
this angle at location $(i,j)$.

Here is the resulting image.
\begin{center}
  \includegraphics[width=0.65\linewidth]{img/listings}
\end{center}

And here is the matlab code.
\begin{lstlisting}
clear all
close all

% Distance of the grid that we are looking at from the head
d = 1;

% Y axis of the head (also the grid).
% When we are looking at the center of the grid this should map
% to the x axis of the camera (eye).
gridh = [0, 1, 0];

% X axis of the camera.
gridc0 = [1, 0];

% Length of quivers when drawn.
qui = 0.3;

figure(1);
clf;
hold on;

% Loop over some points on the grid.
for i=-3:3
    for j=-3:3
        % Position of the point on the grid in the head coordinates.
        xh = [d, i, j];
        % Get the angle between the grid line and the camera x dir
        % both in the camera coordinate
        a = get_angle(xh, gridh, gridc0);
        % Draw a quiver with that angle.
        dir = qui * [cos(a), sin(a)];
        p = zeros(2,2);
        p(1,:) = [i,j] - dir;
        p(2,:) = [i,j] + dir;
        plot(p(:,1), p(:,2))
    end
end

% Show the grid 
grid on

% Revert the x-axis since in the camera the x-axis points to the left.
set(gca, 'XDir','reverse')
xlabel('x')
ylabel('y')
title(sprintf('what you see (grid is on a curtain at d=%.1f)',d))
print(gcf, '-dpdf', 'listings.pdf');

function theta=get_angle(xh, gridh, gridc0)
% Given gaze vector xh, finds the angle between grid_h and grid_c0 in
% camera coordinates.
    Ehl = e_hl(xh);
    gridl = Ehl * gridh(:);
    gridc = conv_lc(gridl);
    gridc = gridc ./ norm(gridc, 2);
    theta = acos( gridc(:)'*gridc0(:) );
    if(gridc(1)*gridc0(2)-gridc(2)*gridc0(1) < 0)
        theta = -theta; 
    end
end

function M = e_hl(gh)
% function M = e_hl(gaze)
% e_hl * x_h = x_l
% Moves from the head coordinates to the eye coordinates.

gh = gh / norm(gh, 2);
theta = acos(gh(1));

if( abs( sin(theta) ) > 1e-10 )
    nz =  gh(2) / sin(theta); 
    ny = -gh(3) / sin(theta);
else 
    ny = 1;
    nz = 0;
end

brn = [0, -nz, ny; nz, 0, 0; -ny, 0, 0];
M = eye(3) + sin(theta)*brn + (1-cos(theta))*brn*brn;

end

function xc = conv_lc(xl)
% function xc = e_lc(xl)
% convert eye coordinates to camera coordinates.
%xc = zeros(2,1);
%xc(1) = xl(2)/xl(1);
%xc(2) = xl(3)/xl(1);
    xc = xl(2:3);
end
\end{lstlisting}
    

\end{document}

%%% Local Variables:
%%% TeX-master: "solution2"
%%% End:
