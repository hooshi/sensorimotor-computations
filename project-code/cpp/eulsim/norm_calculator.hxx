#ifndef EULSIM_NORM_CALCULATOR_H
#define EULSIM_NORM_CALCULATOR_H

#include <eulsim/defs.hxx>

namespace eulsim
{


class NormCalculator
{
  double _max_value, _sum_abs, _sum_square;
  int n_values;

public:
  NormCalculator() : _max_value(0), _sum_abs(0), _sum_square(0), n_values(0) {}

  void
  add_value(double v)
  {
    _max_value = EULSIM_MAX(EULSIM_ABS(v), _max_value);
    _sum_abs += EULSIM_ABS(v);
    _sum_square += v * v;
    ++n_values;
  }

  void
  add_matrix(const arma::mat &mat)
  {
    const int n = mat.n_cols;
    const int m = mat.n_rows;
    _max_value = EULSIM_MAX(arma::norm(arma::abs(mat), "inf"), _max_value);
    _sum_abs += arma::norm(mat, 1);
    _sum_square += arma::norm(mat) * arma::norm(mat);
    n_values += (n * m);
  }

  void
  norms(double *l1, double *l2, double *linf)
  {
    if (l1)
      *l1 = _sum_abs / n_values;
    if (l2)
      *l2 = sqrt(_sum_square / n_values);
    if (linf)
      *linf = _max_value;
  }
};

} // namespace eulsim

#endif /* EULSIM_NORM_CALCULATOR_H */