classdef Barrier
   % Information about a Barrier that the solid can contact
    properties( Access = public )
        x
        direction % True would be to the right
    end
    
    methods
        %==========================================
        function this = Barrier(x, direction)
            % function this = Barrier(x, direction)
            % True would be to the right
            this.x = x;
            this.direction = logical(direction);
        end

        %==========================================
        function result = is_inside(this, x)
            % function result = is_inside(this, x)
            if( this.direction  )
                result = (x > this.x);
            else 
                result = (x < this.x);
            end
        end % end of initial solution
        
        %==========================================
        function result = signed_dist(this, x)
            % function result = signed_dist(this, x)
            if( this.direction )
                result = (this.x - x);
            else 
                result = (x - this.x);
            end
        end

    end
end


