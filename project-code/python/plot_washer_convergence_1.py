import numpy as np
import matplotlib.pyplot as plt

v1 = np.loadtxt('../results/washer/23/data.0.hst')
v2 = np.loadtxt('../results/washer/23/data.1.hst')
v = np.concatenate((v1, v2))
l1, = plt.semilogy( v[:,1], '-' , color='r')
plt.semilogy( v[:,3], '--', color='r' )

v1 = np.loadtxt('../results/washer/46/data.0.hst')
v2 = np.loadtxt('../results/washer/46/data.1.hst')
v = np.concatenate((v1, v2))
l2, =plt.semilogy( v[:,1], '-' , color='g')
plt.semilogy( v[:,3], '--', color='g' )

v1 = np.loadtxt('../results/washer/92/data.0.hst')
v2 = np.loadtxt('../results/washer/92/data.1.hst')
v = np.concatenate((v1, v2))
l3, =plt.semilogy( v[:,1], '-' , color='b')
plt.semilogy( v[:,3], '--', color='b' )

v1 = np.loadtxt('../results/washer/184/data.0.hst')
v2 = np.loadtxt('../results/washer/184/data.1.hst')
v3 = np.loadtxt('../results/washer/184/data.2.hst')
v4 = np.loadtxt('../results/washer/184/data.3.hst')
v5 = np.loadtxt('../results/washer/184/data.4.hst')
v = np.concatenate((v1, v2, v3, v4, v5))
l4, =plt.semilogy( v[:,1], '-' , color='m')
plt.semilogy( v[:,3], '--', color='m' )

plt.legend([l1, l2, l3, l4], ['23x23','46x46','92x92','184x184'])
plt.gca().set_xlabel('iteration')
plt.gca().set_ylabel('residual norm')
plt.savefig('washer-norms-with-refinement.pdf', bbox_inches='tight')
plt.show()
