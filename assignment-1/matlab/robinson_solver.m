function [E, t] = robinson_solver(tspan, funcR, y0, parameters)
%function [E, t] = robinson_solver(t, R, y0, parameters)
%   E, location of eye
%   tspan, [t0 tend]
%   t, time
%   R, firing rate
%   parameters, other parameters including Et, r, k

if nargin == 0
   test_robinson_solver() 
   return
end

assert( isa(funcR, 'function_handle')  );
 
% Create the RHS
dEdt = @(t_, E_) robinson_model(t_, E_, funcR, parameters);

% Solve
maxstep = (tspan(2)-tspan(1) )/100;
options = odeset('MaxStep', maxstep);
[E, t] = ode45(dEdt, tspan, y0, options);

end


function test_robinson_solver()

clear
close all

params = containers.Map('KeyType', 'char', 'ValueType', 'any');
params('k') = 4;
params('r') = 0.95;
params('Et') = 0;

tspan = [0, 5];
y0 = 0;
[E, t] = robinson_solver(tspan, @test_funcR, y0, params);

plot(E,t);

end


function val = test_funcR(t)
if( (t >= 0.) && (t < 1.) )
    val = 1;
else
    val = 0;
end
end