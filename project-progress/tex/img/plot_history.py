import numpy as np
import matplotlib.pyplot as plt

x = np.loadtxt('convergence-history.txt')

l1, = plt.semilogy( x[:,0], x[:,1], color='r', linewidth=.75 ) 
# plt.semilogy( x[:,0], x[:,2], color='g', linewidth=0.5 ) 
l2, = plt.semilogy( x[:,0], x[:,3], color='b', linewidth=0.75 ) 
# plt.semilogy( x[:,0], x[:,4], color='m', linewidth=0.5 ) 

plt.legend([l1,l2], [r'$\| D_1 \|_2$',r'$\| D_2 \|_2$'])
plt.ylabel('residual norm')
plt.xlabel('iteration')
plt.savefig('_convergence-history.pdf',bbox_inches='tight')

plt.show()
