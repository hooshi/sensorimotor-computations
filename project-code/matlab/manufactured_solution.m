%
% I will use a manufactured solution 
% v = [alpha*cos(x)*cos(y); beta*sin(x)*sin(2*y)]
% ksi = [gamma*cos(2*x)*cos(y); theta*cos(x)*sin(2*y)]
% To evaluate
% fv   (x,y) = (\nabla v) v + 1\rho \nabla . T + g
% fksi (x,y) = -(\nabla ksi) v
% For simplicity I will assum that T = C F where C is a 2x2 matrix.
% This is a simpler case compared to the general linear case

syms x y alpha beta gamma theta


% Velocity
vel = [alpha*cos(x)*cos(y); beta*sin(x)*sin(2*y)];

% Refmap
ksi = [gamma*cos(2*x)*cos(y); theta*cos(x)*sin(2*y)];

dvel = [diff(vel, x) diff(vel, y)];
dksi = [diff(ksi, x) diff(ksi, y)];
defgrad = inv(dksi);
laplacianksi = diff(dksi(:,1),x) + diff(dksi(:,2),y);
detdksi = det(dksi);
detdefgrad = det(defgrad);
divdefgrad = diff(defgrad(:,1),x) + diff(defgrad(:,2),y);


% To C
flid = fopen('manufactured_solution.txt','w');
fprintf(flid, "VEL \n%s \n\n", ccode(vel));
fprintf(flid, "KSI \n%s \n\n", ccode(ksi) );
fprintf(flid, "GRAD_VEL \n%s \n\n", ccode(dvel) );
fprintf(flid, "GRAD_KSI \n%s \n\n", ccode(dksi) );
fprintf(flid, "LAPLACIAN_KSI \n%s \n\n", ccode(laplacianksi) );
fprintf(flid, "det(GRAD_KSI) \n%s \n\n", ccode(detdksi) );
fprintf(flid, "F \n%s \n\n", ccode(defgrad) );
fprintf(flid, "det(F) \n%s \n\n", ccode(detdefgrad) );
fprintf(flid, "div(F) \n%s \n\n", ccode(divdefgrad) );
fclose(flid);