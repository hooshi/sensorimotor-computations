#define GETPOT_NAMESPACE pot
#include <contrib/getpot.hxx>
#include <contrib/visit_writer.h>

#include <eulsim/free_solid_simulator.hxx>
#include <eulsim/io.hxx>
#include <eulsim/solid.hxx>

using namespace eulsim;

class StVenantKirchhof : public Material
{
  double _mu, _lambda, _rho;

public:
  StVenantKirchhof(double rho = 1., double poisson_ratio = 0.25, double E = 1.)
  {
    _rho = rho;
    _lambda = poisson_ratio * E / (1 + poisson_ratio) / (1 - 2 * poisson_ratio);
    _mu = E / 2. / (1 + poisson_ratio);
  }

  double
  rho_0() override
  {
    return _rho;
  }

  Mat2r
  stress(Mat2r &F) override
  {
    //const Mat2r I(arma::fill::eye);
    //const Mat2r E = F.t() * F - I;
    //const double J = arma::det(F);
    //const Mat2r S = _lambda * (E(0, 0) + E(1, 1)) * I + 2 * _mu * E;
    //Mat2r sigma = 1. / J * F * S * F.t();

    // printf("J %lf \n", J);

    // const Mat2r sigma = arma::sqrt(  F * F.t() + Mat2r(arma::fill::eye)*1e-10  ) - Mat2r(arma::fill::eye) ;
    // std::cout << F * F.t()  << std::endl;
    // sigma.fill(0);
    Mat2r sigma(arma::fill::zeros);
    return sigma;
  }
};

// Circle centered at 0,0
class TravellingCircle : public FreeSolid
{
  double radius, strechx;
  Material &_material;
  double v0;

public:
  TravellingCircle(double sx, double r, Material &materialin, double v0in)
    : radius(r), strechx(sx), _material(materialin), v0(v0in)
  {
  }

  void
  initial_solution(const Vec2r &pt, Vec2r *velocity, Vec2r *refmap) override
  {
    // Find the unstreched config
    if (refmap)
      {
        (*refmap) = pt;
        (*refmap)(0) /= strechx;
      }
    if (velocity)
      {
        velocity->fill(0);
        velocity->at(0) = v0; // zero initial condition or uniform v
      }
  }

  bool
  is_in_refmap(const Vec2r &refmap) override
  {
    return arma::norm(refmap) < radius;
  }

  Material &
  material() override
  {
    return _material;
  }
};


int
main(int argc, char *argv[])
{
  pot::GetPot getpot(argc, argv);

  const double nx = getpot("nx", int(20));
  const double ny = getpot("ny", int(20));
  const double r = getpot("r", double(1));
  const double dt = getpot("dt", double(0.01));
  const double gravity = getpot("g", double(0));
  const int nt = getpot("nt", int(100));
  const double E = getpot("E", double(1));
  const double sx = getpot("sx", double(1));
  const int nwrite = getpot("nw", int(1));
  const double v0 = getpot("v0", double(0));
  // std::string ifname = getpot("iname", "");

  // CREATE THE GRID
  Grid grid(nx, ny, { -2, -1.25 }, { 6, 1.25 });

  // CREATE THE MATERIAL AND SOLID
  StVenantKirchhof material(1., 0.25, E);
  TravellingCircle solid(sx, r, material, v0);

  // SIMULATION INFO
  FreeSolidSimulator::SimulationInfo simulation_info;
  simulation_info.dt = dt;
  simulation_info.g = gravity;

  // SIMULATOR
  FreeSolidSimulator simulator(grid, solid, std::vector<const Obstacle *>(), simulation_info);

  // SOLUTION
  FreeSolidSimulator::SolutionState solution(grid);
  simulator.apply_initial_conditions(&solution);

  DOFVector_1RV is_valid(grid), mass(grid);
  is_valid.make_equal_to(solution.is_valid);
  simulator.compute_mass(solution.k, &solution.is_valid, &mass);
  IO_write_vtk_grid("travelling-init.0.vtk", grid, { &is_valid, &mass }, { &solution.v, &solution.k },
                    { "is_valid", "mass", "v", "xi" });



  {
    std::ofstream hstfile("history.hst", std::fstream::out | std::fstream::binary);
    hstfile << "# dt = " << simulator.info().dt << "\n";
    hstfile << "# g = " << simulator.info().g << "\n";

    for (int i = 1; i < nt; ++i)
      {

        arma::mat residual_norms;
        simulator.update_all(&solution, &residual_norms);

        std::cout << i << std::scientific << std::setprecision(6)               //
                  << " " << residual_norms(0, 1) << " " << residual_norms(1, 1) //
                  << " " << simulator.get_total_mass() << "\n";
        hstfile << i << std::scientific << std::setprecision(6)               //
                << " " << residual_norms(0, 1) << " " << residual_norms(1, 1) //
                << " " << simulator.get_total_mass() << "\n";


        if (i % nwrite == 0)
          {
            std::stringstream ss;

            ss << "travelling-init." << i << ".vtk";
            is_valid.make_equal_to(solution.is_valid);
            IO_write_vtk_grid(ss.str(), grid, { &is_valid, &simulator.get_mass_dofvector() },
                              { &solution.v, &solution.k }, { "is_valid", "mass", "v", "xi" });

            // Triangulation tri;
            // Grid subgrid(16 * nx, 16 * ny, { -1.5, -1.5 }, { 1.5, 1.5 });
            // ImplicitFunction *fn = simulator.is_mass_at_functor(solution.k);
            // Geometry_extract_zero_surface(*fn, subgrid, NULL, &tri, 16);
            // delete fn;
            // ss.str(""); ss << "travelling-tri." << i << ".vtk";
            // IO_write_vtk_triangulation(ss.str(),tri);
          }
      }

    // solution.v.print_on_grid(stdout);
  }



  return 0;
}
