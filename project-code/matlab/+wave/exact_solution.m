function [v, xit] = exact_solution(x, t, params)
% function [v,xi] = exact_solution(x, t, params)

if( nargin == 0) run_min_example(); return; end

wave.check_params( params );
assert( isequal( size(t), size(t(1)) ) );

z = x - params.c * t;

xit = 0.5 * erf( z );
% xi = erf(z);
denom = 1 + 1. / sqrt(pi) * exp(-(z.^ 2));
v  = params.c * (1 - 1 ./ denom );
end


function run_min_example()
params = struct;
params.c = 1;
params = wave.default_params(params);

t = 3;
x = linspace(-5, 5, 100);
x = x(:);

[v, xit] = wave.exact_solution(x, t, params);
figure();
hold off; plot( x, v, 'b', 'linewidth', 2);
hold on; plot( x, xit, 'r', 'linewidth', 2);
end
