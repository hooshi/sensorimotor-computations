/** dof_vector.hxx
 *
 * Vector for storing degrees of freedom.
 */

#ifndef EULSIM_DOF_VECTOR_H
#define EULSIM_DOF_VECTOR_H

#include <cstdio>
#include <fstream>
#include <iomanip>
#include <string>

#include <eulsim/defs.hxx>
#include <eulsim/grid.hxx>
#include <eulsim/norm_calculator.hxx>

namespace eulsim
{


enum StorageType
{
  STORAGE_VERTICES,
  STORAGE_EDGES_X,
  STORAGE_EDGES_Y,
  STORAGE_CELLS
};

//
// DOFVector
//
// A wrapper around a vector that stores a quantity on the mesh.
// It supports storing values at the vertices, at the cell centers, or on
// the edges.
template <typename DATATYPE, Index_t BLOCKSIZE, StorageType STORAGETYPE>
class DOFVector
{
public:
  typedef DOFVector<DATATYPE, BLOCKSIZE, STORAGETYPE> CurrentType;
  typedef DATATYPE DataType;
  const static Index_t block_size = BLOCKSIZE;
  const static StorageType storage_type = STORAGETYPE;

  DOFVector(Grid &);
  virtual ~DOFVector();

  void print_on_grid(FILE * = stdout, int block = 0);

  DataType &value_at(Index2d id, int block = 0);
  const DataType &value_at(Index2d id, int block = 0) const;

  Index_t n_values() const;
  Index_t n_values(int dim) const;
  Real origin(int dim) const;

  template <typename DATATYPE2>
  void make_equal_to(DOFVector<DATATYPE2, BLOCKSIZE, STORAGETYPE> &other);

  void set(const DataType &v);
  void set_axby(const double a, const CurrentType &x, const double b, const CurrentType &y);

  const Grid &grid() const;
  Grid &grid();
  std::vector<DataType> &get_values();

  // IO
  void write_ascii(std::string fname);
  void read_ascii(std::string fname);

protected:
  Grid &_grid;
  std::vector<DataType> _values;
};

template <Index_t BLOCKSIZE, StorageType STORAGETYPE>
class DOFVector_R : public DOFVector<Real, BLOCKSIZE, STORAGETYPE>
{
public:
  typedef DOFVector<Real, BLOCKSIZE, STORAGETYPE> BaseType;
  typedef DOFVector_R<BLOCKSIZE, STORAGETYPE> CurrentType;
  typedef arma::Col<Real>::fixed<BLOCKSIZE> VectorType;
  typedef arma::Mat<Real>::fixed<BLOCKSIZE, 2 /*dim*/> GradType;

  DOFVector_R(Grid &g) : BaseType(g) {}

  VectorType vec_at(Index2d id) const;
  void set_vec_at(Index2d id, const VectorType &);
  void add_vec_at(Index2d id, const VectorType &);
  void get_norms(double *l1, double *l2, double *linf) const;
};

typedef DOFVector_R<4, STORAGE_VERTICES> DOFVector_4RV;
typedef DOFVector_R<2, STORAGE_VERTICES> DOFVector_2RV;
typedef DOFVector_R<1, STORAGE_VERTICES> DOFVector_1RV;
typedef DOFVector<float, 1, STORAGE_VERTICES> DOFVector_1FV;

// boolean but since vec(bool) is crap, use char
template <StorageType STORAGETYPE>
using DOFVector_1C = DOFVector<char, 1, STORAGETYPE>;
typedef DOFVector_1C<STORAGE_VERTICES> DOFVector_1CV;

#include <eulsim/dof_vector_meat.hxx>

} // namespace eulsim

#endif /*EULSIM_DOF_VECTOR_H*/
