classdef Solid
   % Information about a free solid to be simulated
    properties
        scenario_id
        model_type
        bounds
        v0
        stretch
        C
    end
    
    methods
        %==========================================
        function this = Solid(scenario_id, v0, stretch, bounds, C, model_type)
            % function this = Solid(scenario_id, v0, stretch, bounds, C, model_type)
            if((nargin < 1) || isempty(scenario_id)) scenario_id = 1; end
            if((nargin < 2) || isempty(v0)         ) v0 = 0;          end
            if((nargin < 3) || isempty(stretch)    ) stretch = 1;     end
            if((nargin < 4) || isempty(bounds)     ) bounds = [-1 1]; end
            if((nargin < 5) || isempty(C)          ) C = 1;           end
            if((nargin < 6) || isempty(model_type))  model_type = 1; end
            
            this.scenario_id = scenario_id;
            this.v0 = v0;
            this.stretch = stretch;
            this.bounds = bounds;
            this.C = C;
            this.model_type = model_type;
        end
        
        %==========================================
        function [v, xi] = initial_solution(this, x)
            % function [v, xi] = initial_solution(x)
            
            switch this.scenario_id
                case 1
                    v = zeros( size(x) ) + this.v0;
                     % if you squish the solid by r=0.1, xi0 would be 10x
                    xi =  x ./ this.stretch;
                otherwise
                    assert(0);
            end
            
        end % end of initial solution
        
        %==========================================
        function result = is_refmap_inside(this, xi)
            % function result = is_refmap_inside(this, xi)
            result = (xi < this.bounds(2)) & (xi > this.bounds(1));
        end % end of initial solution
        
        %==========================================
        function result = refmap_signed_dist(this, xi)
            dist1 = abs( this.bounds(1) - xi );
            dist2 = abs( this.bounds(2) - xi );
            result = min( dist1, dist2 );
            result( this.is_refmap_inside( xi ) ) = -result( this.is_refmap_inside( xi ) );
        end
        
        %==========================================
        function T = cauchy_stress(this, dxidx)
            switch this.model_type
                case 1
                    T = this.C * (1 ./ (dxidx) - 1);
                otherwise 
                    assert( 0 )
            end
        end
    end
end


