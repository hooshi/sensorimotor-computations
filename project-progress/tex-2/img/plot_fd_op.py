import numpy as np
import matplotlib.pyplot as plt

linf = [
    [4.8831e-01, 6.0481e-01, 7.8206e-02, 6.1827e-02, ],
    [3.1836e-01, 4.4212e-01, 4.0191e-02, 3.1792e-02, ],
    [1.3411e-01, 2.1873e-01, 2.0351e-02, 1.6061e-02, ],
    [4.3614e-02, 8.2277e-02, 1.0237e-02, 8.0650e-03, ],
]
h0 = 1./11/9
h = [h0, h0/2, h0/4, h0/8]

h = np.array(h)
linf = np.array(linf)

opt = dict(linewidth=2)
l1, = plt.loglog(h, linf[:,0],'r-o',**opt)
l2, = plt.loglog(h, linf[:,1],'g-o',**opt)
l3, = plt.loglog(h, linf[:,2],'b-o',**opt)
l4, = plt.loglog(h, linf[:,3],'m-o',**opt)
l5, = plt.loglog(h, h, 'k--',**opt)

# plt.xlim([1e-3, 1e-2])
# plt.locator_params(axis='x', numticks=3)

plt.xlabel('h')
plt.ylabel(r'$L_\infty$ norm of discretization error')

plt.figlegend( [l1,l2,l3,l4,l5],
            [r'$f_{1x}$', r'$f_{1y}$', r'$f_{2x}$', r'$f_{2y}$', r'$O(h)$'],
            'upper right')

plt.savefig('_convergence-fd-operators.pdf', bbox_inches='tight')
plt.show()
