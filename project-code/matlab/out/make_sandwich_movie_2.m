clear

fig = figure('pos',[10 10 900 300])

MOVIE = 0;

if(MOVIE)
    k=1;
    filename = 'sandwich_2.gif'
    for i=10:10:6000

        %data1 = load(sprintf("sandwich_c10_n200/%d.dat", i), '-mat');
        %data2 = load(sprintf("sandwich_c10_n400/%d.dat", i*2), '-mat');
        data3 = load(sprintf("sandwich_c10_n800/%d.dat", i*4), '-mat');
        t = data3.data{1};
        %dx1 = data1.data{2}; dx1 = dx1(2)-dx1(1);
        %dx2 = data2.data{2}; dx2 = dx2(2)-dx2(1);
        dx3 = data3.data{2}; dx3 = dx3(2)-dx3(1);
        
        
        
        hold off, plot([1.2, 1.2], [-2, 2], 'm--');
        hold on, plot([-2, -2], [-2, 2], 'm--');
        %hold on, l1=plot( data1.data{2}, data1.data{5}/dx1, 'r', 'linewidth', 2);
        %hold on, l2=plot( data2.data{2}, data2.data{5}/dx2, 'g', 'linewidth', 2);
        hold on, l1=plot( data3.data{2}, data3.data{3}, 'b', 'linewidth', 2);
        hold on, l2=plot( data3.data{2}, data3.data{4}, 'r', 'linewidth', 2);
        hold on, l3=plot( data3.data{2}, data3.data{5}/dx3, 'k', 'linewidth', 2);
        legend([l1,l2,l3],{'v','xi','rho'}, 'Location', 'southeast')
        ylim([-1.5 1.5])
        title(sprintf("t=%.4f", t))
        
        f = getframe(fig);
        [im, cm] = rgb2ind(frame2im(f),256);
        if k == 1
            imwrite(im,cm,filename,'gif', 'DelayTime', 0,'Loopcount',inf);
        else
            imwrite(im,cm,filename,'gif','WriteMode','append', 'DelayTime', 0);
        end
        k=k+1;
    end
else
    for i=[10, 200, 400, 800, 1000, 1200, 1400, 1600, 1800]

        data3 = load(sprintf('sandwich_c10_n800/%d.dat', i*4), '-mat');
        t = data3.data{1};
        dx3 = data3.data{2}; dx3 = dx3(2)-dx3(1);
                
        hold off, plot([1.2, 1.2], [-2, 2], 'm--', 'linewidth', 2);
        hold on, plot([-2, -2], [-2, 2], 'm--', 'linewidth', 2);
  
        hold on, l1=plot( data3.data{2}, data3.data{3}, 'b', 'linewidth', 2);
        hold on, l2=plot( data3.data{2}, data3.data{4}, 'r', 'linewidth', 2);
        hold on, l3=plot( data3.data{2}, data3.data{5}/dx3, 'k', 'linewidth', 2);
        legend([l1,l2,l3],{'v','xi','rho'}, 'Location', 'southeast')
        ylim([-1.5 1.5])
        title(sprintf('t=%.4f', t))
        saveas(gcf, sprintf('bounce2-%d.svg', i))
    end
end
