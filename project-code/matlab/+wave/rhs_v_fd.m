function rhs = rhs_v_fd(v, xi, params)
% function rhs = rhs_v_upwind(v, xit, params)

if(nargin == 0 ) run_min_example(); return; end

wave.check_params(params);

dx = params.dx;
c = params.c;

v_plus = circshift(v, -1);
v_minus = circshift(v, 1);
v_plus(end)= 2*v_plus(end-1)-v_plus(end-2); %extrapolate from inside
v_minus(1) = 2*v_minus(2)    -v_minus(3); %extrapolate from inside

xi_plus = circshift(xi, -1);
xi_minus = circshift(xi, 1);
%xi_plus(end)= xi_plus(end-1);  % Neumann
%xi_minus(1) = xi_minus(2);     % Neumann
xi_plus(end)= 2*xi_plus(end-1)-xi_plus(end-2); %extrapolate from inside
xi_minus(1) = 2*xi_minus(2)    -xi_minus(3);      %extrapolate from inside

dxidx = (xi_plus - xi_minus) / 2 / dx;
rho = (dxidx + 1);

dxidx_edge = (xi_plus - xi) /dx;
dxidx_edge_minus = circshift(dxidx_edge, 1);
dxidx_edge_minus(1) = (xi_minus(2) - xi_minus(1)) / dx;
T_plus = c*c*( 1 ./ (dxidx_edge + 1) - 1 );
T_minus = c*c*( 1 ./ (dxidx_edge_minus + 1) - 1 );
dTdx = (T_plus - T_minus) / dx;

vdvdx = [];
switch params.advm
    case "center"
        vdvdx = v .* (v_plus - v_minus) / 2 / dx;
    case "upwind_fd"
        vpos = max(v, 0);
        vmin = min(v, 0);
        vdvdx = vpos .* (v - v_minus) / dx + vmin .* (v_plus - v) / dx;
    case "upwind_fv"
        % Try but probably not worth, go for semi-Lagrangian.
        assert(0);
end

rhs =  -vdvdx + dTdx./rho;
% rhs = -vdvdx;

end

function run_min_example()


size=20;
c = 1;

syms  xs
v_sym =  symfun( sinh( 1.3*sin( 2*pi*xs ) ), xs);
xi_sym =  symfun( erf((xs-0.5)*5), xs);
dv_sym =  diff(v_sym, xs);
dxi_sym = diff(xi_sym, xs);
dT_sym = diff(  c^2 * (1 ./ (dxi_sym + 1) - 1), xs);
vdv_sym = v_sym*dv_sym;
rho_sym = (dxi_sym + 1);

rhs_sym = -vdv_sym + dT_sym / rho_sym;
num_funs = matlabFunction(v_sym, xi_sym, rhs_sym);

params = wave.default_params();
params.advm = "upwind_fd";
params.c = c;

for i=1:5
    x = linspace(0, 1, size); x = x(:);
    dx = 1 / (size - 1);
    params.dx = dx;

    
    [v, xi, rhs_exact] =  num_funs(x);
    rhs_fd = wave.rhs_v_fd(v, xi, params);
    
    if(i==3)
        hold off, plot(x, v,'b','linewidth',2);
        hold on, plot(x, xi,'r','linewidth',2);
        hold on, plot(x(2:end-1), rhs_fd(2:end-1),'g','linewidth',2);
        hold on, plot(x(1:2:end), rhs_exact(1:2:end),'mo','linewidth',2);
    end
    
    rhs_exact = rhs_exact(2:end-1);
    rhs_fd = rhs_fd(2:end-1);
    fprintf("%5d %15.6e %15.6e %15.6e \n", size, ...
        max(rhs_exact), max(rhs_fd), max(rhs_exact - rhs_fd) );
    size = size * 2;
end

end