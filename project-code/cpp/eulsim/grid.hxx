/** grid.hxx
 *
 * Abstraction for a grid.
 *
 * Shayan Hoshyari February 2018
 */

#ifndef EULSIM_GRID_H
#define EULSIM_GRID_H

#include <string>

#include <eulsim/defs.hxx>

namespace eulsim
{

//
// Edge, cell and vertex numbering in the grid follow this pattern:
//
//   x (i,j+1)-(i,j+1)--x(i+1,j+1)
//   |                  |
//   |                  |
//   (i,j)    (i,j)     (i+1,j)
//   |                  |
//   x (i,j)--(i,j)---- x(i+1,j)
//
class Grid
{
public:
  Grid(int n_cells_x, int n_cells_y, Vec2r lower_left, Vec2r top_right);
  ~Grid();
  Vec2r cell_center_xyz(Index2d) const;
  Vec2r vertex_xyz(Index2d) const;
  Vec2r edge_center_xyz(Index2d, int alignment) const;

  Real delta(int dim) const;
  Real length_max(int dim) const;

  int n_cells(int dim) const;
  int n_edges(int dim, int alignment) const;
  int n_verts(int dim) const;

  std::vector<Index2d> vertex_neighbour_vertices(const Index2d vid) const;
  std::vector<Index2d> vertex_star_vertices(const Index2d vid) const;
  std::vector<Index2d> vertex_neighbour_cells(const Index2d vid) const;
  std::vector<Index3d> vertex_neighbour_edges(const Index2d vid) const;
  std::vector<Index3d> vertex_nearby_edges(const Index2d vid, const int depth) const;
  void edge_vertices(const Index3d eid, Index2d *v0, Index2d *v1);

  // Iterate over vertices
  Index2d first_vertex();
  void advance_vertex(Index2d &);
  bool is_end_vertex(const Index2d &);
  bool is_boundary_vertex(const Index2d &);

  // Iterate over vertices
  Index2d first_cell();
  void advance_cell(Index2d &);
  bool is_end_cell(const Index2d &);
  bool is_boundary_cell(const Index2d &);

  // Smaller grid around a vertex
  Grid subsampling_grid_vertex(const Index2d&, int level=0);

  // read and write
  void write_ascii(std::string);
  void read_ascii(std::string);
  void write_stream(std::ostream &);
  void read_stream(std::istream &);

private:
  // The dimensions of the domain.
  Real _x_from, _x_to;
  Real _y_from, _y_to;
  Real _delta_x;
  Real _delta_y;

  // Number of cells in each direction
  Index_t _n_cells_x;
  Index_t _n_cells_y;
};

} // namespace eulsim

#endif /*EULSIM_GRID_H*/
