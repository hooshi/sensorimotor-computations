#include <eulsim/defs.hxx>
#include <eulsim/grid.hxx>
#include <eulsim/geometry.hxx>

using namespace eulsim;

namespace  test_intersection_nsp
{

static int
_line_helper (const Vec2r y0, const Vec2r y1, const Vec2r x0, const Vec2r x1)
{
  const Real tol = 1e-13;

  const Vec2r tgny = y1 - y0;
  const Vec2r tgnx = x1 - x0;
  bool are_parallel = EULSIM_ABS (arma_cross2d (tgny, tgnx)) < tol;

  Real tx;
  bool do_intersect;
  do_intersect = Geometry_line_intersection (y0, y1, x0, x1, &tx);

  std::string str_x0 = eulsim::arma_print (x0);
  std::string str_x1 = eulsim::arma_print (x1);
  std::string str_y0 = eulsim::arma_print (y0);
  std::string str_y1 = eulsim::arma_print (y1);

  if (are_parallel)
    {
      EULSIM_FORCE_ASSERT (!do_intersect);
    }
  else
    {
      EULSIM_FORCE_ASSERT (do_intersect);
      Real ty;
      do_intersect = Geometry_line_intersection (x0, x1, y0, y1, &ty);
      EULSIM_FORCE_ASSERT (do_intersect);

      Vec2r inter0 = x0 + (x1 - x0) * tx;
      Vec2r inter1 = y0 + (y1 - y0) * ty;

      const double diff = arma::norm (inter0 - inter1);
      EULSIM_FORCE_ASSERT (diff < tol);
    }

  return 0;
}

class Ellipse : public ImplicitFunction
{
public:
  double
  eval (Vec2r x)
  {
    Vec2r z = (x - x0) / radius;
    return arma::dot (z, z) - 1;
  }

  Ellipse (Vec2r x0in, Vec2r rin) : x0 (x0in), radius (rin) {}

  Vec2r x0;
  Vec2r radius;
};

static int
_ellipse_helper (Ellipse &el, const Vec2r x0, const Vec2r x1)
{
  const double tol = 1e-10;
  const int n_iter = 100;
  //
  // Find analytical solution
  //
  Mat2r M ({{1 / el.radius (0) * el.radius (0), 0}, {0, 1 / el.radius (1)}});
  M = M % M;
  Vec2r x0ma = x0 - el.x0;
  Vec2r alpha = x1 - x0;

  const double c = arma::as_scalar (x0ma.t () * M * x0ma) - 1;
  const double b = arma::as_scalar (x0ma.t () * M * alpha);
  const double a = arma::as_scalar (alpha.t () * M * alpha);

  std::vector<double> tvals;
  if (b * b - a * c > 0)
    {
      double t = (-b - sqrt (b * b - a * c)) / a;
      if ((t > 0) && (t < 1))
        {
          const double res = el.eval (x0 + (x1 - x0) * t);
          EULSIM_FORCE_ASSERT (EULSIM_ABS (res) < tol);
          tvals.push_back (t);
        }
      t = (-b + sqrt (b * b - a * c)) / a;
      if ((t > 0) && (t < 1))
        {
          const double res = el.eval (x0 + (x1 - x0) * t);
          EULSIM_FORCE_ASSERT (EULSIM_ABS (res) < tol);
          tvals.push_back (t);
        }
    }

  //
  // Use my implementation
  //
  bool is_intersecting;
  double t_num;
  is_intersecting = Geometry_signed_distance_intersection (el, x0, x1, &t_num, n_iter);

  if (is_intersecting)
    {
      double res = el.eval (x0 + (x1 - x0) * t_num);
      printf ("    Ellipse intersection: Code gives t=%.5g (%.2e), Analytical "
              "gives: ",
              t_num, res);
      for (int i = 0; i < (int) tvals.size (); ++i)
        {
          res = el.eval (x0 + (x1 - x0) * tvals[i]);
          printf ("%.5g (%.2e)", tvals[i], res);
        }
      printf ("\n");

      EULSIM_FORCE_ASSERT (EULSIM_ABS (res) < tol);
    }
  else
    {
      printf ("    Ellipse intersection: Not intersecting. \n");
    }

  EULSIM_FORCE_ASSERT (bool(tvals.size ()) == is_intersecting);
  if (tvals.size () == 1)
    {
      EULSIM_FORCE_ASSERT (EULSIM_ABS (t_num - tvals[0]) < tol);
    }
  else if (tvals.size () == 2)
    {
      EULSIM_FORCE_ASSERT ((EULSIM_ABS (t_num - tvals[0]) < tol)
                           || (EULSIM_ABS (t_num - tvals[1]) < tol));
    }

  return 0;
}

} // end of test_intersection_nsp

int
test_intersection ()
{
  //
  // Line Line intersection
  //
  Vec2r pts[][4] = {{"-1.88,1.60", "-0.8,-1.05", "0.57,2.72", "1.77,0.07"},
                    {"-1.88,1.60", "-0.8,-1.05", "0.57,2.72", "4.2, 3.57"}};
  for (int i = 0; i < 2; ++i)
    {
      test_intersection_nsp::_line_helper (pts[i][0], pts[i][1], pts[i][2], pts[i][3]);
    }

  //
  // Line, signed distance intersection
  //
  test_intersection_nsp::Ellipse el ({0, 0}, {1, 0.5});
  // std::cout << "Ellipse " << el.x0.t() << " " << el.radius.t() << "\n";
  test_intersection_nsp::_ellipse_helper (el, {0.5, 0}, {1.6, 0});
  test_intersection_nsp::_ellipse_helper (el, {0.5, 0}, {1.6, 3});
  test_intersection_nsp::_ellipse_helper (el, {0.5, 0}, {1.6, -3});
  test_intersection_nsp::_ellipse_helper (el, {0.5, 0.2}, {-3, 1});

  return 0;
}
