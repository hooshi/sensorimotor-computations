#include <fstream>
#include <sstream>
#include <string>

#include <contrib/visit_writer.h>

#include <eulsim/geometry.hxx>
#include <eulsim/io.hxx>

namespace eulsim
{



void
IO_write_vtk_grid(const std::string filename, Grid &grid)
{
  IO_write_vtk_grid(filename,
                    grid,                           //
                    std::vector<DOFVector_1RV *>(), //
                    std::vector<DOFVector_2RV *>(), //
                    std::vector<std::string>());
}

void
IO_write_vtk_grid(const std::string filename,
                  Grid &g,                                   //
                  const std::vector<DOFVector_1RV *> &data1, //
                  const std::vector<DOFVector_2RV *> &data2, //
                  const std::vector<std::string> &data_names)

{
  const int MAX_VARS = 100;

  int dims[3] = { g.n_verts(0), g.n_verts(1), 1 };
  std::vector<float> x(dims[0]);
  std::vector<float> y(dims[1]);
  std::vector<float> z = { 0 };
  int n_vars = data2.size() + data1.size() + 1; // for vertex id
  int var_dim[MAX_VARS];
  int centering[MAX_VARS];
  float *data_float_ptr[MAX_VARS];
  std::vector<std::vector<float>> data_float(n_vars);
  const char *varnames[MAX_VARS];
  //
  for (unsigned int j = 0; j < data1.size(); ++j)
    {
      unsigned int i = j;

      varnames[i] = data_names[i].c_str();
      centering[i] = 1;
      var_dim[i] = 1;

      data_float[i].resize(dims[0] * dims[1]);
      for (int ig = 0; ig < g.n_verts(0); ++ig)
        {
          for (int jg= 0; jg < g.n_verts(1); ++jg)
            {
              data_float[i][ig + dims[0] * jg] = data1[j]->value_at({ ig, jg }, 0);
            }
        }
      data_float_ptr[i] = data_float[i].data();
    }
  //
  for (unsigned int j = 0; j < data2.size(); ++j)
    {
      unsigned int i = data1.size() + j;

      varnames[i] = data_names[i].c_str();
      centering[i] = 1;
      var_dim[i] = 3;

      data_float[i].resize(dims[0] * dims[1] * 3);
      for (int ig = 0; ig < g.n_verts(0); ++ig)
        {
          for (int jg = 0; jg < g.n_verts(1); ++jg)
            {
              data_float[i][0 + 3 * ig + 3 * dims[0] * jg] = data2[j]->value_at({ ig, jg }, 0);
              data_float[i][1 + 3 * ig + 3 * dims[0] * jg] = data2[j]->value_at({ ig, jg }, 1);
              data_float[i][2 + 3 * ig + 3 * dims[0] * jg] = 0;
            }
        }
      data_float_ptr[i] = data_float[i].data();
    }
  //
  {
    unsigned int i = data1.size() + data2.size();

    varnames[i] = "vid";
    centering[i] = 1;
    var_dim[i] = 3;

    data_float[i].resize(dims[0] * dims[1] * 3);
    for (int ig = 0; ig < g.n_verts(0); ++ig)
      {
        for (int jg = 0; jg < g.n_verts(1); ++jg)
          {
            data_float[i][0 + 3 * ig + 3 * dims[0] * jg] = ig;
            data_float[i][1 + 3 * ig + 3 * dims[0] * jg] = jg;
            data_float[i][2 + 3 * ig + 3 * dims[0] * jg] = 0;
          }
      }
    data_float_ptr[i] = data_float[i].data();
  }
  //
  for (int i = 0; i < g.n_verts(0); ++i)
    {
      x[i] = g.vertex_xyz({ i, 0 })[0];
    }
  for (int j = 0; j < g.n_verts(1); ++j)
    {
      y[j] = g.vertex_xyz({ 0, j })[1];
    }

  VISIT_write_rectilinear_mesh(filename.c_str(), 0, dims, x.data(), y.data(), z.data(), n_vars, var_dim, centering,
                               varnames, data_float_ptr);
}


void
IO_write_vtk_isurface(std::string fname, IsoSurface &iso)
{
   EULSIM_ASSERT_EQUAL_MSG(iso.n_chains, 1, ""); // May explore nc > 1 later
  const int n_pts = iso.chain_size[0];
  std::vector<float> pts(n_pts * 3);
  std::vector<int> connectivity(n_pts * 2);
  std::vector<int> cell_types(n_pts, VISIT_LINE);

  for (int i = 0; i < n_pts; ++i)
    {
      pts[i * 3 + 0] = iso.points[i * 2];
      pts[i * 3 + 1] = iso.points[i * 2 + 1];
      pts[i * 3 + 2] = 0;
      connectivity[i * 2 + 0] = i;
      connectivity[i * 2 + 1] = (i + 1) % n_pts;
    }

  VISIT_write_unstructured_mesh(fname.c_str(),                                                       //
                                0, n_pts, pts.data(), n_pts, cell_types.data(), connectivity.data(), //
                                0, NULL, NULL, NULL, NULL);
}

void
IO_write_vtk_triangulation(std::string fname, Triangulation &tri)
{
  const int n_pts = tri.n_points;
  const int n_tri = tri.n_triangles;
  std::vector<float> pts(n_pts * 3);
  std::vector<int> connectivity(n_tri * 3);
  std::vector<int> cell_types(n_tri, VISIT_TRIANGLE);

  for (int i = 0; i < n_pts; ++i)
    {
      pts[i * 3 + 0] = tri.points[i * 2];
      pts[i * 3 + 1] = tri.points[i * 2 + 1];
      pts[i * 3 + 2] = 0;
    }
  for (int i = 0; i < n_tri; ++i)
    {
      connectivity[i * 3 + 0] = tri.conn[i * 3 + 0];
      connectivity[i * 3 + 1] = tri.conn[i * 3 + 1];
      connectivity[i * 3 + 2] = tri.conn[i * 3 + 2];
    }


  VISIT_write_unstructured_mesh(fname.c_str(),                                                       //
                                0, n_pts, pts.data(), n_tri, cell_types.data(), connectivity.data(), //
                                0, NULL, NULL, NULL, NULL);
}

void
IO_butcher_filename(const std::string fname, std::string &f1, int &no, std::string &f2)
{
  // Find a dot
  std::size_t dot_id = fname.find(".");

  // No dot found
  if (dot_id == std::string::npos)
    {
      f1 = fname;
      no = -1;
      f2 = "";
      return;
    }

  // Dot found read number after dot.
  int digit = -1;
  {
    // Try to read the digit
    std::stringstream ss(fname.substr(dot_id + 1));
    ss >> digit;
    // make sure do not fail
    if (ss.fail())
      digit = -1;
  }

  // No digit found
  if (digit < 0)
    {
      // print 'bad %s is not a number' % candid`
      f1 = fname.substr(0, dot_id);
      f2 = fname.substr(dot_id + 1);
      no = -1;
      return;
    }

  // Everything does exist and is fine.
  {
    no = digit;
    f1 = fname.substr(0, dot_id);
    std::string noandf2 = fname.substr(dot_id + 1);
    std::size_t dot_id2 = noandf2.find(".");

    // std::cout << "***: " << noandf2 << std::endl;
    // std::cout << "***: " << dot_id2 << std::endl;

    // clang-format off
    if( dot_id2 == std::string::npos )  f2 = "";
    else                           f2 = noandf2.substr(dot_id2+1);
    // clang-format on
  }
}

int
IO_butcher_filename(const std::string fname)
{
  std::string f1, f2;
  int no;
  IO_butcher_filename(fname, f1, no, f2);
  return no;
}

std::string
IO_glue_filename(const std::string f1, const int no, const std::string f2)
{
  std::stringstream ss;
  ss << f1;
  // clang-format off
  if( no < 0 )  ss << ".0";
  else          ss << "." << no;
  if (f2 != "") ss << "." << f2;
  // clang-format on
  return ss.str();
}

std::string
IO_enumerate_filename(const std::string fname, const int no)
{
  std::string f1, f2;
  int currentno;
  IO_butcher_filename(fname, f1, currentno, f2);
  return IO_glue_filename(f1, no, f2);
}

static bool
does_file_exist(const std::string &fname)
{
  std::ifstream f(fname.c_str());
  return f.good();
}


int
IO_latest_number_on_disk_for_filename(const std::string fname)
{
  std::string f1, f2;
  int no;
  IO_butcher_filename(fname, f1, no, f2);

  if (no < 0)
    no = 0;
  while (true)
    {
      // std::cout << "*****: " << IO_glue_filename(f1, no, f2) << "\n";
      if (!does_file_exist(IO_glue_filename(f1, no, f2)))
        return no;
      ++no;
    }
}



} // namespace eulsim
