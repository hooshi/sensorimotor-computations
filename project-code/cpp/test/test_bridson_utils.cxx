#include <sstream>

#include <eulsim/bridson_utils.hxx>
#include <eulsim/dof_vector.hxx>
#include <eulsim/geometry.hxx>
#include <eulsim/grid.hxx>
#include <eulsim/io.hxx>


using namespace eulsim;

namespace test_bridson_utils_nsp
{

void
test_extrapolation()
{

  Grid grid(80, 80, { -3, -3 }, { 3, 3 });
  DOFVector_2RV velocity(grid);
  DOFVector_1CV is_valid(grid);
  DOFVector_1RV is_valid_real(grid);

  velocity.set(0);
  is_valid.set(0);

  for (Index2d vid = grid.first_vertex(); !grid.is_end_vertex(vid); grid.advance_vertex(vid))
    {
      Vec2r pos = grid.vertex_xyz(vid);
      if (arma::norm(pos) < 1)
        {
          is_valid.value_at(vid) = 1;
          velocity.value_at(vid, 0) = sin(arma::norm(pos));
          velocity.value_at(vid, 1) = sin(sinh(pos(0))) + cos(cosh(pos(1)));
        }
    }


  is_valid_real.make_equal_to(is_valid);
  IO_write_vtk_grid("before-extrapolation.vtk", grid, { &is_valid_real }, { &velocity }, { "isvalid", "velocity" });
  Geometry_extrapolate_closest_point(velocity, is_valid);
  is_valid_real.make_equal_to(is_valid);
  IO_write_vtk_grid("after-extrapolation.vtk", grid, { &is_valid_real }, { &velocity }, { "isvalid", "velocity" });
}

class ParabolicIntersection : public ImplicitFunction
{
public:
  // f1 = y- x^2 + 1
  // f2 = y+ x^2 - 1
  double
  eval(Vec2r pos) override
  {
    const double x = pos(0);
    const double y = pos(1);
    const double f1 = y - bridson::sqr(x) + 1;
    const double f2 = -(y + bridson::sqr(x) - 1);
    if ((f1 > 0) && (f2 > 0))
      return 1;
    else
      return 0;
  }
};

class IsInWrapper : public ParabolicIntersection
{
  double
  eval(Vec2r pos) override
  {
    if (EULSIM_ABS(ParabolicIntersection::eval(pos)) < 1e-6)
      return 1;
    else
      return -1;
  }
};

void
test_subsampling()
{
  const int REFINEMENT_LEVEL = 8;
  const int lw = 10;
  const double exact = 2 * 4. / 3.;

  ParabolicIntersection ps;
  IsInWrapper psin;

  double old_err, err;
  for (int i = 4; i < REFINEMENT_LEVEL; ++i)
    {
      const int size = (int)pow(2, i);
      Grid grid(size, size, { -1.5, -1.5 }, { 1.5, 1.5 });
      const double ans = Geometry_integrate_via_voxels(grid, ps);
      err = EULSIM_ABS(ans - exact);

      if (i > 0)
        {
          printf("level=%*d integral=%*.6e diff=%*.6e ratio=%*.6e \n", lw, i, lw, ans, lw, err, lw, old_err / err);
        }

      DOFVector_1RV val(grid);
      for (Index2d vid = grid.first_vertex(); !grid.is_end_vertex(vid); grid.advance_vertex(vid))
        {
          Vec2r pos = grid.vertex_xyz(vid);
          val.value_at(vid) = ps.eval(pos);
        }
      old_err = err;

      // std::stringstream ss;
      // ss << "ps-pslevel-" << i << ".vtk";
      // IO_write_vtk_grid(ss.str(), grid, { &val }, {}, { "value" });
    }

  printf("\n");
  // Now use local stuff
  for (int i = 0; i < REFINEMENT_LEVEL - 4; ++i)
    {
      const int size = (int)pow(2, 4);
      Grid grid(size, size, { -1.5, -1.5 }, { 1.5, 1.5 });

      double ans = 0;
      int n_refine = 0;
      int n_single = 0;
      for (Index2d vid = grid.first_vertex(); !grid.is_end_vertex(vid); grid.advance_vertex(vid))
        {
          std::vector<Index2d> neighs = grid.vertex_neighbour_cells(vid);

          char sub = 0;
          char in = EULSIM_ABS(ps.eval(grid.cell_center_xyz(neighs[0]))) > 1e-6;
          char innew = 0;
          for (unsigned int i = 1; i < neighs.size(); ++i)
            {
              innew = EULSIM_ABS(ps.eval(grid.cell_center_xyz(neighs[i]))) > 1e-6;
              if (innew != in)
                {
                  sub = 1;
                  break;
                }
            }

          if (!sub)
            {
              ans += ps.eval(grid.vertex_xyz(vid)) * grid.delta(0) * grid.delta(1);
              ++n_single;
              // printf("hello \n");
            }
          else
            {
              // printf("sub \n");
              Grid subgrid = grid.subsampling_grid_vertex(vid, i);
              ans += Geometry_integrate_via_voxels(subgrid, ps);
              ++n_refine;
            }
        }
      err = EULSIM_ABS(ans - exact);

      if (i > 0)
        {
          printf("level=%*d integral=%*.6e diff=%*.6e ratio=%*.6e \n", lw, i, lw, ans, lw, err, lw, old_err / err);
        }
      old_err = err;
      printf("Refined: %d, single: %d \n", n_refine, n_single);
    }

  // Now test my function
  for (int i = 0; i < REFINEMENT_LEVEL - 4; ++i)
    {
      const int size = (int)pow(2, 4);
      Grid grid(size, size, { -1.5, -1.5 }, { 1.5, 1.5 });

      double ans = 0;
      for (Index2d vid = grid.first_vertex(); !grid.is_end_vertex(vid); grid.advance_vertex(vid))
        {
          ans += Geometry_subsample_via_voxels(grid, vid, ps, psin, i);
        }
      err = EULSIM_ABS(ans - exact);

      if (i > 0)
        {
          printf("level=%*d integral=%*.6e diff=%*.6e ratio=%*.6e \n", lw, i, lw, ans, lw, err, lw, old_err / err);
        }
      old_err = err;
    }
} // All done

void
test_lerp()
{
  // TESP PERIODIC GET BARYCENTERIC
  {
    int i;
    double fx;
    bridson::get_periodic_barycentric(21.2, 20, i, fx);
    printf("i: %d, fx: %f \n", i, fx);
    //
    bridson::get_periodic_barycentric(20.2, 20, i, fx);
    printf("i: %d, fx: %f \n", i, fx);
    //
    bridson::get_periodic_barycentric(19.2, 20, i, fx);
    printf("i: %d, fx: %f \n", i, fx);
    //
    bridson::get_periodic_barycentric(-0.2, 20, i, fx);
    printf("i: %d, fx: %f \n", i, fx);
    //
    bridson::get_periodic_barycentric(-1.2, 20, i, fx);
    printf("i: %d, fx: %f \n", i, fx);
    /////////
    bridson::get_periodic_barycentric(3.4, 20, i, fx);
    printf("i: %d, fx: %f \n", i, fx);
    //
    bridson::get_periodic_barycentric(19., 20, i, fx);
    printf("i: %d, fx: %f \n", i, fx);
  }

  // TEST INTERPOLATION
  {
    Grid g(4, 4, { -1, -1 }, { 1, 1 });
    DOFVector_1RV dof(g);
    for (int i = 0; i < 5; ++i)
      for (int j = 0; j < 5; ++j)
        {
          dof.value_at({ i, j }) = i + j;
        }

    dof.print_on_grid(stdout);

    double val;
    val = arma::as_scalar(bridson::interpolate_value({ 0.75, -1. }, dof));
    printf("value: %f \n", val);
    //
    val = arma::as_scalar(bridson::interpolate_value({ 0.89, -1. }, dof));
    printf("value: %f \n", val);
    //
    val = arma::as_scalar(bridson::interpolate_value({ 1.3, -1. }, dof));
    printf("value: %f \n", val);
  }
}

} // End of test_bridson_utils_nsp

int
test_bridson_utils()
{
  test_bridson_utils_nsp::test_extrapolation();
  test_bridson_utils_nsp::test_subsampling();
  test_bridson_utils_nsp::test_lerp();
  return 0;
}
