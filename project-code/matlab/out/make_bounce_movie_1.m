clear

fig = figure('pos',[10 10 900 300])

MOVIE = 0;

if(MOVIE)
    k=1;
    filename = 'bounce.gif'
    for i=0:185

        data1 = load(sprintf("bounce_upwind/%d.dat", i), '-mat');
        t = data1.data{1};
        dx1 = data1.data{2}; dx1 = dx1(2)-dx1(1);
        

        hold off, l1=plot( data1.data{2}, data1.data{3}, 'b', 'linewidth', 2);
        hold on, l2=plot( data1.data{2}, data1.data{4}, 'r', 'linewidth', 2);
        hold on, l3=plot( data1.data{2}, data1.data{5}/dx1, 'k', 'linewidth', 2);
        legend([l1,l2,l3],{'v','xi','rho'}, 'Locaion', 'southeast')
        ylim([-4 4])
        title(sprintf("t=%.4f", t))
        
        f = getframe(fig);
        [im, cm] = rgb2ind(frame2im(f),256);
        if k == 1
            imwrite(im,cm,filename,'gif', 'DelayTime', 0,'Loopcount',inf);
        else
            imwrite(im,cm,filename,'gif','WriteMode','append', 'DelayTime', 0);
        end
        k=k+1;
    end
else
    for i=[0, 5, 10, 15, 20, 25, 30, 35, 40]

        data1 = load(sprintf('bounce_upwind/%d.dat', i), '-mat');
        t = data1.data{1};
        dx1 = data1.data{2}; dx1 = dx1(2)-dx1(1);
        

        hold off, l1=plot( data1.data{2}, data1.data{3}, 'b', 'linewidth', 2);
        hold on, l2=plot( data1.data{2}, data1.data{4}, 'r', 'linewidth', 2);
        hold on, l3=plot( data1.data{2}, data1.data{5}/dx1, 'k', 'linewidth', 2);
        legend([l1,l2,l3],{'v','xi','rho'}, 'Location', 'southeast')
        ylim([-4 4])
        title(sprintf('t=%.4f', t))
        saveas(gcf, sprintf('bounce-%d.svg', i))
    end    
end
