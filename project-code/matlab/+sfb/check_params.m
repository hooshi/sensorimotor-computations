function check_params(params)
% function check_params(params)
 to_check = { 'solid', 'dx', 'dt', 'advm', 'g' , 'x', 'n_sample', 'barriers'};
 
 for i=1:length(to_check)
     assert( isfield(params, to_check{i}) , sprintf(" ``%s\'\' not in params", to_check{i}) );
     
 end
 
end

