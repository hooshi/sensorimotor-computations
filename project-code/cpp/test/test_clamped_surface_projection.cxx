#include <cmath>

#include <contrib/visit_writer.h>

#include <eulsim/clamped_solid_simulator.hxx>
#include <eulsim/solid.hxx>

using namespace eulsim;

namespace test_cmp_projection_nsp
{

// A circle with radius 1 centered at (0,0)
class MyMaterial : public Material
{
  Real
  rho_0()
  {
    return 1;
  }
  Mat2r
  stress(Mat2r &def_gradient)
  {
    return Mat2r(def_gradient);
  }
};

class Circle : public ClampedSolid
{
  Real radiusx;
  Real radiusy;

public:
  Circle(Real rx = 0.8, Real ry = 0.8) : radiusx(rx), radiusy(ry) {}

  void
  project_point(const Vec2r &pt, SurfacePoint *spt)
  {
    spt->surface_id = 0;
    spt->t = atan2(pt(1) / radiusy, pt(0) / radiusx);
  }

  Vec2r
  surface_point_xy(const SurfacePoint &spt)
  {
    Vec2r ans;
    ans(0) = radiusx * cos(spt.t);
    ans(1) = radiusy * sin(spt.t);
    return ans;
  }

  void
  boundary_solution(const Real /* time */, const SurfacePoint &spt, Vec2r *velocity, Vec2r *refmap)
  {
    const double x = radiusx * cos(spt.t);
    const double y = radiusy * sin(spt.t);
    initial_solution({ x, y }, velocity, refmap);
    // if(velocity) (*velocity) = {0,0};
    // if(refmap) (*refmap) = {0,0};
  }

  virtual bool
  is_in_initially(const Vec2r &pt)
  {
    if (arma::norm(pt / Vec2r({ radiusx, radiusy })) < 1)
      return true;
    else
      return false;
  }

  void
  initial_solution(const Vec2r &pt, Vec2r *velocity, Vec2r *refmap)
  {
    const Real x = pt(0);
    const Real y = pt(1);
    if (velocity)
      {
        // A linear function
        (*velocity)(0) = 8 * x - 12 * y + 2;
        // A quadratic function
        (*velocity)(1) = x * y + 3 * x * x - 2 * x * y + 8 * x - 12 * y + 2;
      }
    if (refmap)
      {
        // Combination of sin and cos
        (*refmap)(0) = sin(x * x) - cos(sinh(y));
        (*refmap)(1) = cos(x * x) - sin(sinh(y));
      }
  }

  bool
  is_in_refmap(const Vec2r &refmap)
  {
    if (arma::norm(refmap / Vec2r({ radiusx, radiusy })) < 1)
      return true;
    else
      return false;
  }

  Material &
  material()
  {
    static MyMaterial my_material;
    return my_material;
  }

  void
  write_vtk(const std::string filename)
  {
    const int n_pts = 500;
    std::vector<float> pts(n_pts * 3);
    std::vector<int> connectivity(n_pts * 2);
    std::vector<int> cell_types(n_pts, VISIT_LINE);

    for (int i = 0; i < n_pts; ++i)
      {
        const float theta = float(i) / n_pts * 2 * EULSIM_PI;
        const float x = float(radiusx) * cosf(theta);
        const float y = float(radiusy) * sinf(theta);
        pts[i * 3 + 0] = x;
        pts[i * 3 + 1] = y;
        pts[i * 3 + 2] = 0;
        connectivity[i * 2 + 0] = i;
        connectivity[i * 2 + 1] = (i + 1) % n_pts;
      }

    VISIT_write_unstructured_mesh(filename.c_str(), 0, n_pts, pts.data(), n_pts, cell_types.data(), connectivity.data(),
                                  0, NULL, NULL, NULL, NULL);
  }
};

void
write_grid(Grid &g, ClampedSolidSimulator &css, std::string filename, DOFVector_2RV *vel = NULL,
           DOFVector_2RV *ksi = NULL)
{

  int dims[3] = { g.n_verts(0), g.n_verts(1), 1 };
  std::vector<float> x(dims[0]);
  std::vector<float> y(dims[1]);
  std::vector<float> z = { 0 };
  int n_vars = 2;
  int vardim[] = { 1, 3, 3, 3 };
  int centering[] = { 1, 1, 1, 1 };
  const char *const varnames[] = { "activation", "vid", "v", "ksi" };
  std::vector<float> status(dims[0] * dims[1]);
  std::vector<float> vid(dims[0] * dims[1] * 3);
  std::vector<float> velf(dims[0] * dims[1] * 3);
  std::vector<float> ksif(dims[0] * dims[1] * 3);
  float *vars[] = { status.data(), vid.data(), velf.data(), ksif.data() };
  if (vel)
    ++n_vars;
  if (ksi)
    ++n_vars;


  for (int i = 0; i < g.n_verts(0); ++i)
    {
      x[i] = g.vertex_xyz({ i, 0 })[0];
    }
  for (int j = 0; j < g.n_verts(1); ++j)
    {
      y[j] = g.vertex_xyz({ 0, j })[1];
    }
  for (int i = 0; i < g.n_verts(0); ++i)
    {
      for (int j = 0; j < g.n_verts(1); ++j)
        {
          status[i + dims[0] * j] = css.vertex_status({ i, j }).vtype;
          vid[0 + 3 * i + 3 * dims[0] * j] = i;
          vid[1 + 3 * i + 3 * dims[0] * j] = j;
          vid[2 + 3 * i + 3 * dims[0] * j] = 0;
          if (vel)
            {
              velf[0 + 3 * i + 3 * dims[0] * j] = vel->value_at({ i, j }, 0);
              velf[1 + 3 * i + 3 * dims[0] * j] = vel->value_at({ i, j }, 1);
              velf[2 + 3 * i + 3 * dims[0] * j] = 0;
            }
          if (ksi)
            {
              ksif[0 + 3 * i + 3 * dims[0] * j] = ksi->value_at({ i, j }, 0);
              ksif[1 + 3 * i + 3 * dims[0] * j] = ksi->value_at({ i, j }, 1);
              ksif[2 + 3 * i + 3 * dims[0] * j] = 0;
            }
        }
    }

  VISIT_write_rectilinear_mesh(filename.c_str(), 0, dims, x.data(), y.data(), z.data(), n_vars, vardim, centering,
                               varnames, vars);
}

void
write_ghost_rays(Grid &g, ClampedSolidSimulator &css, std::string filename)
{

  // Find ghost points
  std::vector<ClampedSolidSimulator::VertexGhostStatus *> ghosts;
  std::vector<Vec2r> ghosts_xy;
  for (int i = 0; i < g.n_verts(0); ++i)
    {
      for (int j = 0; j < g.n_verts(1); ++j)
        {
          if (css.vertex_status({ i, j }).vtype == ClampedSolidSimulator::VERTEX_GHOST)
            {
              ghosts.push_back(&css.vertex_status({ i, j }));
              ghosts_xy.push_back(g.vertex_xyz({ i, j }));
            }
        }
    }


  std::vector<float> pts;
  std::vector<int> connectivity;
  std::vector<int> cell_types;

  for (int i = 0; i < (int)ghosts.size(); ++i)
    {
      ClampedSolidSimulator::VertexGhostStatus *vgs = ghosts[i];

      // If the other weights are zero, it means that the point is on the bdry.
      if ((vgs->w1 == 0.) && (vgs->w2 == 0.))
        {
          EULSIM_FORCE_ASSERT(EULSIM_ABS(vgs->w0 - 1.) < 1e-10);
          continue;
        }

      // Make sure that weights make sense.
      const double tr = 1 - 1. / (1 - vgs->w0);
      const double te = (1 - tr) * vgs->w2;
      const double onemte = (1 - tr) * vgs->w1;
      EULSIM_FORCE_ASSERT(EULSIM_ABS(te + onemte - 1.) < 1e-10);

      Vec2r vg, vb, vstar, v0, v1;
      vg = ghosts_xy[i];
      v0 = g.vertex_xyz(vgs->p1);
      v1 = g.vertex_xyz(vgs->p2);
      vb = css.solid().surface_point_xy(vgs->p0);
      vstar = v0 * onemte + v1 * te;

      //   connectivity.push_back(pts.size() / 3);
      //   connectivity.push_back(pts.size() / 3 + 1);
      //   connectivity.push_back(pts.size() / 3 + 2);
      //   cell_types.push_back(VISIT_TRIANGLE);
      //   pts.push_back(vg(0));
      //   pts.push_back(vg(1));
      //   pts.push_back(0);
      //   pts.push_back(v0(0));
      //   pts.push_back(v0(1));
      //   pts.push_back(0);
      //   pts.push_back(v1(0));
      //   pts.push_back(v1(1));
      //   pts.push_back(0);

      connectivity.push_back(pts.size() / 3);
      connectivity.push_back(pts.size() / 3 + 1);
      connectivity.push_back(pts.size() / 3 + 2);
      cell_types.push_back(VISIT_TRIANGLE);
      pts.push_back(vg(0));
      pts.push_back(vg(1));
      pts.push_back(0);
      pts.push_back(vb(0));
      pts.push_back(vb(1));
      pts.push_back(0);
      pts.push_back(vstar(0));
      pts.push_back(vstar(1));
      pts.push_back(0);
    }


  VISIT_write_unstructured_mesh(filename.c_str(), 0, pts.size() / 3, pts.data(), cell_types.size(), cell_types.data(),
                                connectivity.data(), 0, NULL, NULL, NULL, NULL);
}

void
find_extrapolation_errors(ClampedSolid &solid, int nx, int ny, Vec2r *verr, Vec2r *ksierr, int fidx)
{
  Grid grid(nx, ny, { -1, -1 }, { 1, 1 });
  ClampedSolidSimulator::SimulationInfo info;
  info.dt = 0.001;
  ClampedSolidSimulator simulator(grid, solid, info);
  simulator.init();

  DOFVector_2RV v(grid), ksi(grid);
  v.set(0);
  ksi.set(0);

  // Set the inner guys
  for (int i = 0; i < grid.n_verts(0); ++i)
    {
      for (int j = 0; j < grid.n_verts(1); ++j)
        {
          Index2d vid({ i, j });

          if (simulator.vertex_status(vid).vtype == ClampedSolidSimulator::VERTEX_ACTIVE)
            {
              Vec2r vl, ksil;
              simulator.solid().initial_solution(grid.vertex_xyz(vid), &vl, &ksil);
              v.set_vec_at(vid, vl);
              ksi.set_vec_at(vid, ksil);
            }
        }
    }

  // Extrapolate
  simulator.extrapolate_values(0, v, ClampedSolidSimulator::VAR_VELOCITY);
  simulator.extrapolate_values(0, ksi, ClampedSolidSimulator::VAR_REFMAP);

  // Evaluate the error
  int n_ghosts = 0;
  *verr = { 0, 0 };
  *ksierr = { 0, 0 };

  DOFVector_2RV dofverr(grid), dofksierr(grid);
  dofverr.set(0); dofksierr.set(0);

  for (int i = 0; i < grid.n_verts(0); ++i)
    {
      for (int j = 0; j < grid.n_verts(1); ++j)
        {
          Index2d vid({ i, j });

          if (simulator.vertex_status(vid).vtype == ClampedSolidSimulator::VERTEX_GHOST)
            {
              Vec2r vl, ksil;
              simulator.solid().initial_solution(grid.vertex_xyz(vid), &vl, &ksil);

              ++n_ghosts;
              //*verr += (v.vec_at(vid) - vl) % (v.vec_at(vid) - vl);
              //*ksierr += (ksi.vec_at(vid) - ksil) % (ksi.vec_at(vid) - ksil);
              dofverr.set_vec_at(vid, arma::abs(v.vec_at(vid) - vl));
              dofksierr.set_vec_at(vid, arma::abs(ksi.vec_at(vid) - ksil) );
              (*verr) = arma::max((*verr), arma::abs(v.vec_at(vid) - vl));
              (*ksierr) = arma::max((*ksierr), arma::abs(ksi.vec_at(vid) - ksil));
            }
        }
    }
  //*ksierr /= n_ghosts;
  //*ksierr = arma::sqrt(*ksierr);
  //*verr /= n_ghosts;
  //*verr = arma::sqrt(*verr);


  // char fname[1024];
  // sprintf(fname, "extrap_%d.vtk", fidx);
  // write_grid(grid, simulator, fname, &dofverr, &dofksierr);
}

} // End of test_cmp_projection_nsp

int
test_clamped_surface_projection()
{
  test_cmp_projection_nsp::Circle solid(0.8, 0.8);

  Grid grid(17, 17, { -1, -1 }, { 1, 1 });
  ClampedSolidSimulator::SimulationInfo info;
  info.dt = 0.001;
  ClampedSolidSimulator simulator(grid, solid, info);

  // Write the grid
  simulator.init_vertex_status();
  solid.write_vtk("mesh_1.vtk");
  test_cmp_projection_nsp::write_grid(grid, simulator, "mesh_2.vtk");

  // Write the edges near some vertices (15x15 grid)
  // 10, 8 depth 1
  // 9        8        0
  // 10         7         1
  // 10         8         0
  // 10         8         1
  // 10, 8 depth 2
  // 8        8        0
  // 9        7        0
  // 9        7        1
  // 9        8        0
  // 10         9         1
  // 9        8        1
  // 9        9        0
  // 10         6         1
  // 10         7         0
  // 10         7         1
  // 11         7         1
  // 10         8         0
  // 11         8         0
  // 10         8         1
  // 11         8         1
  // 10         9         0
  // clang-format off
  // std::cout << "(10, 8) depth 1\n"; for (auto e : grid.vertex_nearby_edges({10,8},1)){ std::cout << e.t();} std::cout << "\n";
  // std::cout << "(10, 8) depth 2\n"; for (auto e : grid.vertex_nearby_edges({10,8},2)){ std::cout << e.t();} std::cout << "\n";
  // std::cout << "(10, 8) depth 3\n"; for (auto e : grid.vertex_nearby_edges({10,8},3)){ std::cout << e.t();} std::cout << "\n";
  // clang-format on


  // Write dear gohsts (no typo)
  //   for (int i = 0; i < grid.n_verts(0); ++i)
  //     {
  //       for (int j = 0; j < grid.n_verts(1); ++j)
  //         {
  //           if (simulator.vertex_status({ i, j }).vtype == ClampedSolidSimulator::VERTEX_GHOST)
  //             simulator.vertex_status({ i, j }).vtype = ClampedSolidSimulator::VERTEX_INACTIVE;
  //         }
  //     }
  // simulator.vertex_status({ 1, 2 }).vtype = ClampedSolidSimulator::VERTEX_GHOST;
  // simulator.vertex_status({ 1, 3 }).vtype = ClampedSolidSimulator::VERTEX_GHOST;
  // simulator.vertex_status({ 2, 2 }).vtype = ClampedSolidSimulator::VERTEX_GHOST;s
  simulator.init();
  test_cmp_projection_nsp::write_ghost_rays(grid, simulator, "mesh_3.vtk");

  //
  // Extrapolate the velocity and refmap, make sure they are accurate
  //
  constexpr int n_refinement = 5;
  arma::mat::fixed<n_refinement, 2> ksierr, verr;
  const int width = 7;
  int nx = 11;
  int ny = 9;
  for (int i = 0; i < n_refinement; ++i)
    {
      Vec2r ksierrl, verrl;
      find_extrapolation_errors(solid, nx, ny, &verrl, &ksierrl, i);
      verr.row(i) = verrl.t();
      ksierr.row(i) = ksierrl.t();
      printf("    %3dx%3d %*.4e %*.4e %*.4e %*.4e \n", nx, ny, width, verrl(0), width, verrl(1), width, ksierrl(0),
             width, ksierrl(1));
      nx *= 2;
      ny *= 2;
    }
  for (int i = 1; i < n_refinement; ++i)
    {
      printf("    %*.2e %*.2e %*.2e %*.2e \n", width, verr(i - 1, 0) / verr(i, 0), width, verr(i - 1, 1) / verr(i, 1),
             width, ksierr(i - 1, 0) / ksierr(i, 0), width, ksierr(i - 1, 1) / ksierr(i, 1));
    }

  return 0;
}
