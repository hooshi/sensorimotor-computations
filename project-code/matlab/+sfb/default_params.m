function [params] = default_params(params)
% function [params] = default_params(params)
if( nargin == 0) 
    params = struct;
end
params = utility.check_default(params, 'solid', sfb.Solid());
% params = utility.check_default(params, 'x', []);
params = utility.check_default(params, 'dx', 0.01);
params = utility.check_default(params, 'dt', 0.01);
params = utility.check_default(params, 'advm', 'upwind');
params = utility.check_default(params, 'n_sample', 8);
params = utility.check_default(params, 'g', 0);
params = utility.check_default(params, 'barriers', {});
end

