#include "eulsim/grid.hxx"
#include "eulsim/defs.hxx"
#include "eulsim/predicates.hxx"

#include <queue>

namespace eulsim
{

// ================================================================
//                        GRID
// ================================================================

Grid::Grid(int n_cells_x, int n_cells_y, Vec2r lower_left, Vec2r top_right)
  : _x_from(lower_left(0))
  , _x_to(top_right(0))
  , _y_from(lower_left(1))
  , _y_to(top_right(1))
  , _delta_x((_x_to - _x_from) / n_cells_x)
  , _delta_y((_y_to - _y_from) / n_cells_y)
  , _n_cells_x(n_cells_x)
  , _n_cells_y(n_cells_y)
{
}

Grid::~Grid() {}

Vec2r
Grid::cell_center_xyz(const Index2d i2d) const
{
  const int i = i2d(0);
  const int j = i2d(1);

  EULSIM_ASSERT_LESSER_MSG(i, n_cells(0), "");
  EULSIM_ASSERT_LESSER_MSG(j, n_cells(1), "");

  Vec2r xy;
  xy(0) = _x_from + (i + 0.5) * delta(0);
  xy(1) = _y_from + (j + 0.5) * delta(1);
  return xy;
}

Vec2r
Grid::vertex_xyz(const Index2d i2d) const
{
  const int i = i2d(0);
  const int j = i2d(1);

  EULSIM_ASSERT_LESSER_MSG(i, n_verts(0), "");
  EULSIM_ASSERT_LESSER_MSG(j, n_verts(1), "");

  Vec2r xy;
  xy(0) = _x_from + i * delta(0);
  xy(1) = _y_from + j * delta(1);
  return xy;
}

Vec2r
Grid::edge_center_xyz(Index2d i2d, int alignment) const
{
  const int i = i2d(0);
  const int j = i2d(1);

  EULSIM_ASSERT_LESSER_MSG(i, n_edges(0, alignment), "");
  EULSIM_ASSERT_LESSER_MSG(j, n_edges(1, alignment), "");

  Vec2r xy;
  switch (alignment)
    {
      case 0:
        {
          xy(0) = _x_from + i * delta(0) + _delta_x / 2.;
          xy(1) = _y_from + j * delta(1);
          break;
        }
      case 1:
        {
          xy(0) = _x_from + i * delta(0);
          xy(1) = _y_from + j * delta(1) + _delta_y / 2.;
          break;
        }
      default: EULSIM_FORCE_ASSERT(0);
    }
  return xy;
}

Real
Grid::delta(const int dim) const
{
  switch (dim)
    {
      case 0: return _delta_x; break;
      case 1: return _delta_y; break;
      default: EULSIM_FORCE_ASSERT(0);
    }
}

Real
Grid::length_max(const int dim) const
{
  switch (dim)
    {
      case 0: return _x_to - _x_from; break;
      case 1: return _y_to - _y_from; break;
      default: EULSIM_FORCE_ASSERT(0);
    }
}

int
Grid::n_cells(const int dim) const
{
  switch (dim)
    {
      case 0: return _n_cells_x; break;
      case 1: return _n_cells_y; break;
      default: EULSIM_FORCE_ASSERT(0);
    }
}

int
Grid::n_edges(const int dim, const int alignment) const
{
  switch (dim)
    {
      case 0:
        {
          switch (alignment)
            {
              case 0: return _n_cells_x;
              case 1: return _n_cells_x + 1;
              default: EULSIM_FORCE_ASSERT(0);
            }
          break;
        }
      case 1:
        {
          switch (alignment)
            {
              case 0: return _n_cells_y + 1;
              case 1: return _n_cells_y;
              default: EULSIM_FORCE_ASSERT(0);
            }
          break;
        }
      default: EULSIM_FORCE_ASSERT(0);
    }
}

int
Grid::n_verts(const int dim) const
{
  switch (dim)
    {
      case 0: return _n_cells_x + 1; break;
      case 1: return _n_cells_y + 1; break;
      default: EULSIM_FORCE_ASSERT(0);
    }
}


std::vector<Index3d>
Grid::vertex_neighbour_edges(const Index2d vid) const
{
  const int i = vid(0);
  const int j = vid(1);

  EULSIM_ASSERT_LESSER_MSG(i, n_verts(0), "");
  EULSIM_ASSERT_LESSER_MSG(j, n_verts(1), "");

  std::vector<Index3d> neighs;
  neighs.reserve(4);

  // clang-format off
  if (i != 0)              neighs.push_back({ i-1, j  , 0});
  if (j != 0)              neighs.push_back({ i  , j-1, 1});
  if (i != n_verts(0) - 1) neighs.push_back({ i  , j  , 0});
  if (j != n_verts(1) - 1) neighs.push_back({ i  , j  , 1});
  // clang-format on

  return neighs;
}

std::vector<Index2d>
Grid::vertex_neighbour_vertices(const Index2d vid) const
{
  const int i = vid(0);
  const int j = vid(1);

  EULSIM_ASSERT_LESSER_MSG(i, n_verts(0), "");
  EULSIM_ASSERT_LESSER_MSG(j, n_verts(1), "");

  std::vector<Index2d> neighs;
  neighs.reserve(4);

  // clang-format off
  if (i != 0)              neighs.push_back({ i - 1, j      });
  if (j != 0)              neighs.push_back({ i    , j - 1  });
  if (i != n_verts(0) - 1) neighs.push_back({ i + 1, j      });
  if (j != n_verts(1) - 1) neighs.push_back({ i    , j + 1  });
  // clang-format on

  return neighs;
}

std::vector<Index2d>
Grid::vertex_star_vertices(const Index2d vid) const
{
  const int i = vid(0);
  const int j = vid(1);

  EULSIM_ASSERT_LESSER_MSG(i, n_verts(0), "");
  EULSIM_ASSERT_LESSER_MSG(j, n_verts(1), "");

  std::vector<Index2d> neighs;
  neighs.reserve(8);


  const bool isnt_imin = i != 0;
  const bool isnt_imax = i != n_verts(0) - 1;
  const bool isnt_jmin = j != 0;
  const bool isnt_jmax = j != n_verts(1) - 1;

  // clang-format off
  if(isnt_imin             ) neighs.push_back(Index2d{i - 1, j     });
  if(isnt_imin && isnt_jmin) neighs.push_back(Index2d{i - 1, j - 1 });
  if(             isnt_jmin) neighs.push_back(Index2d{i    , j - 1 });
  if(isnt_imax && isnt_jmin) neighs.push_back(Index2d{i + 1, j - 1 });
  if(isnt_imax             ) neighs.push_back(Index2d{i + 1, j     });
  if(isnt_imax && isnt_jmax) neighs.push_back(Index2d{i + 1, j + 1 });
  if(             isnt_jmax) neighs.push_back(Index2d{i    , j + 1 });
  if(isnt_imin && isnt_jmax) neighs.push_back(Index2d{i - 1, j + 1 });
  // clang-format on

  return neighs;
}

std::vector<Index2d>
Grid::vertex_neighbour_cells(const Index2d vid) const
{
  const int i = vid(0);
  const int j = vid(1);

  EULSIM_ASSERT_LESSER_MSG(i, n_verts(0), "");
  EULSIM_ASSERT_LESSER_MSG(j, n_verts(1), "");

  std::vector<Index2d> neighs;
  neighs.reserve(4);

  // clang-format off
  if ((i != 0)              && (j != n_verts(1) - 1))  neighs.push_back({ i-1,j  });
  if ((i != 0)              && (j != 0)             )  neighs.push_back({ i-1,j-1});
  if ((i != n_verts(0) - 1) && (j != 0)             )  neighs.push_back({ i  ,j-1});
  if ((i != n_verts(0) - 1) && (j != n_verts(1) - 1))  neighs.push_back({ i  ,j  });
  // clang-format on

  return neighs;
}

std::vector<Index3d>
Grid::vertex_nearby_edges(const Index2d i2d, const int max_depth) const
{
  //
  // First find all the neighbouring vertices using bfs search up to level of max_depth.
  //
  std::vector<Index3d> target_edges;
  {
    std::vector<Index2d> visited_vertices;
    std::queue<Index2d> to_visit_vertices;
    std::queue<int> to_visit_depth;

    to_visit_vertices.push(i2d);
    to_visit_depth.push(0);
    while (!to_visit_vertices.empty())
      {
        const Index2d vertex = to_visit_vertices.front();
        to_visit_vertices.pop();
        const int depth = to_visit_depth.front();
        to_visit_depth.pop();

        // If we are going beyond the depth or we had previously visited the
        // vertex continue
        if (depth >= max_depth)
          continue;
        if (VectorFind<2>()(visited_vertices, vertex) != visited_vertices.end())
          continue;

        // Add the edges of the vertex to the neighbour edges
        std::vector<Index3d> vertex_edges = this->vertex_neighbour_edges(vertex);
        std::copy(vertex_edges.begin(), vertex_edges.end(), std::back_inserter(target_edges));

        // Add the neighbours of this vertex to the search queue
        std::vector<Index2d> vertex_verts = this->vertex_neighbour_vertices(vertex);
        for (std::vector<Index2d>::iterator it = vertex_verts.begin(); it != vertex_verts.end(); ++it)
          {
            to_visit_depth.push(depth + 1);
            to_visit_vertices.push(*it);
          }
      } // End of BFS
  }     // End of finding edges

  //
  // Now prune the duplicated edges
  //
  std::sort(target_edges.begin(), target_edges.end(), IsIndexLesserOrdered<3>());
  for (int i = (int)target_edges.size() - 1; i >= 1; --i)
    {
      if (IsIndexEqual<3>(target_edges[i])(target_edges[i - 1]))
        {
          std::swap(target_edges[i], target_edges.back());
          target_edges.pop_back();
        }
    }

  return target_edges;
} // All done

void
Grid::edge_vertices(const Index3d eid, Index2d *v0, Index2d *v1)
{
  const int i = eid(0);
  const int j = eid(1);
  const int algn = eid(2);

  EULSIM_ASSERT_LESSER_MSG(i, n_verts(0), "");
  EULSIM_ASSERT_LESSER_MSG(j, n_verts(1), "");
  EULSIM_ASSERT_LE_MSG(algn, 1, "");

  // clang-format off
  if(algn == 0)
    {
      if (v0) *v0 = {i  , j};
      if (v1) *v1 = {i+1, j};
    }
  else 
    {
      if (v0) *v0 = {i, j  };
      if (v1) *v1 = {i, j+1};
    }
  // clang-format on
}


Index2d
Grid::first_vertex()
{
  return { 0, 0 };
}

void
Grid::advance_vertex(Index2d &vid)
{
  int sum = vid(0) + vid(1) * n_verts(0);
  ++sum;
  vid = { sum % n_verts(0), sum / n_verts(0) };
}

bool
Grid::is_end_vertex(const Index2d &vid)
{
  const int sum = vid(0) + vid(1) * n_verts(0);
  return sum == n_verts(0) * n_verts(1);
}

bool
Grid::is_boundary_vertex(const Index2d &vid)
{
  return (vid(0) == 0) || (vid(0) == n_verts(0) - 1) || (vid(1) == 0) || (vid(1) == n_verts(1) - 1);
}


Index2d
Grid::first_cell()
{
  return { 0, 0 };
}

void
Grid::advance_cell(Index2d &cid)
{
  int sum = cid(0) + cid(1) * n_cells(0);
  ++sum;
  cid = { sum % n_cells(0), sum / n_cells(0) };
}

bool
Grid::is_end_cell(const Index2d &cid)
{
  const int sum = cid(0) + cid(1) * n_cells(0);
  return sum == n_cells(0) * n_cells(1);
}

bool
Grid::is_boundary_cell(const Index2d &cid)
{
  return (cid(0) == 0) || (cid(0) == n_cells(0) - 1) || (cid(1) == 0) || (cid(1) == n_cells(1) - 1);
}

Grid
Grid::subsampling_grid_vertex(const Index2d &vid, int level)
{
  const int i = vid(0);
  const int j = vid(1);

  EULSIM_ASSERT_LESSER_MSG(i, n_verts(0), "");
  EULSIM_ASSERT_LESSER_MSG(j, n_verts(1), "");
  Vec2r pos = vertex_xyz(vid);
  Vec2r pmin = pos, pmax = pos;

  // clang-format off
  if (i != 0)              pmin(0) -= delta(0)/2.;
  if (j != 0)              pmin(1) -= delta(1)/2.;
  if (i != n_verts(0) - 1) pmax(0) += delta(0)/2.;
  if (j != n_verts(1) - 1) pmax(1) += delta(1)/2.;
  // clang-format on

  const int size = (int)pow(2, level);
  return Grid(size, size, pmin, pmax);
}


// read and write
void
Grid::write_ascii(std::string fname)
{
  std::ofstream fstr(fname, std::fstream::out);
  EULSIM_FORCE_ASSERT_MSG(fstr.is_open(), "File " << fname << " could not be found.");
  write_stream(fstr);
}

void
Grid::read_ascii(std::string fname)
{
  std::ifstream fstr(fname, std::fstream::in);
  EULSIM_FORCE_ASSERT_MSG(fstr.is_open(), "File " << fname << " could not be found.");
  read_stream(fstr);
}

void
Grid::write_stream(std::ostream &fstr)
{
  fstr << _x_from << " " << _x_to << "\n";
  fstr << _y_from << " " << _y_to << "\n";
  fstr << _n_cells_x << " " << _n_cells_y << "\n";
  fstr << _n_cells_x << " " << _n_cells_y << "\n";
}

void
Grid::read_stream(std::istream &fstr)
{
  fstr >> _x_from >> _x_to;
  fstr >> _y_from >> _y_to;
  fstr >> _n_cells_x >> _n_cells_y;
  _delta_x = (_x_to - _x_from) / _n_cells_x;
  _delta_y = (_y_to - _y_from) / _n_cells_y;
}



} // namespace eulsim