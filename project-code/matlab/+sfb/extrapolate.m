function [value_o] = extrapolate(value,is_valid)
% function [value_o, is_valid_o] = extrapolate(value,is_valid)

if( nargin == 0) run_min_example(); return; end

idx1 = find(is_valid, 1, 'first');
idx2 = find(is_valid, 1, 'last');
indices = 1:size(value,1);
indices = indices(:);

value_o = value;
value_o( (~is_valid) & (indices > idx2) ) = value(idx2);
value_o( (~is_valid) & (indices < idx1) ) = value(idx1);
end

function run_min_example()
x = linspace(0, 2*pi, 200);
x = x(:);
y = sin( 3*x );
is_valid = (x > 0.4) & (x < 3.2);
ye = sfb.extrapolate(y,is_valid);
hold off, plot(x, y);
hold on, plot(x,ye);
end