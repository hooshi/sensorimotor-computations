clear all
close all


%===================================================================
% Set the parameters
%===================================================================

params = containers.Map('KeyType', 'char', 'ValueType', 'any');
params('k') = 4;
params('r') = 0.95;
params('Et') = 0;

tspan = [0, 5];
y0 = 0;

PART1 = 1
PART2 = 1

if PART1
%===================================================================
% Try some step function inputs
%===================================================================
plotopts.Color = 'b';
plotopts.LineWidth = 2;

R_input = @(t) R_pulse_step(t, 0, 0, 1);
[E, t] = robinson_solver(tspan, R_input, y0, params);
hold on; plot_1 = plot(E,t, plotopts, 'linestyle', '-');

R_input = @(t) R_pulse_step(t, 0, 0, 2);
[E, t] = robinson_solver(tspan, R_input, y0, params);
hold on; plot_2 = plot(E,t, plotopts, 'linestyle', '--');

R_input = @(t) R_pulse_step(t, 0, 0, 3);
[E, t] = robinson_solver(tspan, R_input, y0, params);
hold on; plot_3 = plot(E,t, plotopts, 'linestyle', '-.');

xlabel('t');
ylabel('E');
legend([plot_1, plot_2, plot_3,],{'H=1', 'H=2', 'H=3'});
print('../latex/img/robinson-step.eps','-depsc');

%===================================================================
% Try some pulse function inputs
%===================================================================


% ============= Height 3
WIDTH=3;
plotopts.Color = 'g';
plotopts.LineWidth = 2;

R_input = @(t) R_pulse_step(t, WIDTH, 1, 0);
[E, t] = robinson_solver(tspan, R_input, y0, params);
hold off; plot_1 = plot(E,t, plotopts, 'linestyle', '-');

R_input = @(t) R_pulse_step(t, WIDTH, 2, 0);
[E, t] = robinson_solver(tspan, R_input, y0, params);
hold on; plot_2 = plot(E,t, plotopts, 'linestyle', '--');

R_input = @(t) R_pulse_step(t, WIDTH, 3, 0);
[E, t] = robinson_solver(tspan, R_input, y0, params);
hold on; plot_3 = plot(E,t, plotopts, 'linestyle', '-.');

% ======== Height 2
WIDTH=2;
plotopts.Color = 'r';
plotopts.LineWidth = 2;

R_input = @(t) R_pulse_step(t, WIDTH, 1, 0);
[E, t] = robinson_solver(tspan, R_input, y0, params);
hold on; plot_4 = plot(E,t, plotopts, 'linestyle', '-');

R_input = @(t) R_pulse_step(t, WIDTH, 2, 0);
[E, t] = robinson_solver(tspan, R_input, y0, params);
hold on; plot_5 =  plot(E,t, plotopts, 'linestyle', '--');

R_input = @(t) R_pulse_step(t, WIDTH, 3, 0);
[E, t] = robinson_solver(tspan, R_input, y0, params);
hold on; plot_6 = plot(E,t, plotopts, 'linestyle', '-.');

% ========= Height 1
WIDTH=1;
plotopts.Color = 'b';
plotopts.LineWidth = 2;

R_input = @(t) R_pulse_step(t, WIDTH, 1, 0);
[E, t] = robinson_solver(tspan, R_input, y0, params);
hold on; plot_7 =  plot(E,t, plotopts, 'linestyle', '-');

R_input = @(t) R_pulse_step(t, WIDTH, 2, 0);
[E, t] = robinson_solver(tspan, R_input, y0, params);
hold on; plot_8 = plot(E,t, plotopts, 'linestyle', '--');

R_input = @(t) R_pulse_step(t, WIDTH, 3, 0);
[E, t] = robinson_solver(tspan, R_input, y0, params);
hold on; plot_9 = plot(E,t, plotopts, 'linestyle', '-.');

plot_10 = plot([0],[0],plotopts,'color','k','linestyle','-');
plot_11 = plot([0],[0],plotopts,'color','k','linestyle','--');
plot_12 = plot([0],[0],plotopts,'color','k','linestyle','-.');

legend([plot_10, plot_11, plot_12, plot_7, plot_4, plot_1], ...
{'H=1','H=2','H=3','W=1','W=2','W=3'});

xlabel('t');
ylabel('E');
print('../latex/img/robinson-pulse.eps','-depsc');

%===================================================================
% Try some step pulse-step inputs
%===================================================================
HEIGHT1=2;
WIDTH=0.2;
plotopts.LineWidth = 2;

R_input = @(t) R_pulse_step(t, WIDTH, HEIGHT1, 0.5);
[E, t] = robinson_solver(tspan, R_input, y0, params);
hold off; plot_1 =  plot(E,t, plotopts, 'color', 'k');

R_input = @(t) R_pulse_step(t, WIDTH, HEIGHT1, 1);
[E, t] = robinson_solver(tspan, R_input, y0, params);
hold on; plot_2 = plot(E,t, plotopts, 'color', 'r');

R_input = @(t) R_pulse_step(t, WIDTH, HEIGHT1, 2);
[E, t] = robinson_solver(tspan, R_input, y0, params);
hold on; plot_3 = plot(E,t, plotopts, 'color', 'g');

R_input = @(t) R_pulse_step(t, WIDTH, HEIGHT1, 3);
[E, t] = robinson_solver(tspan, R_input, y0, params);
hold on; plot_4 = plot(E,t, plotopts, 'color', 'b');

R_input = @(t) R_pulse_step(t, WIDTH, HEIGHT1, 4);
[E, t] = robinson_solver(tspan, R_input, y0, params);
hold on; plot_5 = plot(E,t, plotopts, 'color', 'm');


legend([plot_1, plot_2, plot_3, plot_4, plot_5], ...
{'H2=0.5','H2=1','H2=2','H2=3','H2=4'});

xlabel('t');
ylabel('E');
print('../latex/img/robinson-pulse-step.eps','-depsc');

end %% PART1

%===================================================================
% For fixed R = 500 spike/sec plot
%  plot (i) the pulse duration T vs. eye position at time T,
% and (ii) max eye velocity vs. pulse duration.
%===================================================================

if PART2
R_input = @(t) 500;
    
end %% PART2


function val = R_pulse_step(t, width, height1, height2)
if( (t >= 0.) && (t < width) )
    val = height1;
else
    val = height2;
end
end
