#include <contrib/par_msquares.h>

#include <eulsim/geometry.hxx>
#include <eulsim/predicates.hxx>

namespace eulsim
{

bool
_Geometry_signed_distance_intersection_helper(ImplicitFunction &sgn, const Vec2r &x0, const Vec2r &x1, Real *t,
                                              const int n_call, const int max_iter)
{
  const double eps = 1e-15;

  double dist0 = sgn.eval(x0);
  double dist1 = sgn.eval(x1);

  if (EULSIM_ABS(dist0) < eps)
    {
      *t = 0;
      return true;
    }
  else if (EULSIM_ABS(dist1) < eps)
    {
      *t = 1;
      return true;
    }
  else if (dist0 * dist1 > eps)
    return false;

  const double guess = -dist0 / (dist1 - dist0);

  if ((n_call == max_iter))
    {
      if (t)
        *t = guess;
      return true;
    }
  else
    {
      Vec2r mid = x0 + guess * (x1 - x0);
      const double dist_mid = sgn.eval(mid);
      // Damn these degenerate cases
      if (dist_mid * dist0 < eps)
        {
          double tlocal;
          bool does_intersect;
          does_intersect = _Geometry_signed_distance_intersection_helper(sgn, x0, mid, &tlocal, n_call + 1, max_iter);
          if (t)
            *t = guess * tlocal;
          EULSIM_ASSERT(does_intersect);
          return true;
        }
      else
        {
          EULSIM_ASSERT(dist_mid * dist1 < eps);
          double tlocal;
          bool does_intersect;
          does_intersect = _Geometry_signed_distance_intersection_helper(sgn, mid, x1, &tlocal, n_call + 1, max_iter);
          if (t)
            *t = guess + (1 - guess) * tlocal;
          EULSIM_ASSERT(does_intersect);
          return true;
        }
    } // End of recursion
} // All done

bool
Geometry_signed_distance_intersection(ImplicitFunction &sgn, const Vec2r &x0, const Vec2r &x1, Real *t,
                                      const int max_iter)
{
  return _Geometry_signed_distance_intersection_helper(sgn, x0, x1, t, 0, max_iter);
}

bool
Geometry_line_intersection(const Vec2r &y0, const Vec2r &y1, const Vec2r &x0, const Vec2r &x1, Real *t)
{
  const double eps = 1e-15;

  const Vec2r tangent = Vec2r(y1 - y0);
  const Vec2r n = Vec2r({ -tangent(1), tangent(0) });

  const double LHS = arma::dot((x1 - x0), n);
  if (EULSIM_ABS(LHS) < eps)
    return false;

  *t = arma::dot((y0 - x0), n) / LHS;

  EULSIM_ASSERT_LE_MSG(EULSIM_ABS(arma::dot(x0 + (x1 - x0) * (*t) - y0, n)), 1e-10, "intersection failed.");
  return true;
}

////////////////////

struct marching_cubes_is_in_data
{
  ImplicitFunction *fn;
  Real isoval;
  Grid *grid;
};

static int
marching_cubes_is_in(int idx, void *ctx)
{
  // static int cnt = 0;
  //++cnt;

  marching_cubes_is_in_data *ct = (marching_cubes_is_in_data *)ctx;
  Index2d vid{ idx % ct->grid->n_verts(0), idx / ct->grid->n_verts(0) };
  Vec2r pos = ct->grid->vertex_xyz(vid);
  Real val = ct->fn->eval(pos);
  int ans = (val > ct->isoval ? false : true);
  // printf("%d %d(%d, %d)(%f, %f) %.3f %d \n", cnt, idx, vid(0), vid(1),  pos(0), pos(1), val, ans);
  return ans;
}

static float
marching_cubes_height(float x, float y, void *contextptr)
{
  return 0;
}

bool
Geometry_extract_zero_surface(ImplicitFunction &implicit_function, Grid &grid, IsoSurface *zero_surface,
                              Triangulation *tri, const int step)
{

  // Run marching squares
  // Must scale since cell size is 1 now
  par_msquares_meshlist *par_mlist;
  {
    const int cell_size = step;
    const int flags = 0;
    const int width = grid.n_verts(0);
    const int height = grid.n_verts(1);
    //
    marching_cubes_is_in_data ctx;
    ctx.fn = &implicit_function;
    ctx.isoval = 0;
    ctx.grid = &grid;
    //
    // par_mlist = par_msquares_grayscale(values, width, height, cell_size, threshold, flags);
    par_mlist = par_msquares_function(width, height, cell_size, flags, //
                                      &ctx, marching_cubes_is_in, marching_cubes_height);
    //
    int n_par_meshes = par_msquares_get_count(par_mlist);
    EULSIM_ASSERT(n_par_meshes == 1);
  }

  if (tri)
    {
      par_msquares_mesh const *par_mesh = par_msquares_get_mesh(par_mlist, 0);
      EULSIM_FORCE_ASSERT(par_mesh->dim == 2);
      tri->n_points = par_mesh->npoints;
      tri->n_triangles = par_mesh->ntriangles;
      //
      tri->points.resize(2 * tri->n_points);
      for (int i = 0; i < tri->n_points; ++i)
        {
          tri->points[2 * i] = par_mesh->points[2 * i];
          tri->points[2 * i + 1] = par_mesh->points[2 * i + 1];
        }
      //
      tri->conn.resize(tri->n_triangles * 3);
      for (int i = 0; i < tri->n_triangles; ++i)
        {
          tri->conn[3 * i] = par_mesh->triangles[3 * i];
          tri->conn[3 * i + 1] = par_mesh->triangles[3 * i + 1];
          tri->conn[3 * i + 2] = par_mesh->triangles[3 * i + 2];
        }
    }
  par_msquares_free(par_mlist);


  return true;
} // All done

template <int BS>
void
Geometry_extrapolate_closest_point(DOFVector_R<BS, STORAGE_VERTICES> &dv, DOFVector_1CV &is_valid)
{
  const int N_PROPAGATION_LAYERS = 6;
  typedef DOFVector_R<BS, STORAGE_VERTICES> DOFVector_XRV;


  EULSIM_FORCE_ASSERT(&dv.grid() == &is_valid.grid());
  DOFVector_1CV is_valid_buffer(dv.grid());
  DOFVector_XRV dv_buffer(dv.grid());
  Geometry_extrapolate_closest_point(dv, dv_buffer, is_valid, is_valid_buffer, N_PROPAGATION_LAYERS);
}
template void Geometry_extrapolate_closest_point<1>(DOFVector_1RV &, DOFVector_1CV &);
template void Geometry_extrapolate_closest_point<2>(DOFVector_2RV &, DOFVector_1CV &);
template void Geometry_extrapolate_closest_point<4>(DOFVector_4RV &, DOFVector_1CV &);


template <int BS>
void
Geometry_extrapolate_closest_point(DOFVector_R<BS, STORAGE_VERTICES> &dv,      //
                                   DOFVector_R<BS, STORAGE_VERTICES> &temp_dv, //
                                   DOFVector_1CV &is_valid,                    //
                                   DOFVector_1CV &old_is_valid, const int n_layers)
{
  typedef DOFVector_R<BS, STORAGE_VERTICES> DOFVector_XRV;

  Grid &grid = dv.grid();
  EULSIM_FORCE_ASSERT(&grid == &is_valid.grid());
  EULSIM_FORCE_ASSERT(&grid == &temp_dv.grid());
  EULSIM_FORCE_ASSERT(&grid == &old_is_valid.grid());

  for (int layers = 0; layers < n_layers; ++layers)
    {
      old_is_valid.make_equal_to(is_valid);
      temp_dv.make_equal_to(dv);

      for (Index2d vid = grid.first_vertex(); !grid.is_end_vertex(vid); grid.advance_vertex(vid))
        {
          typename DOFVector_XRV::VectorType sum(arma::fill::zeros);
          int count = 0;

          std::vector<Index2d> neighbours = grid.vertex_neighbour_vertices(vid);
          for (unsigned int nidf = 0; nidf < neighbours.size(); ++nidf)
            {
              const Index2d &nid = neighbours[nidf];
              if (old_is_valid.value_at(nid))
                {
                  ++count;
                  sum += dv.vec_at(nid);
                }
            }

          // If any of neighbour cells were valid,
          // assign the cell their average value and tag it as valid
          if (count > 0)
            {
              temp_dv.set_vec_at(vid, sum / (double)count);
              is_valid.value_at(vid) = 1;
            }

        } // End of vertices
      // update with the new changes
      dv.make_equal_to(temp_dv);
    } // End of layers
} // All done

double
Geometry_integrate_via_voxels(Grid &grid, ImplicitFunction &fn)
{
  double ans = 0;
  const double dx = grid.delta(0);
  const double dy = grid.delta(1);

  for (Index2d cid = grid.first_cell(); !grid.is_end_cell(cid); grid.advance_cell(cid))
    {
      const Vec2r pos = grid.cell_center_xyz(cid);
      const double value = fn.eval(pos);
      ans += value * dx * dy;
    }

  return ans;
}

// On the boundaries the function should be able to
// to get the values periodically.
// That is why I am dealing with out of bound values.
double
Geometry_subsample_via_voxels(Grid &grid, const Index2d &vid,               //
                              ImplicitFunction &fn, ImplicitFunction &isin, //
                              int level)
{

  EULSIM_FORCE_ASSERT(!grid.is_boundary_vertex(vid));

  const double dx = grid.delta(0);
  const double dy = grid.delta(1);
  const Vec2r pos = grid.vertex_xyz(vid);
  const double x0 = pos(0);
  const double y0 = pos(1);

  Vec2r centers[4] = { { x0 + dx / 2., y0 + dy / 2. },
                       { x0 - dx / 2., y0 + dy / 2. }, //
                       { x0 + dx / 2., y0 - dy / 2. },
                       { x0 - dx / 2., y0 - dy / 2. } };

  bool should_subsample = false;
  bool is_first_in = isin.eval(centers[0]) < 0;
  bool is_next_in = false;
  for (unsigned int i = 1; i < 4; ++i)
    {
      is_next_in = isin.eval(centers[i]) < 0;
      if (is_next_in != is_first_in)
        {
          should_subsample = true;
          break;
        }
    }

  if (!should_subsample)
    {
      return fn.eval(grid.vertex_xyz(vid)) * grid.delta(0) * grid.delta(1);
    }
  else
    {
      const int size = (int)std::pow(2, level);
      Grid subgrid(size, size, { x0 - dx/2., y0 - dy/2. }, { x0 + dx/2., y0 + dy/2. });
      return Geometry_integrate_via_voxels(subgrid, fn);
    }
}


// static double
// Geometry_subsample_periodic_via_voxels(Grid &grid, const Index2d &vid,                  //
//                                        ImplicitFunction &value, ImplicitFunction &isin, //
//                                        int level)
// {
//   const int ni = grid.n_verts(0) - 1;
//   const int nj = grid.n_verts(1) - 1;

//   if ((vid(0) == 0) || (vid(0) == ni))
//     {
//       return Geometry_subsample_via_voxels(grid, { 0, vid(1) }, value, isin, level) //
//              + Geometry_subsample_via_voxels(grid, { ni, vid(1) }, value, isin, level);
//     }
//   else if ((vid(1) == 0) || (vid(1) == nj))
//     {
//       return Geometry_subsample_via_voxels(grid, { vid(0), 0 }, value, isin, level) //
//              + Geometry_subsample_via_voxels(grid, { vid(0), nj }, value, isin, level);
//     }
//   else
//     {
//       return Geometry_subsample_via_voxels(grid, vid, value, isin, level);
//     }
// }

} // End of eulsim