/** clamped_solid_simulator.hxx
 *
 * Steady-state solution of a solid with specified boundary.
 */

#ifndef EULSIM_CLAMPED_SOLID_SIMULATOR_HXX
#define EULSIM_CLAMPED_SOLID_SIMULATOR_HXX

#include <string>

#include <eulsim/defs.hxx>
#include <eulsim/dof_vector.hxx>
#include <eulsim/grid.hxx>
#include <eulsim/solid.hxx>

namespace eulsim
{

class ClampedSolidSimulator
{

public:
  struct SimulationInfo
  {
    double dt;
    double g;
    double damping_velocity;
    bool   do_first_order_velocity_extrapolation;
    SimulationInfo();
  };

  struct SolutionState
  {
    DOFVector_2RV v, k;
    SolutionState(Grid &gin) : v(gin), k(gin) {}
  };


  enum VertexType
  {

    VERTEX_ACTIVE = 0,
    VERTEX_GHOST = 1,
    VERTEX_INACTIVE = 2
  };

  enum CellType
  {
    CELL_ACTIVE = 0,
    CELL_INACTIVE
  };

  enum VariableType
  {
    VAR_VELOCITY,
    VAR_REFMAP
  };

  struct VertexGhostStatus
  {
    VertexType vtype;
    Real w0, w1, w2;
    Index2d p1, p2;
    SurfacePoint p0;
  };

  ClampedSolidSimulator(Grid &grid_in, ClampedSolid &solid_in, const SimulationInfo info_in);
  ~ClampedSolidSimulator();

  // initialize everything
  void init_vertex_status();
  void init_ghost_extrapolation_weights(Index2d i2d, VertexGhostStatus *vgs, Vec2r *p_intersection);
  void init(bool for_test = false);

  // Extrapolating for the ghosts
  void extrapolate_values(Real time, DOFVector_2RV &variable, VariableType variable_type);

  // Updating each unknown
  void compute_rhs_velocity(const DOFVector_2RV &velocity, const DOFVector_2RV &refmap, DOFVector_2RV *rhs);
  void compute_rhs_refmap(const DOFVector_2RV &velocity, const DOFVector_2RV &refmap, DOFVector_2RV *rhs);

  // Set initial conditions
  void apply_initial_conditions(SolutionState *state);
  
  // Time advance
  void update_euler(SolutionState *state, arma::mat *residual = NULL);
  void update_rk3(SolutionState *state, arma::mat *solutionchange);

  // Write the solution
  void write_vtk_solution_str(const std::string filename, SolutionState *state);
  void write_vtk_solution_unstr(const std::string filename, SolutionState *state);
  void write_vtk_ghost_rays(const std::string filename);

  // Interim quantities, just for testing.
  DOFVector_1RV &get_rhoarea_dofvector();
  DOFVector_2RV &get_divstressarea_dofvector();
  DOFVector_2RV &get_laplacianksiarea_dofvector();
  DOFVector_4RV &get_nablavarea_dofvector();

  Grid &grid();
  VertexGhostStatus &vertex_status(Index2d);
  ClampedSolid &solid();

  SimulationInfo& info(){ return _info; }
  const SimulationInfo& info() const { return _info; }

private:
  // Paramters used in the simulation
  SimulationInfo _info;

  // The grid
  Grid &_grid;

  // The solid and the boundary conditions
  ClampedSolid &_solid;


  // Temporary storage for assembling different parts of the Residual.
  DOFVector_1RV _rhoarea;
  DOFVector_2RV _divstressarea;
  DOFVector_2RV _laplacianksiarea; // Only for testing
  DOFVector_4RV _nablavarea;

  // Temporary storage for EULER scheme
  SolutionState _euler_interim;

  // Temporary storage for RK3
  SolutionState _rk3_interim_0;
  SolutionState _rk3_interim_1;
  SolutionState _rk3_interim_2;

  // Vertex information (active, completely passive, ghost)
  DOFVector<VertexGhostStatus, 1, STORAGE_VERTICES> _vertex_status;


  bool _is_initialized;
};

} // namespace eulsim

#endif /*EULSIM_CLAMPED_SOLID_SIMULATOR_HXX*/
