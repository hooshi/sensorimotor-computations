function rhs  = rhs_v_fd(v, xi, mass, is_valid, params)
% function rhs = rhs_v_upwind(v, xit, params)

if(nargin == 0 ) run_min_example(); return; end

if( isempty(is_valid) ) ; end

sfb.check_params(params);

dx = params.dx;

v_plus = circshift(v, -1);
v_minus = circshift(v, 1);
v_plus(end)= 2*v_plus(end-1)-v_plus(end-2);  %extrapolate from inside
v_minus(1) = 2*v_minus(2)    -v_minus(3);    %extrapolate from inside

xi_plus = circshift(xi, -1);
xi_minus = circshift(xi, 1);
xi_plus(end)= 2*xi_plus(end-1)-xi_plus(end-2);    %extrapolate from inside
xi_minus(1) = 2*xi_minus(2)    -xi_minus(3);      %extrapolate from inside

x = params.x;
x_plus = params.x + dx;
x_minus = params.x - dx;

dxidx_edge_plus = (xi_plus - xi) /dx;
dxidx_edge_minus = circshift(dxidx_edge_plus, 1);
dxidx_edge_minus(1) = (xi_minus(2) - xi_minus(1)) / dx;
T_plus  =  params.solid.cauchy_stress( dxidx_edge_plus );
T_minus =  params.solid.cauchy_stress( dxidx_edge_minus );

% No stress out of the solid (investigate)
if 1
    T_plus(  ~params.solid.is_refmap_inside( (xi_plus + xi) / 2.) ) = 0;
    T_minus( ~params.solid.is_refmap_inside( (xi_minus + xi) / 2.) ) = 0;
end
if 0
    idx_plus_in=  params.solid.is_refmap_inside( (xi_plus + xi) / 2.);
    idx_minus_in=  params.solid.is_refmap_inside( (xi_minus + xi) / 2.);
    for i = 1:length(params.barriers)
        idx_plus_in = idx_plus_in | params.barriers{i}.is_inside( (x_plus + x) / 2. );
        idx_minus_in = idx_minus_in | params.barriers{i}.is_inside( (x_minus + x) / 2. );
    end
    T_plus(  ~idx_plus_in ) = 0;
    T_minus( ~idx_minus_in ) = 0;
end
% Why did I code this like this??
if 0
    vec = @(x_) x_(:);
    n = size(xi, 1);
    vid_self = vec(1:n);
    vid_left =  vec([1 1:(n-1)]);
    vid_right =  vec([2:n n]);
    T_plus( vid_self( ~is_valid ) ) = 0;
    T_plus( vid_right( ~is_valid ) ) = 0;
    T_minus( vid_self( ~is_valid ) ) = 0;
    T_minus( vid_left( ~is_valid ) ) = 0;
end
%
dTdx = (T_plus - T_minus) / dx;

vdvdx = [];
switch params.advm
    case "center"
        vdvdx = v .* (v_plus - v_minus) / 2 / dx;
    case "upwind_fd"
        vpos = max(v, 0);
        vmin = min(v, 0);
        vdvdx = vpos .* (v - v_minus) / dx + vmin .* (v_plus - v) / dx;
    otherwise
        assert(0)
end

rhs =  mass .* (-vdvdx+params.g) + dx .* dTdx;

end

function run_min_example()


size=20;
c = 1;

syms  xs
v_sym =  symfun( sinh( 1.3*sin( 2*pi*xs ) ), xs);
xi_sym =  symfun( erf((xs-0.5)*5), xs);
dv_sym =  diff(v_sym, xs);
dxi_sym = diff(xi_sym, xs);
dT_sym = diff(  c^2 * (1 ./ (dxi_sym + 1) - 1), xs);
vdv_sym = v_sym*dv_sym;
rho_sym = (dxi_sym + 1);

rhs_sym = -vdv_sym + dT_sym / rho_sym;
num_funs = matlabFunction(v_sym, xi_sym, rhs_sym);

params = sfb.default_params();
params.advm = "center";
% params.advm = "upwind_fd";
params.solid = sfb.Solid( 1, -1, -1, [-10000 10000], 1, 1);
params.c = c;

for i=1:5
    x = linspace(0, 1, size); x = x(:);
    dx = 1 / (size - 1);
    params.dx = dx;
    params.x = x;
    
    [v, xi, rhs_exact] =  num_funs(x);
    xi = xi + x; % for sfb
    mass = sfb.compute_mass(x, xi, params);
    rhs_fd = sfb.rhs_v_fd(v, xi, mass, [], params);
    rhs_fd = rhs_fd ./ mass;
    % rhs_fd = wave.rhs_v_fd(v, xi, params);
    
    if(i==3)
        hold off, plot(x, v,'b','linewidth',2);
        hold on, plot(x, xi,'r','linewidth',2);
        hold on, plot(x(1:end), rhs_fd(1:end),'g','linewidth',2);
        hold on, plot(x(1:2:end), rhs_exact(1:2:end),'mo','linewidth',2);
        % hold off, plot(x, xi);
        % hold on, plot(x, v);
        % hold off, plot(x, mass);
    end
    
    % Boundary conditions are not the same as the manufactures solution
    rhs_exact = rhs_exact(2:end-1);
    rhs_fd = rhs_fd(2:end-1);
    fprintf("%5d %15.6e %15.6e %15.6e \n", size, ...
        max(rhs_exact), max(rhs_fd), max(rhs_exact - rhs_fd) );
    size = size * 2;
end

end