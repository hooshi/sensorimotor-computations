/** defs.hxx
 *
 * General enums, preprocessors, etc.
 *
 * Shayan Hoshyari February 2018
 */

#ifndef EULSIM_DEFS_HXX
#define EULSIM_DEFS_HXX

#include <armadillo>
#include <cassert>
#include <sstream>

#define EULSIM_ABS(x) ((x) > 0 ? (x) : -(x))
#define EULSIM_MAX(x, y) ((x) > (y) ? (x) : (y))
#define EULSIM_MIN(x, y) ((x) < (y) ? (x) : (y))
#define EULSIM_PI 3.14159265359

#define EULSIM_UNUSED(x) ((void)(x));

// ---------------------------------------------------------
//                          ASSERTIONS
// ---------------------------------------------------------

// Name of a function
#define EULSIM_FUNC __PRETTY_FUNCTION__

#define EULSIM_FORCE_ASSERT_MSG(EXPR, MSG)                                                                             \
  if (!(EXPR))                                                                                                         \
    {                                                                                                                  \
      std::ostringstream ss;                                                                                           \
      ss << std::scientific;                                                                                           \
      printf("Assertion in %s, %d \n", EULSIM_FUNC, __LINE__);                                                         \
      ss.str("");                                                                                                      \
      ss << MSG << "\n";                                                                                               \
      ss << #EXPR << "\n";                                                                                             \
      printf("\033[1;31m %s \033[0m\n", ss.str().c_str());                                                             \
      assert( (EXPR) );                                                                                                    \
      throw;                                                                                                           \
    }
#define EULSIM_FORCE_ASSERT(EXPR) EULSIM_FORCE_ASSERT_MSG(EXPR, "")

#if defined(NDEBUG)
#define EULSIM_ASSERT_MSG(EXPR, MSG)
#else
#define EULSIM_ASSERT_MSG(EXPR, MSG) EULSIM_FORCE_ASSERT_MSG(EXPR, MSG)
#endif

#define EULSIM_ASSERT(EXPR) EULSIM_ASSERT_MSG(EXPR, "")

#define EULSIM_ASSERT_EQUAL_MSG(EXPR, VAL, MSG)                                                                        \
  EULSIM_ASSERT_MSG((EXPR) == (VAL), (EXPR) << " == " << (VAL) << " " << MSG)
#define EULSIM_ASSERT_GE_MSG(EXPR, VAL, MSG) EULSIM_ASSERT_MSG((EXPR) >= (VAL), (EXPR) << " >= " << (VAL) << " " << MSG)
#define EULSIM_ASSERT_LE_MSG(EXPR, VAL, MSG) EULSIM_ASSERT_MSG((EXPR) <= (VAL), (EXPR) << " <= " << (VAL) << " " << MSG)
#define EULSIM_ASSERT_GREATER_MSG(EXPR, VAL, MSG)                                                                      \
  EULSIM_ASSERT_MSG((EXPR) > (VAL), (EXPR) << " > " << (VAL) << " " << MSG)
#define EULSIM_ASSERT_LESSER_MSG(EXPR, VAL, MSG)                                                                       \
  EULSIM_ASSERT_MSG((EXPR) < (VAL), (EXPR) << " < " << (VAL) << " " << MSG)

namespace eulsim
{

typedef double Real;
typedef int Index_t;
typedef arma::Col<Real>::fixed<2> Vec2r;
typedef arma::Col<Real>::fixed<3> Vec3r;
typedef arma::Col<Real>::fixed<4> Vec4r;

template <arma::uword n, arma::uword m>
using Mat_R = arma::Mat<Real>::fixed<n, m>;
typedef Mat_R<2, 2> Mat2r;
typedef arma::Col<Index_t>::fixed<2> Vec2i;
typedef arma::Col<Index_t>::fixed<3> Vec3i;
typedef Vec2i Index2d;
typedef Vec3i Index3d;



inline Real
arma_cross2d(const Vec2r x, const Vec2r y)
{
  return x(0) * y(1) - x(1) * y(0);
}

template <typename eT>
inline std::string
arma_print(const arma::Mat<eT> &mat)
{
  std::stringstream ss;
  ss << mat;
  return ss.str();
}

enum Numbers
{
  ZERO = 0,
  ONE,
  TWO,
  THREE,
  FOUR,
  FIVE,
  SIX,
  SEVEN,
  EIGHT,
  NINE,
  TEN
};

} // namespace eulsim

#endif /* EULSIM_DEFS_HXX */
