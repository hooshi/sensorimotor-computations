
size=10;
for i=1:9
    [verr, xierr] = eval_error(size,'center');
    fprintf("CENTER: %5d %12.6e %12.6e \n", size, verr, xierr);
    size = size*2;
end

fprintf("\n");
size=10;
for i=1:9
    [verr, xierr] = eval_error(size, 'upwind_fd');
    fprintf("UPWIND: %5d %12.6e %12.6e \n", size, verr, xierr);
    size = size*2;
end


function [verr, xierr] = eval_error(size, method)

domain = [-5 5];
alpha = 0.1;
t_end = 4;
offset = -2;

params.c = 1;
params.dx = (domain(2)- domain(1))/(size-1);
params.dt = alpha * params.dx / params.c;
n_steps = round(t_end / params.dt);
params.dt = t_end / double(n_steps);

params.advm = method;

x = linspace(domain(1), domain(2), size);
[v,xi] = wave.exact_solution(x, offset, params);

t = 0;
for i=1:n_steps    
    %if(mod(i,60) == 0)
    %    hold off, plot(x, v, 'b', 'linewidth', 2);
    %    hold on, plot(x, xi, 'r', 'linewidth', 2);
    %    ylim([-0.5 0.5])
    %    pause(0.3)
    %end
    
    [v,xi] = wave.advance_euler(v, xi, params);
    t = t + params.dt;
end

% t - t_end

[ve, xie] = wave.exact_solution(x, offset+t_end,params);
verr = max( abs(ve - v) ) ;
xierr = max( abs(xie- xi) ) ;
%figure()
%hold off, plot(x, v, 'b', 'linewidth', 2);
%hold on, plot(x, xi, 'r', 'linewidth', 2);
%ylim([-0.5 0.5])
end