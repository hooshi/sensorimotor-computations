import numpy as np
import matplotlib.pyplot as plt

v = np.loadtxt('../results/washer/46-velocity-extp/second-order.3.hst')
l1, = plt.semilogy( v[:,1], '-' , color='r')
plt.semilogy( v[:,3], '--', color='r' )

# v = np.loadtxt('../results/washer/46-velocity-extp/first-order.1.hst')
# l2, = plt.semilogy( v[:,1], '-' , color='g')
# plt.semilogy( v[:,3], '--', color='g' )

v = np.loadtxt('../results/washer/46-velocity-extp/mu-one.0.hst')
l3, = plt.semilogy( v[:,1], '-' , color='g')
plt.semilogy( v[:,3], '--', color='g' )

v = np.loadtxt('../results/washer/46-velocity-extp/mu-zero.0.hst')
l4, = plt.semilogy( v[:,1], '-' , color='b')
plt.semilogy( v[:,3], '--', color='b' )

v = np.loadtxt('../results/washer/46-velocity-extp/mu-one-nobug.0.hst')
l5, = plt.semilogy( v[:,1], '-' , color='m')
plt.semilogy( v[:,3], '--', color='m' )


# plt.legend([l1, l2, l3, l4, l5], ['fo-ext,mu=10dxdy','so-ext,mu=10dxdy','mu=dxdy', 'mu=0', 'mu=1'])
plt.legend([l5, l1, l3, l4], [r'$\mu=1$', r'$\mu=10\Delta x\Delta y$',r'$\mu=\Delta x \Delta y$', r'$\mu=0$'])
plt.gca().set_xlabel('iteration')
plt.gca().set_ylabel('residual norm')
plt.savefig('washer-convergence-experiments.pdf', bbox_inches='tight')
plt.show()
