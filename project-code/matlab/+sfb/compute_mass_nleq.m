function [mass, J, n_contact] = compute_mass_nleq(x, xi, params)
% function [mass] = compute_mass(x, xi, params)

if( nargin == 0) run_min_example(); return; end


sfb.check_params(params);
n = size(x, 1);
dx = params.dx;
solid = params.solid; 
% n_max_sample = params.n_sample;

MAX_CONTACT=2;
J = sparse( MAX_CONTACT, n );
n_contact = 0;

mass = zeros(n, 1 );

for i=1:(n-1)
     
     dxidx = ( xi(i+1) - xi(i) ) / dx;
     mass_left = 0;
     mass_right = 0;
     % options = optimset('Display','off', 'MaxIter', params.n_sample);
     
     if( solid.is_refmap_inside( xi(i) ) &&  solid.is_refmap_inside( xi(i+1) ) )
         mass_right = dxidx * dx / 2;
         mass_left = mass_right;
     elseif( solid.is_refmap_inside( xi(i) ) ||  solid.is_refmap_inside( xi(i+1) )  )
         
         xi_of_x = @(x_) ( (x(i+1)-x_)*xi(i) + (x_-x(i))*xi(i+1) ) / dx;
         phi_of_x = @(x_) solid.refmap_signed_dist( xi_of_x(x_) );
         x_mid = (x(i+1) + x(i) ) / 2;
         
         % [x_boundary, fval, reason] = fsolve(phi_of_x,x_mid, options);
         x_boundary = utility.bisection(phi_of_x, x(i), x(i+1), 0, dx/params.n_sample);
         
         if( solid.is_refmap_inside( xi(i) ) )
             dx_left =  max( min( dx/2, x_boundary - x(i)  ), 0 );
             dx_right = max( min( dx/2, x_boundary - x_mid ), 0 );
         else %  solid.is_refmap_inside( xi(i+1) )
             dx_left =  max( min( dx/2, x_mid - x_boundary ), 0 );
             dx_right = max( min( dx/2, x(i+1)- x_boundary ), 0 );
         end
         
         mass_left = dx_left * dxidx;
         mass_right = dx_right * dxidx;
         %mass_left = 0.5*(dx_left+dx_right)*dxidx;
         %mass_right = mass_left;
         
         % fprintf("%.8f, %.8f, %.8f, %.8f, %.8f, %.8f \n", dx/2, x(i), x(i+1), x_boundary, dx_left, dx_right); 
         %hold on; plot([x_boundary, x_boundary], [0, 1]);
         
         %
         % Check for contact with barriers
         %
         n_contact_this_cell = 0;
         for j  = 1:length(params.barriers)
             b = params.barriers{j};
             
             if( b.is_inside( x_boundary ) )
                 
                 n_contact = n_contact + 1;
                 n_contact_this_cell = n_contact_this_cell + 1;
                 assert(n_contact <= MAX_CONTACT );
                 assert( n_contact_this_cell <= 1 );
                
                 normal = 0; 
                 if( b.direction )
                     % checks
                     assert( ~b.is_inside( x(i) ) );
                     assert( b.is_inside( x(i+1) ) );
                     assert( solid.is_refmap_inside( xi(i) ) );
                     assert( ~solid.is_refmap_inside( xi(i+1) )  );
                     normal = 1;
                 else
                     % checks
                     assert( b.is_inside( x(i) ) );
                     assert( ~b.is_inside( x(i+1) ) );
                     assert( ~solid.is_refmap_inside( xi(i) )  );
                     assert( solid.is_refmap_inside( xi(i+1) )  );
                     normal = -1;
                 end
                 
                 % Find the interpolation coefficients
                 J(n_contact, i)   =  normal*(x(i+1)-x_boundary) / dx ;
                 J(n_contact, i+1) =  normal*(x_boundary-x(i))   / dx ; 
             end

         end
     end    
         
     mass(i)   = mass(i)   + mass_left;
     mass(i+1) = mass(i+1) + mass_right;
end

%solid.refmap_signed_dist(xi)
% hold off, plot( x, solid.refmap_signed_dist(xi));

end

function run_min_example()
params = sfb.default_params();

n=33;
x = linspace(-6, 6, n);
x = x(:);
xi_interval = [-3.98, 4];
masse = xi_interval(2) - xi_interval(1);
stretch = 0.39;
params.solid = sfb.Solid(1, 0, stretch, xi_interval, 1);
params.dx = ( x(end) - x(1) ) / (n-1);
params.x = x;

params.n_sample = 5;
params.barriers{end+1} =  sfb.Barrier(1.55,   1) ;
params.barriers{end+1} =  sfb.Barrier(-1.54, 0) ;

for i=1:5
    [~,xi] = params.solid.initial_solution(x);
    [mass, J, n_contact] = sfb.compute_mass_nleq(x, xi, params);
    % mass = sfb.compute_mass(x, xi, params)
    fprintf("%4d %12.6e %d \n", params.n_sample, abs( masse-sum(mass) ) , n_contact);
    params.n_sample = params.n_sample * 2;
end
J

end