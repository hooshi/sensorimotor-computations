function check_params(params)
% function check_params(params)
 to_check = { 'c', 'dx', 'dt', 'advm' };
 
 for i=1:length(to_check)
     assert( isfield(params, to_check{i}) , sprintf(" ``%s\'\' not in params", to_check{i}) );
     
 end
 
end

