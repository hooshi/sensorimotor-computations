function [v,xi] = advance_euler(v, xi,params)
% function [v,xi] = advance_euler(v, xi,params)

rhs = wave.rhs_v_fd(v, xi, params);
v = v + params.dt * rhs;
rhs = wave.rhs_xi_fd(v, xi, params);
xi = xi + params.dt * rhs;

% For some reason this is not way less stable.
% rhs0 = wave.rhs_v_fd(v, xi, params);
% rhs1 = wave.rhs_xi_fd(v, xi, params);
% v = v + params.dt * rhs0;
% xi = xi + params.dt * rhs1;

end
