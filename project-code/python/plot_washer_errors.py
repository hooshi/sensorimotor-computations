import numpy as np
import matplotlib.pyplot as plt

v = np.loadtxt('../results/washer-refinement.log')

vel_inf = v[[0,2,4,6],2]
vel_l2 = v[[0,2,4,6],1]
vel_l1 = v[[0,2,4,6],0]

ksi_inf = v[[1,3,5,7],2]
ksi_l2 = v[[1,3,5,7],1]
ksi_l1 = v[[1,3,5,7],0]

print ksi_inf

n = 1 / np.array([184,92,46,23], dtype=float)

l1, = plt.loglog(n, ksi_inf, 'b-o')
l2, = plt.loglog(n, vel_inf, 'r-o')

l3, = plt.loglog(n, ksi_l2, 'b--^')
l4, = plt.loglog(n, vel_l2, 'r--^')

l5, = plt.loglog(n, ksi_l1, 'b-.s')
l6, = plt.loglog(n, vel_l1, 'r-.s')

l7, = plt.loglog(n, n*n, 'k--')
l8, = plt.loglog(n, n, 'k:')

print v

plt.xlabel('h')
plt.ylabel('Error norm')
plt.legend([l1,l3,l5,l2,l4,l6,l7,l8],
           [r"$\xi - L_\infty$", r"$\xi - L_2$", r"$\xi - L_1$"
            ,r"$v - L_\infty$", r"$v - L_2$", r"$v - L_1$"
            ,r"$O(h^2)$", r"$O(h)$"])
plt.savefig('error-refinement.pdf', bbox_inches='tight')
plt.show()
