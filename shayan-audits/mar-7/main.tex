%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% - Scribe notes for CS530P 25th January 2018.
% - Instructor: Prof. Dinesh Pai, Scribe: Shayan Hoshyari
% - Adapted from: Template for Scribe Notes -- CS500, Will Evans (Spring 2012)
% - Adapted from: Erik Demaine at MIT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[11pt]{article}
\usepackage{latexsym}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{amsfonts}
%\usepackage{epsfig}
\usepackage[margin=1in]{geometry}
\usepackage{url}
\usepackage{graphicx}
\usepackage{float}
\usepackage{listings}
%\usepackage{psfig}

\lstset{
         basicstyle=\footnotesize\ttfamily,
         escapechar=¢,
         language=python,
         frame=single
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Fonts
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage[T1]{fontenc}
\usepackage{kpfonts}%  for math    
\usepackage{libertine}%  serif and sans serif
\usepackage[scaled=0.85]{beramono}%% mono


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Extra commands
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newcommand{\scribeTitle}[3]{
  \begin{centering}
  \framebox{ \vbox{
      \textbf{CPSC 530P Sensorimotor Computation}
      
      \vspace{-1mm}
      { #1 }
      
      \vspace{-1mm}
      \emph{#2, Scribe: #3}
    }}
  \end{centering}
  \vspace*{4mm}
}

\newcommand{\R}{\mathbb{R}}     % use for capital R real number set symbol
\newcommand{\Z}{\mathbb{Z}}     % use for capital Z integer set symbol


\newtheorem{theorem}{Theorem}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{observation}[theorem]{Observation}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{fact}[theorem]{Fact}
\newtheorem{assumption}[theorem]{Assumption}

\usepackage[bookmarks,bookmarksnumbered,%
allbordercolors={0.8 0.8 0.8},%
linktocpage%
]{hyperref}

\parindent 0in
\parskip 1.5ex
%\renewcommand{\baselinestretch}{1.25}

\newcommand{\reffig}[1]{Fig.~\ref{#1}}
\newcommand{\refeq}[1]{Eq.~\ref{#1}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%  Document Body
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

% ---------------------------------------------
% Title
% ---------------------------------------------
\scribeTitle{%
  Lecture: 14, %
  Finite element formulation of the resting solution of a 1-D strand
}{Prof. Dinesh Pai}{Shayan Hoshyari}

\section*{Note on symbols}
Since I wanted to use $X$ for degrees of freedom of function $x$, I
have used $\xi$ for the material space and both $x$ and $X$ for the
physical space.
% ---------------------------------------------
% OBJECTIVE
% ---------------------------------------------
\section*{Objective}
In the last lecture, we introduced the concept of the discrete
solution. Suppose that we are looking for an approximation to the
solution, $u=u(x)$, of a PDE, $\mathcal L(u)=0$. Rather than looking
for the approximate solution inside the space of all the admissible
functions, we will confine ourselves to a set of discrete functions
$u_h=u_h(x;U_h)$. Here $u_h$ is the discrete solution, and $U_h$ is
the vector of degrees of freedom that parameterize $u_h$, and the
subscript $h$ is used to emphasize that these values depend on the
grid. The goal is usually to find $U_h$ such that the discrete
solution is a good approximation of its exact counterpart, e.g.,
\[
  U_h = \arg\min_{U_h}( \| u- u_h \|),
\]
with respect to some function norm.

Clearly, $u$ is not explicitly given, therefore a dicretization scheme
should be used to express the problem of finding the degrees of
freedom as an equivalent problem of finding the solutions to a set of
algebraic equations, or the minimizers of some kind of energy
function. The degrees of freedom $U_h$ can then be solved for using
standard minimization methods from linear algebra.

In this lecture, such a scheme will be introduced based on the
variational principles. In this method, the solution of the problem is
shown to minimize a problem specific energy function. Then the degrees
of freedom $U_h$ are chosen such that the discrete solution $u_h$
minimizes this energy. Many laws of physics may be expressed as the
optimization of some suitable functional~\footnote{For more
  information refer to ``The Variational Principles of Mechanics'' by
  Cornelius Lanczos.}.

The introduced discretization scheme is then used to find the resting
position of a 1-D strand modelled by the St. Venant-Kirchhoff
constitutive relation.

% ---------------------------------------------
% Variational principles 
% ---------------------------------------------
\section*{Variational Principles Example}

Consider a spring with initial length $\xi$, a lumped mass $m$ at the
end, and a deformed length $x$, as shown below.
\begin{center}
  \includegraphics[width=0.8\linewidth]{img/spring}
\end{center}
Working directly with the forces means that one tries to explicitly
account for the spring force $f_\text{spring}(x) = -k(x-\xi)$. The
spring force is then used along with any external force in Newton's
second law of motion to find the deformed position $x$.

A variational formulation will instead look at an energy function
$W_\text{spring}(x,\xi)=\frac{1}{2}k(x-\xi)^2$. When no external
forces are applied, $x$ would be the minimizer of $W$, which would
give $x=\xi$. Also, note that the relation
$f_\text{spring}=-\frac{d W_\text{spring}}{dx}$ holds between the
spring force and the spring energy.

% ---------------------------------------------
% 1D Strand
% ---------------------------------------------
\section*{Resting Position of a 1-D Strand %
  \footnote{Further reading: \\
    Sifakis, Eftychios, and Jernej Barbic. ``FEM simulation of 3D def
    ormable solids''  ACM SIGGRAPH 2012  \\
    Teran, Joseph, Sylvia Blemker, V . Hing, and Ronald
    Fedkiw. ``Finite volume methods for the simulation of
    skeletal muscle'', SCA 2003 \\
    Li, Duo, Shinjiro Sueda, Debanga R.  Neog, and Dinesh
    K. Pai. ``Thin skin elastodynamics'', SIGGRAPH 2013} %
}

We wish to model the resting position of a 1-D strand (in $\xi$ space)
under gravity and different boundary conditions. The strand is a
simple line in the material space $\xi$. As a combined effect of
boundary conditions, gravity, and elasticity, the strand will be
deformed into a resting position described by the motion
$x=x(\xi)$. If the strand behaves according to the
St. Venant-Kirchhoff model, its resting position must minimize the
energy function $W$ found as
\[
  \begin{aligned}
    &W = \int_0^L \left( \frac{1}{2} \mu \left(
      \frac{dx}{d\xi}^T\frac{dx}{d\xi} - 1\right)^2 %
      + \rho_0 A g x_2  \right) d\xi, \\
  \end{aligned}  
\]
where $A$ is the physiological cross-section area, $\rho_0$ is the
density of the strand in the material space, and $g$ is the magnitude
of the gravitational acceleration (its direction is assumed to be
$-x_2$). The goal is to find an approximation to $x$ as a discrete
solution $x_h$ which minimizes a discrete approximation of the energy
$W_h$.

As shown in the figure below, the strand is modeled by a mesh with
$N+1$ vertices $v_0,v_1,\cdots,v_{N}$ with material coordinates
$\xi_0,\xi_1,\cdots,\xi_{N}$ and $N-1$ elements
$e_0,e_1,\cdots,e_{N-1}$. The discrete deformation $x_h$ at every
$\xi$ is defined as $x_h(\xi;X_h) = \sum_{i=1}^N X_{hi} \psi_i(\xi)$.
Here, $X_{h}$ is the vector of degrees of freedom with entries
$X_{hi}$ that represent the value of the discrete deformation at
vertex $i$, and $\psi_i$ is the linear basis function associated with
the vertex $i$:
\[
  \psi_i(\xi) = \begin{cases} %
    \frac{\xi - \xi_{i+1}}{\xi_{i} - \xi_{i+1}}  %
    \quad &\xi_{i} \leq \xi \leq \xi_{i+1} \\    
    \frac{\xi - \xi_{i-1}}{\xi_{i} - \xi_{i-1}}
    \quad &\xi_{i-1} \leq \xi \leq \xi_{i} \\
    0  & \text{otherwise} \\
  \end{cases}
\]
A schematic of the terminology used is shown in the
figure below.

\begin{center}
  \includegraphics[width=0.8\linewidth]{img/deformation}
\end{center}

We will start by finding the derivative $F_{h,i}=\frac{d x_h}{d X}$ at
every element $e_i$.
\[
  \frac{d x_h}{d \xi}(\xi; X_h) %
  = \frac{d\psi_i}{d\xi}(\xi) X_{h,i}+ \frac{d\psi_{i+1}}{d\xi}(\xi)
  X_{h,i+1} %
  = \frac{X_{h,i+1} -X_{h,i}}{\xi_{i+1} - \xi_{i}} = F_{h,i} %
  \qquad \xi_{i} \leq \xi \leq \xi_{i+1}. 
\]
Then, we can define a discrete strain $W^{s}_{h,i}$ energy at every
element
\[
  W^{s}_{h,i}(X_h) %
  = \int_{\xi_i}^{\xi_{i+1}} \frac{1}{2} \mu \left(
    \frac{dx}{d\xi}^T\frac{dx}{d\xi} - 1\right)^2 d \xi%
  = \frac{1}{2} \mu (F_{h,i}^TF_{h,i} - 1).
\]
And also a gravitational energy $W^{g}_{h,i}$ at every element
\[
  W^{g}_{h,i}(X_h) %
  = \int_{\xi_i}^{\xi_{i+1}} \rho_0 A g x_{h,2}(\xi) d \xi %
  = \frac{1}{2} \rho_0 A g ( X_{h,i,2} +  X_{h,i+1,2}).
\]
Finally, the total energy can be found as
\[
  W_h(X_h) =\sum_{i=1}^{N-1} \left( W^{s}_{h,i}(X_h) + W^{g}_{h,i}(X_h) \right).
\]

The following code snippet defines a function {\tt W(xi,x)} that
evaluates the energy in terms of the vertex material positions
$\xi_i$ and the vertex $X_{h,i}$ values.
\begin{lstlisting}[language=Python]
#!/usr/bin/env python3

## Finite Element Modeling of Simplest 1D strand in 2D
## CPSC 530P Sensorimotor Computation, Spring 2018  
## Dinesh K. Pai, UBC
##
## Large deformation non-linear elastostatics, Lagrangian
## discretization Worked example using dense matrices for illustration;
## for real world implementations, dealing with sparsity is key 

import numpy as np
import matplotlib.pyplot as plt
import math
from scipy.optimize import minimize

# Function to plot a strand
def plot_strand(x):
    plt.plot(x[:,0],x[:,1],'-o')
    plt.axis('equal')
    plt.grid()
    plt.xlabel('$x^1$')
    plt.ylabel('$x^2$')
    plt.show()

# Number of elements
n = 10

# Length of strand in the material space.
L = 0.1 

# The xi_i values
xi = np.transpose([np.linspace(0,L,n+1)])
# Can use arbitrary mesh too! E.g., X = np.transpose([[0, 0.0025,
# 0.005, 0.01, 0.02, 0.04, 0.06, 0.08, 0.09, 0.095, 0.1]])

# Initial guess for X_{hi}
x0 = np.append(xi,  np.zeros((n+1,1)), axis = 1)
# plot_strand(x0) 

# acceleration due to gravity, m/s^2. Note (2,1) array
G = np.array([[0], [-9.81]])
# physiological cross-section area, m^2
PCSA = 1e-4
# density, approximately of water. kg/m^3
RHO = 1e3 
# print('total mass of strand =', PCSA*L*RHO, 'kg')

# Stiffness/Lame' constant. J/m^3 
# 1e-4 is very small, for illustration. Real muscle is much stiffer.
MU = 1e-4


## Difference matrix, square matrix, backward difference
D = np.diag(np.ones(n+1)) - np.diag(np.ones(n),-1) 
# remove extra first row
D = D[1:,:] 
print(D, '\n', np.shape(D))

## Element center matrix
B = np.diag(np.ones(n+1)) + np.diag(np.ones(n),-1) #square backward difference
B = 0.5 * B[1:,:]
print(B, '\n', np.shape(B))

# Function to find W(X_h)
def W (xi,x):
    ## Differential kinematics
    Dxi = D @ xi
    Dx = D @ x
    
    # Deformation gradients for each element.
    # Note: using * and not @
    F = Dx * (1/Dxi) # Dx * Dxi^{-1} in this special case has a simple form
    
    # right Cauchy-Green tensor
    # note that since we're storing coordinates as rows, this is
    # computing F^T F per element
    C = np.diag(F @ np.transpose(F)) 
    
    # Green strain
    Eg = C-1
    
    # St.VK (St. Venant - Kirchoff) Material (behaves well under large stretch,
    # but poorly under compression)
    We = 0.5*MU*(Eg @ np.transpose(Eg))
    
    # center of mass of each element
    c = B @ x
    
    # Gravity potential is also defined so that f = - dW/dx
    # So potential should increase with height, W = - mass x accn due to gravity x height
    Wg = np.sum(- RHO*PCSA*Dxi * (c @ G))
    
    return We + Wg

\end{lstlisting}

To find the resting position of the string when its left end is fixed
at point $a$, and its right end pinned at point $b$, one has to solve:
\[
  X_h = \arg\min(W_h(X_h)) \quad \text{subject to}\quad X_{h0}=a \quad
  \text{ and } \quad X_{hN}=b.
\]
Using the given python function {\tt W(xi,x)}, this can be done as
\begin{lstlisting}[language=Python]
# Get constraint values a and b from the material space.
a = [x0[0,:]]
b = [x0[-1,:]]

# Modified objective function that excludes X_0 and X_N, because their 
# positions are constrained
def W2 (x2): 
    return W(xi, np.concatenate((a, np.reshape(x2, (n-1, 2)), b), axis=0))

# Initial guess
x20 = x0[1:n,:]

# Minimize
x2opt = minimize(W2, x20, method='BFGS', options=dict(disp=True, maxiter=50000))
xopt = np.concatenate((a, np.reshape(x2opt.x, (n-1, 2)), b), axis=0)
plot_strand(xopt)
\end{lstlisting}
And the following values for $x$ will be obtained:
\begin{center}
  \includegraphics[width=0.45\linewidth]{img/strand-under-gravity.pdf}
\end{center}


\end{document}
