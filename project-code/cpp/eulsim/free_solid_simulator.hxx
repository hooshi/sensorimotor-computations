/** free_solid_simulator.hxx
 *
 * A solid which moves around and stuff.
 */

#ifndef EULSIM_FREE_SOLID_SIMULATOR_HXX
#define EULSIM_FREE_SOLID_SIMULATOR_HXX

#include <string>

#include <eulsim/defs.hxx>
#include <eulsim/dof_vector.hxx>
#include <eulsim/grid.hxx>
#include <eulsim/solid.hxx>
#include <eulsim/geometry.hxx>

namespace eulsim
{

class FreeSolidSimulator
{

public:
  struct SimulationInfo
  {
    double dt;
    double g;
    SimulationInfo() : dt(0), g(0) {}
  };

  struct SolutionState
  {
    DOFVector_2RV v, k;
    DOFVector_1CV is_valid;
    SolutionState(Grid &gin) : v(gin), k(gin), is_valid(gin) {}
  };

  enum VariableType
  {
    VAR_VELOCITY,
    VAR_REFMAP
  };

  FreeSolidSimulator(Grid &grid_in, FreeSolid &solid_in, std::vector<const Obstacle *> obstacles_in,
                     const SimulationInfo info_in);
  ~FreeSolidSimulator() {}

  // initialize everything
  // void init();
  // Set initial conditions
  void apply_initial_conditions(SolutionState *state);

  // Extrapolating for the ghosts
  void extrapolate(DOFVector_2RV &velocity, DOFVector_1CV &is_valid, const int n_layers);

  // Updating each unknown
  void compute_mass(DOFVector_2RV &refmap, DOFVector_1CV *is_valid, DOFVector_1RV *mass);
  void compute_rhs_velocity(SolutionState &state,  DOFVector_1RV &mass, DOFVector_2RV *rhs);
  void compute_rhs_refmap(SolutionState &state, DOFVector_2RV *rhs);

  //
  bool is_mass_at(const DOFVector_2RV &refmap, const Vec2r pos);
  ImplicitFunction* is_mass_at_functor(DOFVector_2RV &refmap);
  double rho_at(const DOFVector_2RV &refmap, const Vec2r pos);
  
  // Time advance
  void update_velocity(SolutionState *state, arma::mat *update_norm = NULL);
  void update_refmap(SolutionState *state, arma::mat *update_norm = NULL);
  void update_all(SolutionState *state, arma::mat *update_norm = NULL);

  // Interim quantities, just for testing.
  // clang-format off
  double get_total_mass()  { return _total_mass; }
  DOFVector_1RV& get_mass_dofvector()  { return _mass; }
  DOFVector_2RV& get_divstressarea_dofvector(){ return _divstressarea; }
  DOFVector_2RV& get_laplacianksiarea_dofvector()  {return _laplacianksiarea;}
  DOFVector_4RV& get_nablavarea_dofvector()  {return _nablavarea;}
  Grid&        grid()  {return _grid;}  
  FreeSolid&   solid()  {return _solid;}
  std::vector<const Obstacle*> obstacles() {return _obstacles;}

  SimulationInfo& info()  {return _info;}
  const SimulationInfo&  info() const {return _info;}
  // clang-format on

private:
  // Paramters used in the simulation
  SimulationInfo _info;

  // The grid
  Grid &_grid;

  // The solid (maybe add scripted objects later)
  FreeSolid &_solid;
  std::vector<const Obstacle *> _obstacles;

  // Temporary storage for assembling different parts of the Residual (buffers)
  double        _total_mass;
  DOFVector_1RV _mass;
  DOFVector_2RV _divstressarea;
  DOFVector_2RV _laplacianksiarea; // Only for testing
  DOFVector_4RV _nablavarea;

  // Buffers for extrapolation
  DOFVector_2RV _extrapolation_v_buffer;
  DOFVector_1CV _extrapolation_is_valid_buffer;


  // Temporary storage for EULER scheme
  DOFVector_2RV _euler_buffer;
  // DOFVector_1CV _is_valid_buffer;
};

} // namespace eulsim

#endif /*EULSIM_FREE_SOLID_SIMULATOR_HXX*/
