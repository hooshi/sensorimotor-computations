/** solid.hxx
 *
 * Representing a solid. The application has to define the solid.
 */

#ifndef EULSIM_SOLID_H
#define EULSIM_SOLID_H

#include <eulsim/defs.hxx>
#include <eulsim/grid.hxx>

namespace eulsim
{

// Information for projecting on a surface (line in 2-D)
struct SurfacePoint
{
  Index_t surface_id;
  Real t;
};

// Material constitutive relations
class Material
{
public:
  virtual Real rho_0() = 0;
  virtual Mat2r stress(Mat2r &def_gradient) = 0;
};

// ClampedSolid.
// Abstract class for representing a clamped solid.
// The assumption is that the clamped boundary does not change over time.
class ClampedSolid
{
public:
  virtual void project_point(const Vec2r &pt, SurfacePoint *spt) = 0;
  virtual Vec2r surface_point_xy(const SurfacePoint &spt) = 0;
  virtual void boundary_solution(const Real time, const SurfacePoint &spt, Vec2r *velocity, Vec2r *refmap) = 0;
  virtual bool is_in_initially(const Vec2r &pt) = 0;
  virtual void initial_solution(const Vec2r &pt, Vec2r *velocity, Vec2r *refmap) = 0;
  virtual bool is_in_refmap(const Vec2r &refmap) = 0;
  virtual Material &material() = 0;
};

// FreeSolid.
// Abstract class for representing a solid with free boundaries.
class FreeSolid
{
public:
  virtual void initial_solution(const Vec2r &pt, Vec2r *velocity, Vec2r *refmap) = 0;
  virtual bool is_in_refmap(const Vec2r &refmap) = 0;
  virtual Material &material() = 0;
};

class Obstacle
{
};

} // namespace eulsim

#endif /*EULSIM_SOLID_H*/
