function [dEdt] = robinson_model(t, E, funcR, parameters)
% function [dEdt] = robinson_model(t, E, funcR, parameters)
%  Input:
%    t: 1xm matrix, ode parameters.
%    E: (n=1)xm matrix that holds the unknown value.
%    funcR: firing rate
%    parameters: containers.MapObject holds the paramters, Et, r, k
%  Output:
%    dEdt: (n=1)xm matrix that holds the derivative of y wrt. t.

% run test for no input
if nargin == 0
    test_robinson_model();
    return
end
assert( isa(funcR, 'function_handle')  );

% get the parameters
Et = parameters('Et');
r  = parameters('r');
k  = parameters('k');

dEdt = ( k * (Et - E) + funcR(t) ) / r;

end

function test_robinson_model()

keys = {'k', 'r', 'Et'};
vals = {4, 0.95, 2};
params = containers.Map(keys, vals);
t = linspace(0,3,20); 
t=t(:);
E = linspace(0,3,20) + 2; 
E=E(:);
dEdt = zeros(size(t));
R    = zeros(size(t));

for i=1:size(dEdt)
    dEdt(i) = robinson_model(t(i), E(i), @test_funcR, params);
    R(i) = test_funcR(t(i));
end

[t R E dEdt]

end


function val = test_funcR(t)

if( (t >= 0.) && (t < 1.) )
    val = 1;
else
    val = 0;
end

end
