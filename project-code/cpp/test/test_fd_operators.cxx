#include <eulsim/clamped_solid_simulator.hxx>
#include <eulsim/defs.hxx>

using namespace eulsim;


namespace test_fd_operators_nsp
{

class DumbLinearMaterial : public Material
{
  Mat2r C;

public:
  DumbLinearMaterial(Mat2r C_in) : C(C_in) {}
  Real
  rho_0()
  {
    return 1;
  }
  virtual Mat2r
  stress(Mat2r &def_gradient)
  {
    return C * def_gradient;
  }
};

class InfinitSolid : public ClampedSolid
{
  DumbLinearMaterial &_material;

public:
  InfinitSolid(DumbLinearMaterial &matin) : _material(matin) {}

  virtual void
  project_point(const Vec2r &pt, SurfacePoint *spt)
  {
  }

  virtual Vec2r
  surface_point_xy(const SurfacePoint &spt)
  {
    return { 0, 0 };
  }
  virtual void
  boundary_solution(const Real time, const SurfacePoint &spt, Vec2r *velocity, Vec2r *refmap)
  {
  }
  virtual bool
  is_in_initially(const Vec2r &pt)
  {
    return true;
  }
  virtual void
  initial_solution(const Vec2r &pt, Vec2r *velocity, Vec2r *refmap)
  {
  }
  virtual bool
  is_in_refmap(const Vec2r &refmap)
  {
    return true;
  }
  virtual Material &
  material()
  {
    return _material;
  }
};

struct ManufacturedSolution
{
  double alpha, beta, gamma, theta;
  Mat2r pseudo_solid;
  double gravityconstant;

  ManufacturedSolution(double vx, double vy, double ksix, double ksiy, Mat2r poormansconstitutive, double gin)
    : alpha(vx), beta(vy), gamma(ksix), theta(ksiy), pseudo_solid(poormansconstitutive), gravityconstant(gin)
  {
  }

  Vec2r
  vel_at(const Vec2r pos) const
  {
    const double x = pos(0);
    const double y = pos(1);

    Vec2r vel;
    vel(0) = alpha * cos(x) * cos(y);
    vel(1) = beta * sin(y * 2.0) * sin(x);
    return vel;
  }

  Vec2r
  ksi_at(const Vec2r pos) const
  {
    const double x = pos(0);
    const double y = pos(1);

    Vec2r ksi;
    ksi(0) = gamma * cos(x * 2.0) * cos(y);
    ksi(1) = theta * sin(y * 2.0) * cos(x);
    return ksi;
  }

  Mat2r
  vel_der_at(const Vec2r pos) const
  {
    const double x = pos(0);
    const double y = pos(1);

    Mat2r dvel;
    dvel(0, 0) = -alpha * cos(y) * sin(x);
    dvel(0, 1) = -alpha * cos(x) * sin(y);
    dvel(1, 0) = beta * sin(y * 2.0) * cos(x);
    dvel(1, 1) = beta * cos(y * 2.0) * sin(x) * 2.0;
    return dvel;
  }

  Mat2r
  ksi_der_at(const Vec2r pos) const
  {
    const double x = pos(0);
    const double y = pos(1);

    Mat2r dksi;
    dksi(0, 0) = gamma * sin(x * 2.0) * cos(y) * -2.0;
    dksi(0, 1) = -gamma * cos(x * 2.0) * sin(y);
    dksi(1, 0) = -theta * sin(y * 2.0) * sin(x);
    dksi(1, 1) = theta * cos(y * 2.0) * cos(x) * 2.0;
    return dksi;
  }

  Vec2r
  laplacian_ksi_at(const Vec2r pos) const
  {
    const double x = pos(0);
    const double y = pos(1);

    Vec2r laplacianksi;
    laplacianksi(0) = gamma * cos(x * 2.0) * cos(y) * -5.0;
    laplacianksi(1) = theta * sin(y * 2.0) * cos(x) * -5.0;
    return laplacianksi;
  }


  Vec2r
  div_deformation_at(const Vec2r pos) const
  {
    const double x = pos(0);
    const double y = pos(1);

    Vec2r div_deformation;
    div_deformation(0) = //
        (cos(y * 2.0) * sin(x) * 2.0) / (gamma * cos(y * 2.0) * sin(x * 2.0) * cos(x) * cos(y) * 4.0 +
                                         gamma * cos(x * 2.0) * sin(y * 2.0) * sin(x) * sin(y)) -
        (cos(x * 2.0) * cos(y)) / (theta * cos(y * 2.0) * sin(x * 2.0) * cos(x) * cos(y) * 4.0 +
                                   theta * cos(x * 2.0) * sin(y * 2.0) * sin(x) * sin(y)) +
        cos(y * 2.0) * cos(x) * 1.0 /
            pow(gamma * cos(y * 2.0) * sin(x * 2.0) * cos(x) * cos(y) * 4.0 +
                    gamma * cos(x * 2.0) * sin(y * 2.0) * sin(x) * sin(y),
                2.0) *
            (gamma * cos(x * 2.0) * sin(y * 2.0) * cos(x) * sin(y) -
             gamma * cos(y * 2.0) * sin(x * 2.0) * cos(y) * sin(x) * 4.0 -
             gamma * sin(x * 2.0) * sin(y * 2.0) * sin(x) * sin(y) * 2.0 +
             gamma * cos(x * 2.0) * cos(y * 2.0) * cos(x) * cos(y) * 8.0) *
            2.0 +
        cos(x * 2.0) * sin(y) * 1.0 /
            pow(theta * cos(y * 2.0) * sin(x * 2.0) * cos(x) * cos(y) * 4.0 +
                    theta * cos(x * 2.0) * sin(y * 2.0) * sin(x) * sin(y),
                2.0) *
            (theta * cos(x * 2.0) * cos(y * 2.0) * sin(x) * sin(y) * 2.0 +
             theta * cos(x * 2.0) * sin(y * 2.0) * cos(y) * sin(x) -
             theta * cos(y * 2.0) * sin(x * 2.0) * cos(x) * sin(y) * 4.0 -
             theta * sin(x * 2.0) * sin(y * 2.0) * cos(x) * cos(y) * 8.0);
    div_deformation(1) = //
        -(sin(y * 2.0) * cos(x)) / (gamma * cos(y * 2.0) * sin(x * 2.0) * cos(x) * cos(y) * 4.0 +
                                    gamma * cos(x * 2.0) * sin(y * 2.0) * sin(x) * sin(y)) -
        (sin(x * 2.0) * sin(y) * 2.0) / (theta * cos(y * 2.0) * sin(x * 2.0) * cos(x) * cos(y) * 4.0 +
                                         theta * cos(x * 2.0) * sin(y * 2.0) * sin(x) * sin(y)) +
        sin(y * 2.0) * sin(x) * 1.0 /
            pow(gamma * cos(y * 2.0) * sin(x * 2.0) * cos(x) * cos(y) * 4.0 +
                    gamma * cos(x * 2.0) * sin(y * 2.0) * sin(x) * sin(y),
                2.0) *
            (gamma * cos(x * 2.0) * sin(y * 2.0) * cos(x) * sin(y) -
             gamma * cos(y * 2.0) * sin(x * 2.0) * cos(y) * sin(x) * 4.0 -
             gamma * sin(x * 2.0) * sin(y * 2.0) * sin(x) * sin(y) * 2.0 +
             gamma * cos(x * 2.0) * cos(y * 2.0) * cos(x) * cos(y) * 8.0) -
        sin(x * 2.0) * cos(y) * 1.0 /
            pow(theta * cos(y * 2.0) * sin(x * 2.0) * cos(x) * cos(y) * 4.0 +
                    theta * cos(x * 2.0) * sin(y * 2.0) * sin(x) * sin(y),
                2.0) *
            (theta * cos(x * 2.0) * cos(y * 2.0) * sin(x) * sin(y) * 2.0 +
             theta * cos(x * 2.0) * sin(y * 2.0) * cos(y) * sin(x) -
             theta * cos(y * 2.0) * sin(x * 2.0) * cos(x) * sin(y) * 4.0 -
             theta * sin(x * 2.0) * sin(y * 2.0) * cos(x) * cos(y) * 8.0) *
            2.0;
    return div_deformation;
  }

  Mat2r
  deformation_at(const Vec2r pos) const
  {
    const double x = pos(0);
    const double y = pos(1);

    Mat2r defgrad;

    defgrad(0, 0) = //
        (cos(y * 2.0) * cos(x) * -2.0) / (gamma * cos(y * 2.0) * sin(x * 2.0) * cos(x) * cos(y) * 4.0 +
                                          gamma * cos(x * 2.0) * sin(y * 2.0) * sin(x) * sin(y));
    defgrad(0, 1) = //
        -(cos(x * 2.0) * sin(y)) / (theta * cos(y * 2.0) * sin(x * 2.0) * cos(x) * cos(y) * 4.0 +
                                    theta * cos(x * 2.0) * sin(y * 2.0) * sin(x) * sin(y));
    defgrad(1, 0) = //
        -(sin(y * 2.0) * sin(x)) / (gamma * cos(y * 2.0) * sin(x * 2.0) * cos(x) * cos(y) * 4.0 +
                                    gamma * cos(x * 2.0) * sin(y * 2.0) * sin(x) * sin(y));
    defgrad(1, 1) = //
        (sin(x * 2.0) * cos(y) * 2.0) / (theta * cos(y * 2.0) * sin(x * 2.0) * cos(x) * cos(y) * 4.0 +
                                         theta * cos(x * 2.0) * sin(y * 2.0) * sin(x) * sin(y));
    return defgrad;
  }

  double
  det_dksi_at(const Vec2r pos)
  {
    const double x = pos(0);
    const double y = pos(1);

    double ans = gamma * theta * cos(y * 2.0) * sin(x * 2.0) * cos(x) * cos(y) * -4.0 //
                 - gamma * theta * cos(x * 2.0) * sin(y * 2.0) * sin(x) * sin(y);
    return ans;
  }

  double
  det_deformation_at(const Vec2r pos)
  {
    double ans = 1. / det_dksi_at(pos);
    return ans;
  }


  Vec2r
  rhs_vel_at(const Vec2r pos) const
  {

    const Vec2r vel = vel_at(pos);
    const Mat2r dvel = vel_der_at(pos);
    const Vec2r ksi = ksi_at(pos);
    const Vec2r divmatT = pseudo_solid * div_deformation_at(pos);
    const Mat2r F = deformation_at(pos);
    // More general would be (Mat8*vectorise(matT)).reshape(4,4)
    const Vec2r divF = div_deformation_at(pos);
    const Vec2r divT = pseudo_solid * divF;
    const Vec2r body_force = { 0, -gravityconstant };

    const Vec2r res = -dvel * vel + 1. * arma::det(F) * divT + body_force;
    return res;
  }

  Vec2r
  rhs_ksi_at(const Vec2r pos) const
  {
    const Vec2r vel = vel_at(pos);
    const Mat2r dksi = ksi_der_at(pos);
    const Vec2r res = -dksi * vel;
    return res;
  }

  arma::mat
  eval_truncation_error(Grid &g)
  {


    DOFVector_2RV vel(g), ksi(g);
    DOFVector_2RV man_rhs_vel(g), man_rhs_ksi(g), man_divT(g), man_laplacianksi(g);
    DOFVector_2RV num_rhs_vel(g), num_rhs_ksi(g), num_divT(g), num_laplacianksi(g);
    DOFVector_1RV man_rho(g);
    DOFVector_1RV num_rho(g);
    NormCalculator norm_vx, norm_vy, norm_ksix, norm_ksiy, norm_divT, norm_nablav, norm_rho, norm_laplacianksi;

    DumbLinearMaterial material(pseudo_solid);
    InfinitSolid solid(material);
    ClampedSolidSimulator::SimulationInfo info;
    info.dt = 1;
    info.g = gravityconstant;
    ClampedSolidSimulator sim(g, solid, info);
    sim.init(true);


    // Set the velocity value for all non-boundary cells
    for (Index2d vid = g.first_vertex(); !g.is_end_vertex(vid); g.advance_vertex(vid))
      {
        // Must include boundaries here.
        Vec2r vxy = g.vertex_xyz(vid);
        vel.set_vec_at(vid, vel_at(vxy));
        ksi.set_vec_at(vid, ksi_at(vxy));
      }

    // Evaluate the rhs
    sim.compute_rhs_velocity(vel, ksi, &num_rhs_vel);
    sim.compute_rhs_refmap(vel, ksi, &num_rhs_ksi);

    // Find the error in the rhs
    int n_verts = 0;
    const double dx = g.delta(0);
    const double area = g.delta(0) * g.delta(1);
    for (Index2d vid = g.first_vertex(); !g.is_end_vertex(vid); g.advance_vertex(vid))
      {
        if (g.is_boundary_vertex(vid))
          continue;

        ++n_verts;

        Vec2r vxy = g.vertex_xyz(vid);

        //
        // manufactured stuff
        //
        Vec2r man_rhs_vel_ij = rhs_vel_at(vxy);
        Vec2r man_rhs_ksi_ij = rhs_ksi_at(vxy);
        //
        Vec2r man_divT_ij = pseudo_solid * div_deformation_at(vxy);
        // Vec4r man_nablav = arma::reshape(vel_der_at(vxy), 4, 1);
        Vec2r man_nablav_ij = vel_der_at(vxy) * vel_at(vxy);
        double man_rho_ij = det_dksi_at(vxy);
        Vec2r man_laplacianksi_ij = laplacian_ksi_at(vxy);


        //
        // Numerically evalutated stuff
        //
        Vec2r num_rhs_vel_ij = num_rhs_vel.vec_at(vid);
        Vec2r num_rhs_ksi_ij = num_rhs_ksi.vec_at(vid);
        //
        Vec2r num_divT_ij = sim.get_divstressarea_dofvector().vec_at(vid) / area;
        // Vec4r num_nablav = sim.get_nablavarea_dofvector().vec_at(vid) / area;
        Vec2r num_nablav_ij = arma::reshape(sim.get_nablavarea_dofvector().vec_at(vid), 2, 2) / area * vel_at(vxy);
        double num_rho_ij = sim.get_rhoarea_dofvector().value_at(vid) / 4.;
        // double num_rho_ij = 0.5 * (1. / det_dksi_at(vxy - dx) + 1. / det_dksi_at(vxy + dx));
        Vec2r num_laplacianksi_ij = sim.get_laplacianksiarea_dofvector().vec_at(vid) / area;

        //
        // Set other big vectors so that we can print them
        //
        num_divT.set_vec_at(vid, num_divT_ij);
        num_rhs_ksi.set_vec_at(vid, num_rhs_ksi_ij);
        num_rhs_vel.set_vec_at(vid, num_rhs_vel_ij);
        num_laplacianksi.set_vec_at(vid, num_laplacianksi_ij);
        num_rho.value_at(vid) = num_rho_ij;
        //
        man_divT.set_vec_at(vid, man_divT_ij);
        man_rhs_ksi.set_vec_at(vid, man_rhs_ksi_ij);
        man_rhs_vel.set_vec_at(vid, man_rhs_vel_ij);
        man_laplacianksi.set_vec_at(vid, man_laplacianksi_ij);
        man_rho.value_at(vid) = man_rho_ij;


        // Update the norms
        norm_vx.add_value(num_rhs_vel_ij(0) - man_rhs_vel_ij(0));
        norm_vy.add_value(num_rhs_vel_ij(1) - man_rhs_vel_ij(1));
        norm_ksix.add_value(man_rhs_ksi_ij(0) - num_rhs_ksi_ij(0));
        norm_ksiy.add_value(man_rhs_ksi_ij(1) - num_rhs_ksi_ij(1));
        norm_divT.add_matrix(Vec2r(man_divT_ij - num_divT_ij));
        norm_nablav.add_matrix(Vec2r(man_nablav_ij - num_nablav_ij));
        norm_rho.add_value(man_rho_ij - num_rho_ij);
        norm_laplacianksi.add_matrix(man_laplacianksi_ij - num_laplacianksi_ij);


        // Some sanity checks
        {
          const double detF = det_deformation_at(vxy);
          const double detdksi = det_dksi_at(vxy);
          const double armdetdksi = arma::det(ksi_der_at(vxy));
          const double armdetF = arma::det(deformation_at(vxy));

          EULSIM_FORCE_ASSERT(EULSIM_ABS(detF - 1. / detdksi) < 1e-10);
          EULSIM_FORCE_ASSERT(EULSIM_ABS(armdetdksi - detdksi) < 1e-10);
          EULSIM_FORCE_ASSERT(EULSIM_ABS(armdetF - detF) < 1e-8);
        }

        // std::cout << vid(0) << " " << vid(1) << ": " << arma::abs(man_nablav - num_nablav).t();
        // std::cout << man_nablav.t();
        // std::cout << num_nablav.t();
        // std::cout << vel_at({ vxy(0), vxy(0) + 1 }).t();
        // std::cout << vel_at(vxy).t();
        // std::cout << vel_at({ vxy(0), vxy(0) - 1 }).t();
        // std::cout << "\n";
      }

    // Find errors
    const int row_l1 = 0;
    const int row_l2 = 1;
    const int row_linf = 2;
    Mat_R<3, 7> error_norms;
    norm_vx.norms(&error_norms(row_l1, 0), &error_norms(row_l2, 0), &error_norms(row_linf, 0));
    norm_vy.norms(&error_norms(row_l1, 1), &error_norms(row_l2, 1), &error_norms(row_linf, 1));
    norm_ksix.norms(&error_norms(row_l1, 2), &error_norms(row_l2, 2), &error_norms(row_linf, 2));
    norm_ksiy.norms(&error_norms(row_l1, 3), &error_norms(row_l2, 3), &error_norms(row_linf, 3));
    norm_nablav.norms(&error_norms(row_l1, 4), &error_norms(row_l2, 4), &error_norms(row_linf, 4));
    norm_rho.norms(&error_norms(row_l1, 5), &error_norms(row_l2, 5), &error_norms(row_linf, 5));
    norm_divT.norms(&error_norms(row_l1, 6), &error_norms(row_l2, 6), &error_norms(row_linf, 6));
    // norm_laplacianksi.norms(&error_norms(row_l1, 6), &error_norms(row_l2, 6), &error_norms(row_linf, 6));

    // print some stuff
    // if (!goh)
    //  {
    //    num_rhs_vel.print_on_grid(stdout, 0);
    //    // num_laplacianksi.print_on_grid(stdout, 0);
    //    printf("\n");
    //    man_rhs_vel.print_on_grid(stdout, 0);
    //    goh = 1;
    //  }

    return error_norms;
  }
};

} // End of test_fd_operators_nsp

int
test_fd_operators()
{
  const Mat2r m2eye({ { 1, 0 }, { 0, 1 } });
  const Mat2r m2zero({ { 0, 0 }, { 0, 0 } });
  const Mat2r m2rand({ { 1, 5 }, { 5, 9 } });

  test_fd_operators_nsp::ManufacturedSolution ms(1, 1, 1, 1, m2eye, 0);

  printf("          %10s %10s %10s %10s %10s %10s %10s \n", "resv0", "resv1", "resksi0", "resksi1", "nablav", "rho",
         "divT");
  for (int i = 0; i < 4; ++i)
    {
      const int twopowi = std::pow(2, i);
      const int nx = 11 * twopowi;
      const int ny = 9 * twopowi;

      // Boundary must be before pi/4 otherwise some derivatives become zero and weird things happen
      Grid g(nx, ny, { 0.2, 0.2 }, { 0.71, 0.71 });
      // ms.eval_truncation_error(g).save(std::cout, arma::arma_ascii);

      arma::mat errors = ms.eval_truncation_error(g);
      for (int i = 0; i < (int)errors.n_rows; ++i)
        {
          if (i == 0)
            printf("    L1  : ");
          if (i == 1)
            printf("    L2  : ");
          if (i == 2)
            printf("    Linf: ");
          for (int j = 0; j < (int)errors.n_cols; ++j)
            {
              printf("%10.4e ", errors(i, j));
            }
          printf("\n");
        }
      printf("\n");
    }
  return 0;
}
