\section{Method}
\label{sec:method}

%% ==============================================
%% Equations
%% =============================================
\subsection{Problem Statement}
The deformation of a solid in Eulerian coordinates is prescribed by
the equations
\begin{equation}
  \label{eq:transient}
    \begin{aligned}
      &\rho (v_t + v \cdot \nabla v) = \nabla \cdot T + \rho b,  &(a)\\
      &\rho = \rho_0 \det(\nabla \xi), &(b)\\
      &\xi_t + v \cdot \nabla \xi = 0, &(c)\\
  \end{aligned}
\end{equation}
where $\xi$ is the reference map (or material coordinate), $T$ is the
Cauchy stress tensor, $x$ is the physical coordinate, $v$ is the
velocity, $t$ is time, and $b$ is the vector of body forces. Also,
$\rho_0$ and $\rho$ represent the density in the initial reference
conditions and the physical space, respectively.

\begin{figure}
  \centering
  \includegraphics[width=0.35\linewidth]{img/domain.pdf} \hfil
  \includegraphics[width=0.35\linewidth]{img/grid.pdf}  \\
  \makebox[0.35\linewidth][c]{(a)} \hfil
  \makebox[0.35\linewidth][c]{(b)}
  \caption{Schematic of (a) the domain that the solid occupies in the
    physical space (b) the computational grid}
  \label{fig:problem}
\end{figure}

% Clamped boundary
Given a solid as shown in figure~\ref{fig:problem}-a, this project
considers two cases. In the first case, the location of the solid
$\Omega$ and its boundary $\partial \Omega$ in the physical domain are
prescribed, along with fixed boundary conditions
\[
  v(x,t) = 0; \qquad \xi(x,0) = \xi_0(x) \qquad   x \in \partial \Omega. 
\]
The goal is then to find the steady state deformation of the
solid. Therefore, a damping term $\mu \nabla^2 v$ is added to the
right hand side of equation~(\ref{eq:transient}-a), and the solution
is advanced in time until its rate of change becomes negligible. Since
the steady-state solution of the velocity would be zero, this term
will not introduce additional errors. Section~\ref{sec:results} further
shows the effect of this damping term.

% Free boundary
In the second case, the solid has a boundary which can move freely, so
that the domain that the solid occupies $\Omega$ would be changing
over time. Assuming that the boundary of the solid $\partial \Omega$
is identified as the iso-value of an implicit function $\phi$, the
free surface boundary conditions can be written as:
\[
  T(x,t)n = 0; \qquad \phi_t + v \cdot \nabla \phi = 0 \qquad   x \in \partial \Omega(t),
\]
where $n$ is the unit normal on the solid boundary.  For fluid
simulations, the signed distance function is usually used as the
implicit function $\phi$, and is solved for as one of the
variables. In this work, I follow Levin \etal{} \cite{levin2011}, and
use the reference map $\xi$ itself as the surface tracking implicit
function. Kamrin \etal{} \cite{kamrin2012}, on the other hand, follow
the former approach and explicitly solve for the signed distance
function.  Admittedly, I am still not very comfortable with free
surfaces, and don't have an intuition on the benefits of using one
surface tracking variable over the other. Collision of the solid with
fixed barriers can also be modeled by constraining the velocity to be
non-penetrating on the barrier boundary \cite{levin2011}.

To discretize the problem, the physical domain will be placed on a
structured grid, as shown in figure~\ref{fig:problem}-c, where a
control volume $\Omega_{ij}$ is associated with every vertex. Then,
the velocity and reference map values are solved for at every grid
point and time level. This process is addressed is three main steps:
discretization of the spacial terms, boundary conditions, and time
advance.

%%==============================================
%%             Discretization Scheme
%%==============================================
\subsection{Discretization of the Spacial Terms}

Finite-difference approximations are mainly used in this work to
evaluate a discrete approximation of the differential operators in
equation~\eqref{eq:transient}. To deal with the discontinuous density
field due to the free boundary, however, some ideas have to be
borrowed from the finite-volume method:
equation~(\ref{eq:transient}-a) is integrated inside a control volume
$\Omega_{ij}$, and the divergence theorem is used to yield
\begin{equation}
  \label{eq:fvm}
   \left( \int_{\Omega_{ij}} \rho d\Omega \right)%
  \left( v_t + v \cdot \nabla v -b \right) -
  \int_{\partial\Omega_{ij}} Tn dA = 0,
\end{equation}
where the term $m_{ij}=\int_{\Omega_{ij}} \rho d\Omega$ is evaluated
using quadrature with a subgrid resolution. The conventional
finite-difference approximations can then be applied to discretize the
equations (\ref{eq:transient}-b), (\ref{eq:transient}-c), and
\eqref{eq:fvm}.

%% If we integrate ...
The advective terms $v\cdot\nabla v$ and $v\cdot\nabla \xi$ are
evaluated using a first-order upwind difference scheme
\begin{multline}
  \label{eq-upwind}
  % https://en.wikipedia.org/wiki/Upwind_scheme
  ( v \cdot \nabla \chi )_{ij} = %
  \max(v_x,0) \frac{\chi_{i,j}-\chi_{i-1,j}}{\Delta x} 
  + \min(v_x,0) \frac{\chi_{i+1,j}-\chi_{i,j}}{\Delta x}  \\  
  + \max(v_y,0) \frac{\chi_{i,j}-\chi_{i,j-1}}{\Delta y} 
  + \min(v_y,0) \frac{\chi_{i,j+1}-\chi_{i,j}}{\Delta y} \quad %
  \chi=v_x,v_y,\xi.
\end{multline}
The gradient of the reference map is evaluated at the cell centers
using central difference formulas
\begin{equation}
  \label{eq-xicenter}
  ( \nabla \xi )_{i+\frac{1}{2},j+\frac{1}{2}} = %
  \begin{bmatrix}
    \frac{1}{2\Delta x}(\xi_{i+1,j}+\xi_{i+1,j+1}-\xi_{i,j}-\xi_{i,j+1})^T \\
    \frac{1}{2\Delta y}(\xi_{i,j+1}+\xi_{i+1,j+1}-\xi_{i,j}-\xi_{i+1,j})^T
  \end{bmatrix}^T,
\end{equation}
which can be used to find the density and the Cauchy stress tensor at
control volume boundaries to evaluate the surface integral term in
equation~\eqref{eq:fvm}.

Combining the mentioned numerical approximations by introducing the
differential operators $R_1$ and $R_2$, and neglecting the boundary
conditions, yield the following spacial discretization of the
governing equations
\begin{equation}
\begin{aligned}
  \frac{d v_h}{d t} = R_1(v_h,\xi_h) \\
  \frac{d \xi_h}{d t} = R_2(v_h,\xi_h),
\end{aligned}
\end{equation}
where $v_h$ and $\xi_h$ are the vector of the nodal velocity and
reference map values, respectively.


\subsection{Boundary Conditions}

Applying the boundary conditions are quite different depending on the
condition type, and are as follows.

\subsubsection{Clamped Boundaries}

Assuming that the grid domain is large enough that the nodes on its
boundaries are more than a cell away from $\Omega$, every vertex can
be denoted either as active, ghost, or inactive. Vertices inside
$\Omega$ which are sufficiently far from $\partial \Omega$, will be
denoted as active. Here, I will use a threshold of
$\min(\Delta x/2, \Delta y/2)$ as a metric to determine being
sufficiently far. Then, any vertex which shares a cell with an active
vertex, will be denoted as a ghost. All the other vertices are
considered as inactive. The idea is to only solve for the unknowns at
the active vertices, and then extrapolate the solution from the active
vertices and the boundary conditions to find the solution values at
the ghost vertices. To achieve this, the ray from every ghost vertex
to the nearest boundary point is followed until it intersects the
first edge of the mesh whose vertices are both active. The values of
the solution at the nearest boundary point, and the vertices of the
mentioned edge are then used for extrapolation.  For an annular
domain, figure~\ref{fig:clamped-ghost} shows the ghost vertices, the
rays cast from them to the closest boundary point, and the first
internal edge that they intersect.

\begin{figure}
  \centering
  \includegraphics[width=0.45\linewidth]{img/ghost-rays.pdf} \hfil
  \includegraphics[width=0.45\linewidth]{img/ghost-close.pdf}  \\
  \makebox[0.45\linewidth][c]{(a)} \hfil
  \makebox[0.45\linewidth][c]{(b)} 
  \caption{The rays that are used for extrapolating the solution
    values at ghost vertices for clamped boundaries: (a) all the rays
    (b) a closeup}
  \label{fig:clamped-ghost}
\end{figure}

\subsubsection{Free Boundaries}

Handling problems with free boundaries has proved to be more
challenging. For these problems, the velocity $v$ is only solved for
at vertices with a non-zero mass $m_{ij}$, and is then extrapolated to
a larger vicinity of the solid or the whole domain. The extrapolation
procedure is taken from Bridson \cite{bridson2015fluid} where the
velocity at the closest active vertex (with respect to the Manhattan
norm) is used as the extrapolated value. Also, the stress terms $Tn$
have to be set to zero for the control volume boundaries that lie
outside the solid. Unlike the velocity, the reference map $\xi$ is
solved for throughout the whole domain.

\subsubsection{Time Advance}
Time advance is finding the vector of nodal values at time level
$n+1$, given the vector of nodal values at time level $n$.  The method
used in this work is that of the explicit Euler:
\begin{enumerate}
\item Update the velocity at vertices with non-zero mass:
  $v_h^{n+1}=v_h^{n}+\Delta t R_1(v^n_h,\xi^n_h)$. %
\item Extrapolate the velocity.
\item Update the reference map:
  $\xi_h^{n+1}=\xi_h^{n}+\Delta t R_2(v^{n+1}_h,\xi^n_h)$.
\end{enumerate}
where the superscript $(.)^n$ is used to denote the corresponding time
level of a solution vector.

For 1-D problems, the time advance scheme can be further modified to
handle collisions with fixed barriers. Before updating the velocity, a
check is made to see whether the solid penetrates a barrier (shown in
figure~\ref{fig:collision}). Subsequently, the rate of the change of
the penetration volume, $\dot{g}$, is enforced to be negative during
the time advance. This rate of change can be written as
\[
  \dot g = v_s - v_b = \underbrace{v_{i+1}}_\text{active}(1-q) + \underbrace{v_{i+2}}_\text{ghost}(q) - \underbrace{v_b}_\text{barrier} = v_{i-1} - v_b,
\]
where $v_s$ is the velocity of the solid at its endpoint, and $v_b$ is
the velocity of the barrier.  Other quantities are depicted in
figure~\ref{fig:collision}. Since the constraint is linear, it can
further be written in the compact form $Jv_h < c$. It can then be
incorporated in the time advance scheme by solving the quadratic program
\[
  v_h^{n+1}=
  \arg\min_{v_h}(\frac{1}{2}{v_h}^T{M}{v_h} +
   R^T_{1*}{v_h})  \text{ subject to } {J}{v_h} \leq c,
\]
where $M$ is a diagonal matrix containing the $m_i$ values, and
$R_{1*} = {M}{v_h}^{n} + \Delta t {R}_1({v_h}^{n},{\xi_h}^{n})$.

\begin{figure}
  \centering
  \fbox{\parbox{0.99\linewidth}{\centering
    \includegraphics[width=0.5\linewidth]{img/collision.pdf}
    }}
  \caption{Schematic of 1-D collision}
  \label{fig:collision}
\end{figure}

%%% Local Variables:
%%% TeX-master: "report-0-main"
%%% End: